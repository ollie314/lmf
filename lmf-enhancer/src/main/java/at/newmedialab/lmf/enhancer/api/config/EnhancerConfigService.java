/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.api.config;

import at.newmedialab.lmf.enhancer.exception.EngineAlreadyExistsException;
import at.newmedialab.lmf.enhancer.model.enhancer.EnhancerConfiguration;
import org.apache.marmotta.ldpath.exception.LDPathParseException;

import java.util.List;

/**
 * The enhancer core service provides functionalities for managing the configuration of LMF enhancers.
 * <p/>
 * Author: Sebastian Schaffert
 */
public interface EnhancerConfigService {


    /**
     * Return a collection of all configured enhancement engines.
     * @return
     */
    public List<EnhancerConfiguration> listEnhancementEngines();


    /**
     * Return true if the enhancement engine with the given name exists.
     * @param name
     * @return
     */
    public boolean hasEnhancementEngine(String name);

    /**
     * Return the configuration of the enhancement engine with the given name, or null in case an engine with this
     * name does not exist.
     *
     * @param name
     * @return
     */
    public EnhancerConfiguration getEnhancementEngine(String name);


    /**
     * Create and add the enhancement engine with the name and program passed as argument. Throws EngineAlreadyExistsException
     * in case the engine already exists and LDPathParseException in case the program is not correctly parsed.
     *
     * @param name
     * @param program
     * @return the newly created enhancement engine
     */
    public EnhancerConfiguration createEnhancementEngine(String name, String program) throws EngineAlreadyExistsException, LDPathParseException;


    /**
     * Add a new enhancement engine to the enhancer configuration. If an engine with this name already exists,
     * an exception is thrown.
     * <p/>
     * Note that this method merely updates the configuration and does not automatically re-run the enhancement
     * process for all resources.
     *
     * @param engine
     */
    public void addEnhancementEngine(EnhancerConfiguration engine) throws EngineAlreadyExistsException;


    /**
     * Update the configuration of the enhancement engine given as argument.
     * <p/>
     * Note that this method merely updates the configuration and does not automatically re-run the enhancement
     * process for all resources.
     *
     * @param engine
     */
    public void updateEnhancementEngine(EnhancerConfiguration engine);


    /**
     * Remove the enhancement engine configuration with the given name.
     * <p/>
     * Note that this method merely updates the configuration and does not automatically re-run the enhancement
     * process for all resources.
     *
     * @param engine
     */
    public void removeEnhancementEngine(EnhancerConfiguration engine);
}
