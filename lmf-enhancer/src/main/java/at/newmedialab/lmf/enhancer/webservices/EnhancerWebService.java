/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.webservices;

import at.newmedialab.lmf.enhancer.api.config.EnhancerConfigService;
import at.newmedialab.lmf.enhancer.api.enhancer.EnhancerEngineService;
import at.newmedialab.lmf.enhancer.api.program.EnhancerProgramService;
import at.newmedialab.lmf.enhancer.model.enhancer.EnhancerConfiguration;
import at.newmedialab.lmf.enhancer.model.ldpath.EnhancementProcessor;
import com.google.common.io.CharStreams;

import org.apache.marmotta.commons.sesame.repository.ResourceUtils;
import org.apache.marmotta.ldpath.api.backend.RDFBackend;
import org.apache.marmotta.ldpath.backend.sesame.SesameConnectionBackend;
import org.apache.marmotta.ldpath.exception.LDPathParseException;
import org.apache.marmotta.ldpath.model.fields.FieldMapping;
import org.apache.marmotta.ldpath.model.programs.Program;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
@Path("/enhancer/engines")
public class EnhancerWebService {


    @Inject
    private Logger log;

    @Inject
    private EnhancerConfigService enhancerConfigService;

    @Inject
    private EnhancerProgramService programService;

    @Inject
    private EnhancerEngineService enhancerEngineService;

    @Inject
    private SesameService sesameService;


    @POST
    @Path("/{name}")
    @Consumes("text/plain")
    public Response createEngine(@PathParam("name") final String name, @Context HttpServletRequest request) {

        if (enhancerConfigService.hasEnhancementEngine(name))
            // return 403 forbidden if the engine already exists
            return Response.status(Response.Status.FORBIDDEN).entity("engine with name " + name + " already exists; delete it first").build();
        else {
            try {
                final String programString = CharStreams.toString(request.getReader());

                // Check if the program is valid
                final Program<Value> program = programService.parseProgram(new StringReader(programString));


                Thread t = new Thread("Enhancement engine '" + name + "' create") {
                    @Override
                    public void run() {
                        try {
                            final EnhancerConfiguration e = enhancerConfigService.createEnhancementEngine(name, programString);
                            e.setProgram(program);
                        } catch (Exception e) {
                            log.error("exception while creating enhancement engine (should not happen, all cases checked before)",e);
                        }
                    };
                };
                t.setDaemon(true);
                t.start();

                return Response.ok().build();
            } catch (IOException ex) {
                log.error("error while uploading new enhancement program {}", name, ex);

                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("error while uploading new enhancement program: " + ex.getMessage()).build();
            } catch (LDPathParseException e) {
                log.warn("invalid program for enhancer {}: {}. Core NOT changed", name, e.getMessage());
                return Response.status(Response.Status.BAD_REQUEST).entity("Could not parse program: " + e.getMessage()).build();
            }
        }
    }

    @PUT
    @Path("/{name}")
    @Consumes("text/plain")
    public Response updateEngine(@PathParam("name") final String name, @Context HttpServletRequest request) {
        if (enhancerConfigService.hasEnhancementEngine(name)) {
            try {
                final String newProgram = CharStreams.toString(request.getReader());
                final EnhancerConfiguration engine = enhancerConfigService.getEnhancementEngine(name);

                // Check if the program is valid
                engine.setProgram(programService.parseProgram(new StringReader(newProgram)));
                engine.setProgramString(newProgram);

                Thread t = new Thread("enhancer '" + name + "' update") {
                    @Override
                    public void run() {
                        enhancerConfigService.updateEnhancementEngine(engine);
                    };
                };
                t.setDaemon(true);
                t.start();
                return Response.ok().build();
            } catch (IOException e) {
                log.error("error while uploading new enhancement program {}", name, e);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("error while uploading new enhancement program: " + e.getMessage()).build();
            } catch (LDPathParseException e) {
                log.warn("invalid replacement program for enhancer {}: {}. Core NOT changed", name, e.getMessage());
                return Response.status(Response.Status.BAD_REQUEST).entity("Could not parse program: " + e.getMessage()).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).entity("enhancer with name " + name + " does not exists").build();
    }

    @GET
    @Path("/{name}")
    public Response getProgram(@PathParam("name") String name) {
        try {
            if (enhancerConfigService.hasEnhancementEngine(name)) {
                EnhancerConfiguration engine = enhancerConfigService.getEnhancementEngine(name);

                return Response.ok(engine.getProgramString(), "text/plain").build();
            } else
                return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{name}")
    public Response removeCore(@PathParam("name") String name) {
        if (!enhancerConfigService.hasEnhancementEngine(name))
            // return 404 not found if core does not exist
            return Response.status(Response.Status.NOT_FOUND).entity("engine with name " + name + " does not exists").build();
        else {
            EnhancerConfiguration engine = enhancerConfigService.getEnhancementEngine(name);
            enhancerConfigService.removeEnhancementEngine(engine);
            return Response.ok("enhancer " + name + " deleted").build();
        }
    }


    /**
     * Rebuild the SOLR index by iterating over all resources and storing the index documents for
     * each resource anew.
     *
     * @return ok if successful, 500 if not
     * @HTTP 200 if SOLR index was rebuilt successfully
     * @HTTP 500 if there was an error while rebuilding the SOLR index (see log)
     */
    @POST
    @Path("/reinit")
    public Response reschedule() {
        log.info("recomputing enhancements after admin user request ...");
        try {
            Thread t = new Thread("Enhancement Engine :: resource scheduler") {
                @Override
                public void run() {
                    enhancerEngineService.reschedule();
                };
            };
            t.setDaemon(true);
            t.start();

            return Response.ok().entity("enhnacement engines rescheduled").build();
        } catch (Exception ex) {
            log.error("Error while rescheduling enhancement ...", ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("error while rescheduling enhancement engines").build();
        }
    }

    @GET
    @Produces("application/json")
    public List<String> listActiveEngines() {
        List<String> result = new ArrayList<String>();

        for(EnhancerConfiguration engine : enhancerConfigService.listEnhancementEngines()) {
            result.add(engine.getName());
        }

        return result;
    }

    /**
     * Return a list of all enhancement processors currently registered in the enhacement service. The result will be
     * a list of JSON objects with the fields "namespace" and "name".
     * @return
     */
    @GET
    @Path("/processors")
    @Produces("application/json")
    public List<Map<String,String>> listEnhancementProcessors() {
        List<Map<String,String>> result = new ArrayList<Map<String, String>>();
        for(EnhancementProcessor processor : programService.getProcessors()) {
            Map<String,String> pconfig = new HashMap<String, String>();
            pconfig.put("namespace", processor.getNamespaceUri());
            pconfig.put("name", processor.getLocalName());
            result.add(pconfig);
        }
        return result;
    }


    @POST
    @Consumes("text/plain")
    @Produces("text/plain")
    public Response checkProgram(@Context HttpServletRequest request) {
        try {
            final Program<Value> program = programService.parseProgram(request.getReader());

            RepositoryConnection con = sesameService.getConnection();
            try {
                con.begin();
                SesameConnectionBackend backend = SesameConnectionBackend.withConnection(con);
                return Response.ok().entity(program.getPathExpression(backend)).build();
            } finally {
                con.commit();
                con.close();
            }
        } catch (RepositoryException ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("error while uploading program: " + ex.getMessage()).build();
        } catch (IOException ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("error while uploading program: " + ex.getMessage()).build();
        } catch (LDPathParseException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getLocalizedMessage()).build();
        }
    }

    @POST
    @Path("/debug")
    @Consumes("text/plain")
    @Produces("application/json")
    @SuppressWarnings("unchecked")
    public Response debugProgram(@QueryParam("context") String[] contextURI, @QueryParam("context[]") String[] contextURIarr,
                                 @Context HttpServletRequest request) {
        try {
            final String[] cs = contextURI != null ? contextURI : contextURIarr;
            log.debug("Debugging RdfPath program");
            final Program<Value> program = programService.parseProgram(request.getReader());
            log.trace("Program parsed, found {} fields", program.getFields().size());

            RepositoryConnection con = sesameService.getConnection();
            try {
                con.begin();
                SesameConnectionBackend backend = SesameConnectionBackend.withConnection(con);

                HashMap<String, Object> result = new HashMap<String, Object>();
                for (String c : cs) {
                    final URI context = con.getValueFactory().createURI(c);

                    // FIXME: Change to ResourceUtils.isUsed()
                    if (!ResourceUtils.isSubject(con, c)) {
                        log.info("Debug-Context <{}> not found", c);
                        result.put(c, "404: Not Found");
                        continue;
                    }
                    log.trace("Context loaded: <{}>", context);

                    if (program.getFilter() != null && !program.getFilter().apply(backend, context, Collections.singleton((Value) context))) {
                        result.put(context.toString(), "Does not pass @filter");
                        continue;
                    }

                    log.trace("Evaluating Program for <{}>.", context);
                    HashMap<String, HashSet<String>> contextResult = new HashMap<String, HashSet<String>>();
                    if (program.getBooster() != null) {
                        selectValues(context, contextResult, program.getBooster(), backend);
                    }
                    for (FieldMapping<?, Value> f : program.getFields()) {
                        selectValues(context, contextResult, f, backend);
                    }
                    result.put(context.toString(), contextResult);
                }

                log.trace("Returning {} results", result.size());
                return Response.ok().entity(result).build();

            } finally {
                con.commit();
                con.close();
            }
        } catch (RepositoryException ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("error while debugging program: " + ex.getMessage()).build();
        } catch (IOException ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("error while debugging program: " + ex.getMessage()).build();
        } catch (LDPathParseException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity((e.getCause() != null ? e.getCause() : e).getLocalizedMessage()).build();
        }
    }

    private void selectValues(final URI c, HashMap<String, HashSet<String>> result, FieldMapping<?, Value> f, RDFBackend<Value> backend) {
        log.trace("Getting '{}'-values for <{}>", f.getFieldName(), c);
        Collection<?> values = f.getValues(backend, c);
        HashSet<String> val = new HashSet<String>(values.size());
        for (Object object : values) {
            val.add(String.valueOf(object));
        }
        log.trace("Found {} values", val.size());
        result.put(f.getFieldName(), val);
    }

}
