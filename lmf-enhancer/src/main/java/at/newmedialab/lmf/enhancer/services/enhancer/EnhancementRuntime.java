/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.services.enhancer;

import at.newmedialab.lmf.enhancer.api.enhancer.EnhancerEngineService;
import at.newmedialab.lmf.enhancer.model.enhancer.EnhancerConfiguration;
import at.newmedialab.lmf.worker.services.WorkerRuntime;
import org.openrdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The EnhancementRuntime is the in-memory representation of an enhancement engine and its workers. It maintains
 * all the state necessary during runtime for executing the engine.
 * <p/>
 * Author: Sebastian Schaffert
 */
public class EnhancementRuntime extends WorkerRuntime<EnhancerConfiguration> {

    private static final Logger log = LoggerFactory.getLogger(EnhancementRuntime.class);

    /**
     * the parent enhancer engine service; the runtime itself only manages the current state and workers
     * and calls its parent's methods to actually carry out the work
     */
    private EnhancerEngineService parent;

    private boolean optimizing;

    /**
     * Initialise the enhancement runtime, start a new queue and start up the configured number of workers.
     *
     * @param engine
     * @param parent
     */
    public EnhancementRuntime(EnhancerConfiguration engine, EnhancerEngineService parent) {
        super(engine);
        this.parent = parent;

        optimizing = false;
    }


    public boolean isOptimizing() {
        return optimizing;
    }

    public void setOptimizing(boolean optimizing) {
        this.optimizing = optimizing;
    }


    @Override
    protected void execute(Resource resource) {
        parent.executeEnhancementEngine(config,resource);
    }
}
