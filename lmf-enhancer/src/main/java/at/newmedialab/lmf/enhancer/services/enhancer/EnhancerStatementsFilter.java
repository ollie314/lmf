/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.services.enhancer;

import org.apache.marmotta.commons.sesame.filter.statement.StatementFilter;
import org.openrdf.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.HashMap;

@ApplicationScoped
@Named("versioning")
public class EnhancerStatementsFilter implements StatementFilter {

    private static Logger                  log = LoggerFactory.getLogger(EnhancerStatementsFilter.class);

    private final HashMap<String, Integer> enhancerContexts;

    public EnhancerStatementsFilter() {
        log.debug("Starting up {}", EnhancerStatementsFilter.class.getSimpleName());
        enhancerContexts = new HashMap<String, Integer>();
    }

    @Override
    public boolean accept(Statement object) {
        // Ignore triples in the enhancer-contexts during versioning.
        boolean ignore = object.getContext() != null && enhancerContexts.keySet().contains(object.getContext().stringValue());
        if (ignore) {
            log.trace("Ignoring '{}' for versioning", object);
        }
        return !ignore;
    }

    public void addEnhancerContext(String contextUri) {
        if (contextUri == null) return;
        log.debug("Registering new enhacement context '{}' in filter", contextUri);

        synchronized (enhancerContexts) {
            Integer i = enhancerContexts.get(contextUri);
            if (i == null) {
                i = new Integer(1);
            } else {
                i = i + 1;
            }
            enhancerContexts.put(contextUri, i);
        }
    }

    public void removeEnhancerContext(String contextUri) {
        if (contextUri == null) return;
        log.debug("Unregistering new enhacement context '{}' in filter", contextUri);
        synchronized (enhancerContexts) {
            Integer i = enhancerContexts.get(contextUri);
            if (i != null) {
                int _i = i - 1;
                if (_i > 0) {
                    enhancerContexts.put(contextUri, _i);
                } else {
                    enhancerContexts.remove(contextUri);
                }
            }
        }
    }

}
