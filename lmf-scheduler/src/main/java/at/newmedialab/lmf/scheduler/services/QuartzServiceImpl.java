/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.scheduler.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.statistics.StatisticsModule;
import org.apache.marmotta.platform.core.api.statistics.StatisticsService;
import org.apache.marmotta.platform.core.api.user.UserService;
import org.apache.marmotta.platform.core.events.ConfigurationChangedEvent;
import org.apache.marmotta.platform.core.events.ConfigurationServiceInitEvent;
import org.apache.marmotta.platform.core.exception.UserExistsException;
import org.openrdf.model.URI;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.JobPersistenceException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerMetaData;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerKey;
import org.quartz.UnableToInterruptJobException;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;

import at.newmedialab.lmf.scheduler.api.DependencyInjectingJobFactory;
import at.newmedialab.lmf.scheduler.api.QuartzService;

@ApplicationScoped
public class QuartzServiceImpl implements QuartzService {

    private static final Pattern          INTERVAL_STRING_PATTERN = Pattern.compile("^([\\d]+)\\s*([A-Za-z]+?)[Ss]?$");
    private static final String           DEFAULT_QUARTZ_USER     = "lmf.scheduler";

    @Inject
    private Logger                        log;

    @Inject
    private ConfigurationService          configurationService;

    @Inject
    private UserService                   userService;

    @Inject
    private StatisticsService             statisticsService;

    private Scheduler                     scheduler               = null;
    private QuartzStatisticsModule        stats;

    @Inject
    private DependencyInjectingJobFactory jobFactory;
    private URI quartzUser              = null;
    
    public void startup() {
        try {
            if (scheduler != null && scheduler.isStarted() && !scheduler.isShutdown()) {
                log.debug("QuartzService already started");
                return;
            }

            log.info("Starting up QuartzService...");

            final Properties sConf = getSchedulerConfig();
            try {
            	ensureDatabaseTables(sConf);
            	StdSchedulerFactory sf = new StdSchedulerFactory(sConf);
                scheduler = sf.getScheduler();
                scheduler.setJobFactory(jobFactory);
                scheduler.start();
            } catch (SchedulerException ex) {
                log.warn("could not start scheduler with jdbcJobStore, using non-persistent RAMJobStore");
                if (scheduler != null) scheduler.shutdown();
                // if db-setup failed, fall back to RAMJobStore
                final Properties ramConf = new Properties();
                for (Object o : sConf.keySet()) {
                    if (!o.toString().startsWith("org.quartz.jobStore.")) {
                        ramConf.setProperty(o.toString(), sConf.getProperty(o.toString()));
                    }
                }
                ramConf.setProperty("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
                StdSchedulerFactory sf = new StdSchedulerFactory(ramConf);
                scheduler = sf.getScheduler();
                scheduler.setJobFactory(jobFactory);
                scheduler.start();
            }

            log.debug("Scheduler meta-data:\n{}", scheduler.getMetaData().toString());
            log.info("QuartzService started");

            stats = new QuartzStatisticsModule();
            statisticsService.registerModule(stats.getName(), stats);
        } catch (SchedulerException e) {
            log.error("Error while starting QuartzService: {}", e.getLocalizedMessage());
            e.printStackTrace();
        } finally {
        }
    }

    private void ensureDatabaseTables(Properties config) throws JobPersistenceException  {
        final String prefix = config.getProperty("org.quartz.jobStore.tablePrefix", "QRTZ_");
        final String db_type = config.getProperty("db.vendor");

        final String sql_script = prefix + db_type + ".sql";
        try {
        	//persistenceService.getJDBCConnection();
	        Class.forName(config.getProperty("org.quartz.dataSource.qrtz.driver"));
			final Connection con = DriverManager.getConnection(
					config.getProperty("org.quartz.dataSource.qrtz.URL"),
					config.getProperty("org.quartz.dataSource.qrtz.user"),
					config.getProperty("org.quartz.dataSource.qrtz.password"));
			log.warn("Ensuring Quartz DB tables...");
	        try {
	            try {
	                // Check if tables already exist:
	                Statement stmt = con.createStatement();
                    stmt.execute("SELECT * FROM " + prefix + "LOCKS");
                    stmt.close();
	                log.debug("Quartz-Tables already exist, everything is fine!");
	                return;
	            } catch (SQLException e) {
	                // Table not found, continue...
	            }
	
	            log.info("table {}LOCKS not found, running sql script", prefix);
	            log.debug("using Quartz setup script: {}", sql_script);
	
	            final BufferedReader script = new BufferedReader(new InputStreamReader(QuartzServiceImpl.class.getResourceAsStream(sql_script)));
	            try {
		            String line;
		            StringBuilder currentStmt = new StringBuilder();
		            int sqlCount = 0;
		            while ((line = script.readLine()) != null) {
		                // ignore comments
		                line = line.replaceAll("(--|#).*$", "");
		                final int p = line.indexOf(';');
		                if (p >= 0) {
		                    currentStmt.append(line.substring(0, p));
		                    final String sql = currentStmt.toString();
		                    log.trace("executing SQL: {}", sql);
		                    con.createStatement().execute(sql);
		                    sqlCount++;
		                    currentStmt = new StringBuilder(line.substring(p + 1));
		                } else {
		                    currentStmt.append(line.trim()).append(" ");
		                }
		            }
		            log.trace("executed Quartz setup with {} sql-statements", sqlCount);
	            } finally {
	            	script.close();
	            }
	        } catch (IOException e) {
	            log.error("could not read database script {}: {}", sql_script, e.getMessage());
	            throw new JobPersistenceException("Could not read quartz-db init script", e);
	        } finally {
	        	con.close();
	        }
        } catch (ClassNotFoundException ex) {
        	log.error("Could not load driver for Quartz persistence", ex);
        	throw new JobPersistenceException("Could not load driver for Quartz persistence", ex);
        } catch (SQLException sql) {
        	log.error("Setting up Quartz-Persistence failed", sql);
        	throw new JobPersistenceException("Setting up Quartz-Persistence failed", sql);
		}
    }

    @PreDestroy
    public void shutdown() {
        statisticsService.unregisterModule(stats);
        try {
            if (scheduler != null && scheduler.isStarted() && !scheduler.isShutdown()) {
                log.info("Stopping QuartzService...");
                scheduler.shutdown(true);
                log.debug("Scheduler meta-data:\n{}", scheduler.getMetaData().toString());
                scheduler = null;
                log.info("QuartzService stopped.");
            }
        } catch (SchedulerException e) {
            log.error("Error while stopping QuartzService: {}", e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    /**
     * Makes sure the service is initialised on system startup
     * 
     * @param event
     */
    public void onSystemStartup(@Observes ConfigurationServiceInitEvent event) {
        log.debug("SystemStartup event received, starting up...");
        startup();
    }

    public void onConfigurationChanged(@Observes ConfigurationChangedEvent event) {
        if (event.containsChangedKeyWithPrefix("org.quartz.") || event.containsChangedKeyWithPrefix("quartz.") ) {
            log.info("quartz configuration changed, restarting {}", this.getClass().getSimpleName());
            shutdown();
            startup();
            log.info("restart of {} complete.", this.getClass().getSimpleName());
        }
    }

    private Properties getSchedulerConfig() {
        
        final String db_url = String.format("jdbc:h2:file:%s/%s", configurationService.getHome() + File.separator + "db", configurationService.getStringConfiguration("quartz.db.name", "quartz"));
        
        Properties prop = new Properties();
        
        prop.setProperty("org.quartz.scheduler.instanceName", "LMFScheduler");
        prop.setProperty("org.quartz.scheduler.instanceId", configurationService.getServerUri());
        prop.setProperty("org.quartz.threadPool.threadCount", "5");
        prop.setProperty("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
        prop.setProperty("org.quartz.jobStore.tablePrefix", "QRTZ_");
        prop.setProperty("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");

        prop.setProperty("db.vendor", configurationService.getStringConfiguration("quartz.db.type", "h2"));
        prop.setProperty("org.quartz.jobStore.dataSource", "qrtz");
        prop.setProperty("org.quartz.dataSource.qrtz.driver", configurationService.getStringConfiguration("quartz.db.driver", "org.h2.Driver"));
        prop.setProperty("org.quartz.dataSource.qrtz.URL", configurationService.getStringConfiguration("quartz.db.url", db_url));
        prop.setProperty("org.quartz.dataSource.qrtz.user", configurationService.getStringConfiguration("quartz.db.user", "quartz"));
        prop.setProperty("org.quartz.dataSource.qrtz.password", configurationService.getStringConfiguration("quartz.db.password", ""));

        // Override with settings from the config.
        for (String qKey : configurationService.listConfigurationKeys("org.quartz.")) {
            prop.setProperty(qKey, configurationService.getStringConfiguration(qKey));
        }

        return prop;
    }

    @Override
    public boolean isStarted() throws SchedulerException {
        return scheduler.isStarted();
    }

    @Override
    public SchedulerMetaData getMetaData() throws SchedulerException {
        return scheduler.getMetaData();
    }

    @Override
    public List<JobExecutionContext> getCurrentlyExecutingJobs() throws SchedulerException {
        return scheduler.getCurrentlyExecutingJobs();
    }

    @Override
    public Date scheduleJob(JobDetail jobDetail, Trigger trigger) throws SchedulerException {
        return scheduler.scheduleJob(jobDetail, trigger);
    }

    @Override
    public Date scheduleJob(Trigger trigger) throws SchedulerException {
        return scheduler.scheduleJob(trigger);
    }

    @Override
    public void scheduleJobs(Map<JobDetail, List<Trigger>> triggersAndJobs, boolean replace) throws SchedulerException {
        scheduler.scheduleJobs(triggersAndJobs, replace);
    }

    @Override
    public boolean unscheduleJob(TriggerKey triggerKey) throws SchedulerException {
        return scheduler.unscheduleJob(triggerKey);
    }

    @Override
    public boolean unscheduleJobs(List<TriggerKey> triggerKeys) throws SchedulerException {
        return scheduler.unscheduleJobs(triggerKeys);
    }

    @Override
    public Date rescheduleJob(TriggerKey triggerKey, Trigger newTrigger) throws SchedulerException {
        return scheduler.rescheduleJob(triggerKey, newTrigger);
    }

    @Override
    public void addJob(JobDetail jobDetail, boolean replace) throws SchedulerException {
        scheduler.addJob(jobDetail, replace);
    }

    @Override
    public boolean deleteJob(JobKey jobKey) throws SchedulerException {
        return scheduler.deleteJob(jobKey);
    }

    @Override
    public boolean deleteJobs(List<JobKey> jobKeys) throws SchedulerException {
        return scheduler.deleteJobs(jobKeys);
    }

    @Override
    public void triggerJob(JobKey jobKey) throws SchedulerException {
        scheduler.triggerJob(jobKey);
    }

    @Override
    public void triggerJob(JobKey jobKey, JobDataMap data) throws SchedulerException {
        scheduler.triggerJob(jobKey, data);
    }

    @Override
    public void pauseJob(JobKey jobKey) throws SchedulerException {
        scheduler.pauseJob(jobKey);
    }

    @Override
    public void pauseJobs(GroupMatcher<JobKey> matcher) throws SchedulerException {
        scheduler.pauseJobs(matcher);
    }

    @Override
    public void pauseTrigger(TriggerKey triggerKey) throws SchedulerException {
        scheduler.pauseTrigger(triggerKey);
    }

    @Override
    public void pauseTriggers(GroupMatcher<TriggerKey> matcher) throws SchedulerException {
        scheduler.pauseTriggers(matcher);
    }

    @Override
    public void resumeJob(JobKey jobKey) throws SchedulerException {
        scheduler.resumeJob(jobKey);
    }

    @Override
    public void resumeJobs(GroupMatcher<JobKey> matcher) throws SchedulerException {
        scheduler.resumeJobs(matcher);
    }

    @Override
    public void resumeTrigger(TriggerKey triggerKey) throws SchedulerException {
        scheduler.resumeTrigger(triggerKey);
    }

    @Override
    public void resumeTriggers(GroupMatcher<TriggerKey> matcher) throws SchedulerException {
        scheduler.resumeTriggers(matcher);
    }

    @Override
    public void pauseAll() throws SchedulerException {
        scheduler.pauseAll();
    }

    @Override
    public void resumeAll() throws SchedulerException {
        scheduler.resumeAll();
    }

    @Override
    public List<String> getJobGroupNames() throws SchedulerException {
        return scheduler.getJobGroupNames();
    }

    @Override
    public Set<JobKey> getJobKeys(GroupMatcher<JobKey> matcher) throws SchedulerException {
        return scheduler.getJobKeys(matcher);
    }

    @Override
    public List<? extends Trigger> getTriggersOfJob(JobKey jobKey) throws SchedulerException {
        return scheduler.getTriggersOfJob(jobKey);
    }

    @Override
    public List<String> getTriggerGroupNames() throws SchedulerException {
        return scheduler.getTriggerGroupNames();
    }

    @Override
    public Set<TriggerKey> getTriggerKeys(GroupMatcher<TriggerKey> matcher) throws SchedulerException {
        return scheduler.getTriggerKeys(matcher);
    }

    @Override
    public Set<String> getPausedTriggerGroups() throws SchedulerException {
        return scheduler.getPausedTriggerGroups();
    }

    @Override
    public JobDetail getJobDetail(JobKey jobKey) throws SchedulerException {
        return scheduler.getJobDetail(jobKey);
    }

    @Override
    public Trigger getTrigger(TriggerKey triggerKey) throws SchedulerException {
        return scheduler.getTrigger(triggerKey);
    }

    @Override
    public TriggerState getTriggerState(TriggerKey triggerKey) throws SchedulerException {
        return scheduler.getTriggerState(triggerKey);
    }

    @Override
    public boolean interrupt(JobKey jobKey) throws UnableToInterruptJobException {
        return scheduler.interrupt(jobKey);
    }

    @Override
    public boolean interrupt(String fireInstanceId) throws UnableToInterruptJobException {
        return scheduler.interrupt(fireInstanceId);
    }

    @Override
    public boolean checkExists(JobKey jobKey) throws SchedulerException {
        return scheduler.checkExists(jobKey);
    }

    @Override
    public boolean checkExists(TriggerKey triggerKey) throws SchedulerException {
        return scheduler.checkExists(triggerKey);
    }

    @Override
    public int parseTimeFromIntervalString(String string) {
        Matcher m = INTERVAL_STRING_PATTERN.matcher(string);
        int i = 1;
        if (m.matches()) {
            try {
                i = Integer.parseInt(m.group(1));
            } catch (Exception e) {
                log.debug("Could not parse IntervalString: {}", string);
            }
        }
        return i;
    }

    @Override
    public IntervalUnit parseUnitFromIntervalString(String string) {
        Matcher m = INTERVAL_STRING_PATTERN.matcher(string);
        IntervalUnit u = IntervalUnit.DAY;
        if (m.matches()) {
            try {
                u = IntervalUnit.valueOf(m.group(2).toUpperCase());
            } catch (Exception e) {
                log.debug("Could not parse IntervalString: {}", string);
            }
        }
        return u;
    }

    @Override
    public URI getQuartzUser() {
        final String login = configurationService.getStringConfiguration("org.quartz.lmf.user", DEFAULT_QUARTZ_USER);
        quartzUser = userService.getUser(login);
        if (quartzUser == null) {
            try {
                quartzUser = userService.createUser(login);
            } catch (UserExistsException e) {
                log.warn("Could not create quartz user: {} ({})", login, e.getMessage());
                quartzUser = userService.getAnonymousUser();
            }
        }
        return quartzUser;

    }

    private class QuartzStatisticsModule implements StatisticsModule {

        boolean enabled = true;

        public QuartzStatisticsModule() {
        }

        @Override
        public void enable() {
            enabled = true;
        }

        @Override
        public void disable() {
            enabled = false;
        }

        @Override
        public boolean isEnabled() {
            return enabled;
        }

        @Override
        public List<String> getPropertyNames() {
            return new LinkedList<String>(getStatistics().values());
        }

        @Override
        public Map<String, String> getStatistics() {
            if (!enabled) return Collections.emptyMap();

            if (scheduler == null) return Collections.singletonMap("QuartzService State", "invalid");

            LinkedHashMap<String, String> stats = new LinkedHashMap<String, String>();
            try {
                final SchedulerMetaData metaData = scheduler.getMetaData();
                stats.put("name", String.format("%s", metaData.getSchedulerName()));
                stats.put("version", String.format("%s", metaData.getVersion()));
                stats.put("started", String.format("%tF %<tT", metaData.getRunningSince()));
                stats.put("executed jobs", String.format("%d", metaData.getNumberOfJobsExecuted()));
                stats.put("job store", String.format("%s", metaData.getJobStoreClass()));
                stats.put("thread pool", String.format("%s", metaData.getThreadPoolClass()));
                stats.put("thread pool size", String.format("%d", metaData.getThreadPoolSize()));
            } catch (SchedulerException e) {
                stats.put("error", e.getMessage());
            }
            return stats;
        }

        @Override
        public String getName() {
            return "Quartz Scheduler Statistics";
        }

    }

}
