/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.scheduler.services;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.simpl.PropertySettingJobFactory;
import org.quartz.spi.TriggerFiredBundle;
import org.slf4j.Logger;

import at.newmedialab.lmf.scheduler.api.DependencyInjectedJob;
import at.newmedialab.lmf.scheduler.api.DependencyInjectingJobFactory;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

public class DependencyInjectingJobFactoryImpl extends PropertySettingJobFactory implements DependencyInjectingJobFactory {

    @Inject
    private Instance<DependencyInjectedJob> jobInstance;

    @Inject
    private Logger                          log;

    @Override
    public Job newJob(TriggerFiredBundle bundle, Scheduler scheduler) throws SchedulerException {
        log.debug("producing job for scheduler {} ({})...", scheduler.getSchedulerName(), scheduler.getSchedulerInstanceId());

        final Job newJob;
        final Class<? extends Job> jobClass = bundle.getJobDetail().getJobClass();
        if (DependencyInjectedJob.class.isAssignableFrom(jobClass)) {
            @SuppressWarnings("unchecked")
            Class<? extends DependencyInjectedJob> dijClass = (Class<? extends DependencyInjectedJob>) jobClass;
            log.trace("requested job {} is dependency aware", dijClass.getName());
            newJob = jobInstance.select(dijClass).get();

            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.putAll(scheduler.getContext());
            jobDataMap.putAll(bundle.getJobDetail().getJobDataMap());
            jobDataMap.putAll(bundle.getTrigger().getJobDataMap());

            setBeanProps(newJob, jobDataMap);
        }
        else {
            log.trace("creating job using {}", this.getClass().getSuperclass().getSimpleName());
            newJob = super.newJob(bundle, scheduler);
        }
        return newJob;
    }

}