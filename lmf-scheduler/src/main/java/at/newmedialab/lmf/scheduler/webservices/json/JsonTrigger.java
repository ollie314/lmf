/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.scheduler.webservices.json;

import org.codehaus.jackson.annotate.JsonProperty;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerKey;

import java.util.Date;
import java.util.Map;

public class JsonTrigger {

    protected TriggerKey          tKey;

    @JsonProperty
    protected String              description;
    @JsonProperty
    protected Date                fireTime, startTime, endTime;
    @JsonProperty
    protected String              jobKey;
    @JsonProperty
    protected Map<String, Object> jobData;

    public JsonTrigger(Trigger trigger) {
        tKey = trigger.getKey();
        description = trigger.getDescription();

        startTime = trigger.getStartTime();
        fireTime = trigger.getNextFireTime();
        endTime = trigger.getEndTime();

        jobKey = trigger.getJobKey().toString();
        jobData = trigger.getJobDataMap();
    }

    public JsonTrigger(Trigger trigger, JobDetail jobDetail) {
        this(trigger);
        jobData = jobDetail.getJobDataMap();
        jobData.putAll(trigger.getJobDataMap());
    }

    @JsonProperty
    public String getTriggerKey() {
        return tKey.toString();
    }

    @JsonProperty
    public String getKey() {
        return tKey.getName();
    }

    @JsonProperty
    public String getGroup() {
        return tKey.getGroup();
    }
}
