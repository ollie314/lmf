/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.provider;

import org.apache.marmotta.commons.sesame.model.LiteralCommons;
import org.apache.marmotta.ldclient.api.endpoint.Endpoint;
import org.apache.marmotta.ldclient.api.ldclient.LDClientService;
import org.apache.marmotta.ldclient.api.provider.DataProvider;
import org.apache.marmotta.ldclient.exception.DataRetrievalException;
import org.apache.marmotta.ldclient.model.ClientResponse;
import org.apache.stanbol.entityhub.servicesapi.model.Reference;
import org.apache.stanbol.entityhub.servicesapi.model.Representation;
import org.apache.stanbol.entityhub.servicesapi.model.Text;
import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.*;

/**
 * An LDClient Data Provider allowing to access a Stanbol Entityhub as cache. The provider uses direct access to the
 * embedded Stanbol API and does not connect through HTTP.
 * <p/>
 * Author: Sebastian Schaffert
 */
public class StanbolSiteProvider implements DataProvider {

    public static final String STANBOL_ENTITYHUB = "Stanbol Entityhub";

    private static Logger log = LoggerFactory.getLogger(StanbolSiteProvider.class);

    @Override
    public String getName() {
        return STANBOL_ENTITYHUB;
    }

    @Override
    public String[] listMimeTypes() {
        return new String[] { "application/rdf+xml" };
    }

    @Override
    public ClientResponse retrieveResource(String resource, LDClientService client, Endpoint endpoint) throws DataRetrievalException {
        if(! (endpoint instanceof StanbolSiteEndpoint)) {
            throw new DataRetrievalException("the endpoint configuration passed as argument is not a stanbol configuration");
        }

        StanbolSiteEndpoint e = (StanbolSiteEndpoint)endpoint;


        Repository triples =  new SailRepository(new MemoryStore());
        try {
            triples.initialize();

            if(e.getSite().getEntity(resource) != null) {
                Representation rep = e.getSite().getEntity(resource).getRepresentation();

                try {
                    RepositoryConnection con = triples.getConnection();
                    try {
                        con.begin();

                        URI subject = triples.getValueFactory().createURI(resource);

                        for (Iterator<String> fields = rep.getFieldNames(); fields.hasNext();) {
                            String field = fields.next();
                            URI property = triples.getValueFactory().createURI(field);

                            for (Iterator<Object> fieldValues = rep.get(field); fieldValues.hasNext();) {


                                Collection<Object> values = new ArrayList<Object>();
                                //process the parsed value with the Utility Method ->
                                // this converts Objects as defined in the specification
                                checkValues(triples.getValueFactory(), fieldValues.next(), values);
                                //We still need to implement support for specific types supported by this implementation
                                for (Object current : values){
                                    if (current instanceof Value){
                                        con.add(subject, property, (Value) current);
                                    } else if (current instanceof Reference) {
                                        con.add(subject, property, triples.getValueFactory().createURI(((Reference) current).getReference()));
                                    } else if (current instanceof Text) {
                                        Literal object = triples.getValueFactory().createLiteral(((Text) current).getText(), ((Text) current).getLanguage());
                                        con.add(subject, property, object);
                                    } else { //else add an typed Literal!
                                        if(current instanceof String) {
                                            Literal object = triples.getValueFactory().createLiteral(current.toString());
                                            con.add(subject,property,object);
                                        } else {
                                            String type = LiteralCommons.getXSDType(current.getClass());
                                            Literal object = triples.getValueFactory().createLiteral(current.toString(), triples.getValueFactory().createURI(type));
                                            con.add(subject,property,object);
                                        }
                                    }
                                }


                            }
                        }


                        con.commit();
                    } catch(RepositoryException ex) {
                        con.rollback();
                    } finally {
                        con.close();
                    }
                } catch(RepositoryException ex) {
                    log.error("error retrieving triples from Stanbol",ex);
                }
            }

            ClientResponse result = new ClientResponse(200,triples);
            result.setExpires(new Date(System.currentTimeMillis() + endpoint.getDefaultExpiry() * 1000));
            return result;
        } catch (RepositoryException e1) {
            throw new DataRetrievalException("failed to initialise in-memory repository");
        }
    }


    /**
     * This method was stolen from Stanbol and adapted to Sesame:
     * <p/>
     * Processes a value parsed as object to the representation.
     * This processing includes:
     * <ul>
     * <li> Removal of <code>null</code> values
     * <li> Converting URIs and URLs to {@link org.apache.stanbol.entityhub.servicesapi.model.Reference}
     * <li> Converting String[] with at least a single entry where the first
     * entry is not null to {@link org.apache.stanbol.entityhub.servicesapi.model.Text} (the second entry is used as language.
     * Further entries are ignored.
     * <li> Recursive calling of this Method if a {@link Iterable} (any Array or
     *      {@link java.util.Collection}), {@link Iterator} or {@link java.util.Enumeration} is parsed.
     * <li> All other Objects are added to the result list
     * </ul>
     * TODO: Maybe we need to enable an option to throw {@link IllegalArgumentException}
     * in case any of the parsed values is invalid. Currently invalid values are
     * just ignored.
     * @param value the value to parse
     * @param results the collections the results of the parsing are added to.
     */
    public static void checkValues(ValueFactory valueFactory, Object value,Collection<Object> results){
        if(value == null){
            return;
        } else if(value instanceof Iterable<?>){
            for(Object current : (Iterable<?>)value){
                checkValues(valueFactory,current,results);
            }
        } else if(value instanceof Iterator<?>){
            while(((Iterator<?>)value).hasNext()){
                checkValues(valueFactory,((Iterator<?>)value).next(),results);
            }
        } else if(value instanceof Enumeration<?>){
            while(((Enumeration<?>)value).hasMoreElements()){
                checkValues(valueFactory,((Enumeration<?>)value).nextElement(),results);
            }
        } else if(value instanceof java.net.URI || value instanceof URL){
            results.add(valueFactory.createURI(value.toString()));
        } else if(value instanceof String[]){
            String[] values = (String[])value;

            if(values.length>0 && values[0] != null){
                if(values.length > 1) {
                    results.add(valueFactory.createLiteral(values[0],values[1]));
                } else {
                    results.add(valueFactory.createLiteral(values[0]));
                }
            } else {
                log.warn("String[] "+ Arrays.toString((String[]) value)+" is not a valied natural language array! -> ignore value");
            }
        } else {
            results.add(value);
        }
    }

}
