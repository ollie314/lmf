/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.exception;

import org.apache.marmotta.platform.core.exception.MarmottaException;

/**
 * Thrown in case a managed/referenced site already exists
 * <p/>
 * Author: Sebastian Schaffert
 */
public class SiteAlreadyExistsException extends MarmottaException {

    private static final long serialVersionUID = -1470332075848269646L;

    /**
     * Creates a new instance of <code>KiWiException</code> without detail message.
     */
    public SiteAlreadyExistsException() {
    }

    /**
     * Constructs an instance of <code>KiWiException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public SiteAlreadyExistsException(String msg) {
        super(msg);
    }

    public SiteAlreadyExistsException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public SiteAlreadyExistsException(Throwable c) {
        super(c);
    }
}
