/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.config;

import at.newmedialab.stanbol.configurator.model.LMFManagedSiteConfiguration;

import java.util.Map;

/**
 * Configuration for a LMF Managed Site
 *
 * Field descriptions:
 * <ul>
 * <li>id    internal id of the site; also used to construct the URI to access the site content</li>
 * <li>name  human-readable name of the managed site</li>
 * <li>description human-readable description of the managed site (optional)</li>
 * <li>fieldMappings collection of field mappings of the managed site (optional)</li>
 * </ul>
 * <p/>
 * Author: Sebastian Schaffert
 */
public class ManagedSiteConfiguration extends LMFManagedSiteConfiguration {

    public ManagedSiteConfiguration(String id, String name, String description, Map<String, String> fieldMappings) {
        super(id,name,description,fieldMappings);
    }


}
