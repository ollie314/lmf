/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.config;

import java.util.List;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
public interface StanbolChainConfiguration {


    /**
     * Return the name of this web configuration
     * @return
     */
    public String getId();


    /**
     * Update the name of this web configuration
     *
     * @param id
     */
    public void setId(String id);

    /**
     * Get a short human-readable description of the chain.
     * @return
     */
    public String getDescription();

    /**
     * Return the list of engines this chain makes use of.
     * @return
     */
    public List<String> getEngines();


    /**
     * Update the list of engines this chain makes use of
     * @param engines
     */
    public void setEngines(List<String> engines);


    /**
     * The profile used for creating this chain configuration; may be null
     * @return
     */
    public String getProfile();


    /**
     * The context this chain is attached to (e.g. for lookups)
     * @return
     */
    public String getContext();


    /**
     * Set the context this chain is attached to (e.g. for lookups)
     * @return
     */
    public void setContext(String context);
}
