/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.services;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.marmotta.platform.user.api.AccountService;
import org.apache.marmotta.platform.user.api.AuthenticationService;
import org.apache.marmotta.platform.user.model.UserAccount;

@ApplicationScoped
public class LMFWebConsoleSecurityIntegrator { //implements WebConsoleSecurityProvider {

	@Inject
	private AuthenticationService authenticationService;
	
	@Inject 
	private AccountService accountService;
	
//	@Override
	public Object authenticate(String username, String passwd) {
		if (authenticationService != null) {
			if (authenticationService.authenticateUser(username, passwd)) {
				return accountService.getAccount(username);
			}
		}
		// Auth failed
		return null;
	}

//	@Override
	public boolean authorize(Object user, String role) {
		if (user instanceof UserAccount) {
			UserAccount a = (UserAccount) user;
			if (a.getRoles().contains(role)) {
				return true;
			}
		}

		// Not authorized for this role
		return false;
	}

}
