/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.services;

import at.newmedialab.lmf.stanbol.api.SlingLauncherService;
import at.newmedialab.lmf.stanbol.api.StanbolConfigurationService;
import at.newmedialab.lmf.stanbol.exception.ChainAlreadyExistsException;
import at.newmedialab.lmf.stanbol.exception.EngineAlreadyExistsException;
import at.newmedialab.lmf.stanbol.exception.SiteAlreadyExistsException;
import at.newmedialab.lmf.stanbol.model.config.*;
import at.newmedialab.lmf.stanbol.model.enhancer.EnhancementChain;
import at.newmedialab.lmf.stanbol.model.enhancer.EnhancementEngine;
import at.newmedialab.stanbol.configurator.api.ConfiguratorService;
import at.newmedialab.stanbol.configurator.exception.ConfiguratorException;
import at.newmedialab.stanbol.configurator.model.LMFTopicClassificationTrainingSet;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.stanbol.enhancer.servicesapi.Chain;
import org.apache.stanbol.enhancer.servicesapi.ChainManager;
import org.apache.stanbol.enhancer.servicesapi.EnhancementEngineManager;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class StanbolConfigurationServiceImpl implements StanbolConfigurationService {

    @Inject
    private Logger log;

    @Inject
    private SlingLauncherService slingLauncherService;

    @Inject
    private ConfigurationService configurationService;

    /**
     * Return a list of pre-configured enhancement engines available for installation.
     *
     * @return
     */
    @Override
    public Map<String, StanbolEngineConfiguration> getAvailableEnhancementEngines() {
        Map<String,StanbolEngineConfiguration> result = new HashMap<String, StanbolEngineConfiguration>();
        try {
            Enumeration<URL> modulePropertiesEnum = this.getClass().getClassLoader().getResources("stanbol-engines.properties");

            while(modulePropertiesEnum.hasMoreElements()) {
                URL moduleUrl = modulePropertiesEnum.nextElement();

                try {
                    PropertiesConfiguration stanbolProperties = new PropertiesConfiguration(moduleUrl);

                    List<Object> prefixes = stanbolProperties.getList("engines");

                    for(Object prefix : prefixes) {

                        if("KeywordLinkingEngine".equals(stanbolProperties.getString(prefix + ".type"))) {
                            KeywordLinkingEngineConfig cfg =
                                    new KeywordLinkingEngineConfig(
                                            stanbolProperties.getString(prefix + ".name"),
                                            stanbolProperties.getString(prefix + ".site"),
                                            stanbolProperties.getString(prefix + ".label")
                                    );

                            // TODO: further properties currently not supported
                            result.put(cfg.getId(),cfg);

                            log.warn("KeywordLinkingEngines are no longer supported (name = {})", cfg.getId());
                        } else if("EntityhubLinkingEngine".equals(stanbolProperties.getString(prefix + ".type"))) {
                            EntityhubLinkingEngineConfig cfg =
                                    new EntityhubLinkingEngineConfig(
                                            stanbolProperties.getString(prefix + ".name"),
                                            stanbolProperties.getString(prefix + ".site"),
                                            stanbolProperties.getString(prefix + ".label")
                                    );

                            result.put(cfg.getId(),cfg);

                        } else if("NamedEntityTaggingEngine".equals(stanbolProperties.getString(prefix+".type"))) {
                            EntityTaggingEngineConfig cfg =
                                    new EntityTaggingEngineConfig(
                                            stanbolProperties.getString(prefix + ".name"),
                                            stanbolProperties.getString(prefix + ".site"),
                                            stanbolProperties.getString(prefix + ".label")
                                    );
                            if(stanbolProperties.getString(prefix + ".personType") != null) {
                                cfg.setPersonType(stanbolProperties.getString(prefix + ".personType"));
                            }
                            if(stanbolProperties.getString(prefix + ".organisationType") != null) {
                                cfg.setOrganisationType(stanbolProperties.getString(prefix + ".organisationType"));
                            }
                            if(stanbolProperties.getString(prefix + ".placeType") != null) {
                                cfg.setPlaceType(stanbolProperties.getString(prefix + ".placeType"));
                            }

                            result.put(cfg.getId(),cfg);
                        } else {
                            log.warn("unrecognized engine type: {}",stanbolProperties.getString(prefix+".type"));
                        }

                    }
                } catch (ConfigurationException e) {
                    log.error("the properties file {} was invalid, cannot parse configuration (message: {})",moduleUrl,e.getMessage());
                }
            }
        } catch (IOException e) {
            log.error("I/O error while reading properties configuration: {}",e.getMessage());
        }

        return result;
    }

    /**
     * Return a list of pre-configured enhancement chains available for installation.
     *
     * @return
     */
    @Override
    public Map<String, StanbolChainConfiguration> getAvailableEnhancementChains() {
        Map<String,StanbolChainConfiguration> result = new HashMap<String, StanbolChainConfiguration>();
        try {
            Enumeration<URL> modulePropertiesEnum = this.getClass().getClassLoader().getResources("stanbol-chains.properties");

            while(modulePropertiesEnum.hasMoreElements()) {
                URL moduleUrl = modulePropertiesEnum.nextElement();

                try {
                    PropertiesConfiguration stanbolProperties = new PropertiesConfiguration(moduleUrl);

                    List<Object> prefixes = stanbolProperties.getList("chains");

                    for(Object prefix : prefixes) {
                        if("ListChain".equals(stanbolProperties.getString(prefix + ".type"))) {
                            ListChainConfiguration cfg =
                                    new ListChainConfiguration(stanbolProperties.getString(prefix + ".name"));

                            cfg.setDescription(stanbolProperties.getString(prefix+".description"));
                            cfg.setProfile(stanbolProperties.getString(prefix + ".name"));
                            for(Object engine : stanbolProperties.getList(prefix+".engines")) {
                                cfg.getEngines().add(engine.toString());
                            }

                            result.put(cfg.getId(),cfg);

                        } else {
                            log.warn("unrecognized chain type: {}",stanbolProperties.getString(prefix+".type"));
                        }

                    }

                } catch (ConfigurationException e) {
                    log.error("the properties file {} was invalid, cannot parse configuration (message: {})",moduleUrl,e.getMessage());
                }
            }
        } catch (IOException e) {
            log.error("I/O error while reading properties configuration: {}",e.getMessage());
        }

        return result;
    }


    /**
     * Return a list of all available enhancement chains.
     *
     * TODO: maybe we need to introduce caching ...
     *
     * @return
     */
    @Override
    public Set<EnhancementChain> getEnhancementChains() {
        ChainManager manager = slingLauncherService.getChainManager();
        if(manager != null) {
            Set<EnhancementChain> result = new HashSet<EnhancementChain>();

            for(String name : manager.getActiveChainNames()) {
                Chain chain = manager.getChain(name);

                EnhancementChain lmfchain = new EnhancementChain(name,slingLauncherService.getEmbeddedStanbolURL()+"enhancer/chain/"+name,chain.equals(manager.getDefault()));
                if(configurationService.getStringConfiguration("stanbol.enhancer."+name+".profile") != null) {
                    lmfchain.setInheritedProfile(configurationService.getStringConfiguration("stanbol.enhancer."+name+".profile"));
                }
                if(configurationService.getStringConfiguration("stanbol.enhancer."+name+".context") != null) {
                    lmfchain.setContext(configurationService.getStringConfiguration("stanbol.enhancer."+name+".context"));
                }


                result.add(lmfchain);
            }

            return result;
        } else {
            log.error("Stanbol chain manager service not available; returning empty list");
            return Collections.emptySet();
        }
    }


    /**
     * Return a list of all available enhancement engines.
     *
     * @return
     */
    @Override
    public Set<EnhancementEngine> getEnhancementEngines() {
        EnhancementEngineManager manager = slingLauncherService.getEngineManager();
        if(manager != null) {
            Set<EnhancementEngine> result = new HashSet<EnhancementEngine>();

            for(String name : manager.getActiveEngineNames()) {
                org.apache.stanbol.enhancer.servicesapi.EnhancementEngine engine = manager.getEngine(name);

                EnhancementEngine lmfengine = new EnhancementEngine(name,slingLauncherService.getEmbeddedStanbolURL()+"/enhancer/engine/"+name);

                result.add(lmfengine);
            }

            return result;
        } else {
            log.error("Stanbol engine manager service not available; returning empty list");
            return Collections.emptySet();
        }
    }



    /**
     * Create the keyword linking engine with the given specification. Should check whether an engine with this name
     * already exists and throw an exception.
     *
     * @param engine
     * @return true if the engine has been created successfully
     */
    @Override
    @Deprecated
    public boolean createKeywordLinkingEngine(KeywordLinkingEngineConfig engine) throws EngineAlreadyExistsException {
        ConfiguratorService configurator = slingLauncherService.getConfiguratorService();

        if(configurator != null) {
            try {
                configurator.createKeywordLinkingEngine(engine);
                return true;
            } catch (at.newmedialab.stanbol.configurator.exception.EngineAlreadyExistsException e) {
                throw new EngineAlreadyExistsException("error while creating keyword linking engine, engine already exists or malformed request");
            } catch (ConfiguratorException e) {
                log.error("error while creating keyword linking engine {}",engine.getName(),e);
                return false;
            }
        } else {
            log.error("could not create keyword linking engine; embedded Stanbol not available");
            return false;
        }

    }

    /**
     * Create the entityhub linking engine with the given specification. Should check whether an engine with this name
     * already exists and throw an exception.
     *
     * @param engine
     * @return true if the engine has been created successfully
     */
    @Override
    public boolean createEntityhubLinkingEngine(EntityhubLinkingEngineConfig engine) throws EngineAlreadyExistsException {
        ConfiguratorService configurator = slingLauncherService.getConfiguratorService();

        if(configurator != null) {
            try {
                configurator.createEntityhubLinkingEngine(engine);
                return true;
            } catch (at.newmedialab.stanbol.configurator.exception.EngineAlreadyExistsException e) {
                throw new EngineAlreadyExistsException("error while creating entityhub linking engine, engine already exists or malformed request");
            } catch (ConfiguratorException e) {
                log.error("error while creating entityhub linking engine {}",engine.getName(),e);
                return false;
            }
        } else {
            log.error("could not create entityhub linking engine; embedded Stanbol not available");
            return false;
        }

    }


    /**
     * Create the named entity tagging engine with the given specification. Should check whether an engine with this name
     * already exists and throw an exception.
     *
     * @param engine
     * @return true if the engine has been created successfully
     */
    @Override
    public boolean createEntityTaggingEngine(EntityTaggingEngineConfig engine) throws EngineAlreadyExistsException {
        ConfiguratorService configurator = slingLauncherService.getConfiguratorService();

        if(configurator != null) {
            try {
                configurator.createEntityTaggingEngine(engine);
                return true;
            } catch (at.newmedialab.stanbol.configurator.exception.EngineAlreadyExistsException e) {
                throw new EngineAlreadyExistsException("error while creating keyword linking engine, engine already exists or malformed request");
            } catch (ConfiguratorException e) {
                log.error("error while creating entity tagging engine {}",engine.getName(),e);
                return false;
            }
        } else {
            log.error("could not create entity tagging engine; embedded Stanbol not available");
            return false;
        }

    }

    /**
     * Create a new topic classification engine with the specification given as argument.
     *
     *
     * @param config
     * @throws at.newmedialab.lmf.stanbol.exception.EngineAlreadyExistsException
     *
     */
    @Override
    public boolean createTopicClassificationEngine(TopicClassificationEngineConfig config) throws EngineAlreadyExistsException {
        ConfiguratorService configurator = slingLauncherService.getConfiguratorService();

        if(configurator != null) {
            try {
                configurator.createTopicClassificationEngine(config);
                return true;
            } catch (at.newmedialab.stanbol.configurator.exception.EngineAlreadyExistsException e) {
                throw new EngineAlreadyExistsException("error while creating topic classification engine, engine already exists or malformed request");
            } catch (ConfiguratorException e) {
                log.error("error while creating topic classification engine {}",config.getName(),e);
                return false;
            }
        } else {
            log.error("could not create topic classification engine; embedded Stanbol not available");
            return false;
        }
    }

    /**
     * Create a new topic classification training set. This training set is required for using a topic classification
     * engine.
     *
     * @param config
     * @throws at.newmedialab.lmf.stanbol.exception.EngineAlreadyExistsException
     *
     */
    @Override
    public boolean createTopicClassificationTrainingSet(LMFTopicClassificationTrainingSet config) throws EngineAlreadyExistsException {
        ConfiguratorService configurator = slingLauncherService.getConfiguratorService();

        if(configurator != null) {
            try {
                configurator.createTopicClassificationTrainingSet(config);
                return true;
            } catch (at.newmedialab.stanbol.configurator.exception.EngineAlreadyExistsException e) {
                throw new EngineAlreadyExistsException("error while creating topic classification training set, configuration already exists or malformed request");
            } catch (ConfiguratorException e) {
                log.error("error while creating topic classification training set {}",config.getName(),e);
                return false;
            }
        } else {
            log.error("could not create topic classification training set; embedded Stanbol not available");
            return false;
        }
    }

    /**
     * Create the list chain with the given specification. Should check whether a chain with this name
     * already exists and throw an exception.
     *
     *
     * @param config
     * @return true if the engine has been created successfully
     */
    @Override
    public boolean createListChain(ListChainConfiguration config) throws ChainAlreadyExistsException {
        ConfiguratorService configurator = slingLauncherService.getConfiguratorService();

        if(configurator != null) {
            try {
                configurator.createListChain(config);
                return true;
            } catch (at.newmedialab.stanbol.configurator.exception.ChainAlreadyExistsException e) {
                throw new ChainAlreadyExistsException("error while creating list chain, chain already exists or malformed request");
            } catch (ConfiguratorException e) {
                log.error("error while creating list chain {}",config.getName(),e);
                return false;
            }
        } else {
            log.error("could not create list chain; embedded Stanbol not available");
            return false;
        }
    }

    /**
     * Create a managed site according to the configuration passed as argument. Throws an exception in case the site
     * already exists.
     *
     * @param config
     * @throws at.newmedialab.lmf.stanbol.exception.SiteAlreadyExistsException
     *
     */
    @Override
    public boolean createManagedSite(ManagedSiteConfiguration config) throws SiteAlreadyExistsException {
        ConfiguratorService configurator = slingLauncherService.getConfiguratorService();

        if(configurator != null) {
            try {
                configurator.createManagedSite(config);
                return true;
            } catch (at.newmedialab.stanbol.configurator.exception.ManagedSiteExistsException e) {
                throw new SiteAlreadyExistsException("error while creating managed site, site already exists or malformed request");
            } catch (ConfiguratorException e) {
                log.error("error while creating managed site {}",config.getName(),e);
                return false;
            }
        } else {
            log.error("could not create list chain; embedded Stanbol not available");
            return false;
        }

    }

    /**
     * Asynchronously install the referenced site passed as argument. Will start a separate thread and return.
     *
     * @param configuration
     */
    @Override
    public void createReferencedSite(ReferencedSiteConfiguration configuration) {
        // Thread is started in the constructor
        new StanbolIndexDownloader(configuration);
    }

    /**
     * Delete the managed site with the ID given as argument.
     *
     * @param siteId
     * @return
     */
    @Override
    public boolean deleteManagedSite(String siteId) {
        ConfiguratorService configurator = slingLauncherService.getConfiguratorService();

        if(configurator != null) {
            try {
                configurator.deleteManagedSite(siteId);
                return true;
            } catch (ConfiguratorException e) {
                log.error("error while deleting managed site {}",siteId,e);
                return false;
            }
        } else {
            log.error("could not create list chain; embedded Stanbol not available");
            return false;
        }
    }

}
