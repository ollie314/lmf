/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.clerezza;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.apache.clerezza.rdf.core.*;
import org.apache.clerezza.rdf.core.event.AddEvent;
import org.apache.clerezza.rdf.core.event.RemoveEvent;
import org.apache.clerezza.rdf.core.impl.AbstractMGraph;
import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public abstract class AbstractSesameMGraph extends AbstractMGraph {

    private static final Logger logger = LoggerFactory.getLogger(AbstractSesameMGraph.class);

    /**
     * An optional context used when filtering on the repository.
     */
    private org.openrdf.model.Resource context = null;


    /**
     *  Bidirectional map for managing the conversion from
     *  sesame blank nodes to triamodel blank nodes and vice versa.
     */
    private final BiMap<BNode, org.openrdf.model.BNode> tria2SesameBNodes;

    private ValueFactory valueFactory;

    protected AbstractSesameMGraph(ValueFactory valueFactory, org.openrdf.model.Resource context ) {
        this.valueFactory = valueFactory;
        tria2SesameBNodes = HashBiMap.create();
        this.context      = context;
    }

    /**
     * Aqcuire the repository connection used by this repository, possibly creating a lock to avoid
     * concurrent access to the connection.
     *
     * @return
     */
    protected abstract RepositoryConnection aqcuireConnection();

    /**
     * Release the repository connection used by this repository, possibly releasing resources
     * occupied by the connection.
     *
     * @return
     */
    protected abstract void releaseConnection(RepositoryConnection connection);






    @Override
    public int size() {
        try {
            RepositoryConnection con = aqcuireConnection();
            try {
                if(context == null) {
                    return (int) con.size();
                } else {
                    return (int) con.size(context);
                }
            } finally {
                con.commit();
                releaseConnection(con);
            }
        } catch (RepositoryException re) {
            logger.warn("RepositoryException: {}", re);
            throw new RuntimeException(re);
        }
    }

    @Override
    public void clear() {
        try {
            RepositoryConnection connection = aqcuireConnection();
            Iterator<Triple> i = iterator();
            while (i.hasNext()) {
                Triple triple = i.next();
                remove(triple, connection);
                dispatchEvent(new RemoveEvent(this, triple));
            }

            connection.commit();
            releaseConnection(connection);
        } catch (RepositoryException re) {
            logger.warn("RepositoryException: {}", re);
            throw new RuntimeException(re);
        }
    }

    @Override
    public boolean addAll(Collection<? extends Triple> c) {
        try {
            RepositoryConnection connection = aqcuireConnection();
            boolean added = false;
            try {
                for (Triple t : c) {
                    boolean addedThis = add(t, connection);
                    if (addedThis) {
                        dispatchEvent(new AddEvent(this, t));
                        added = true;
                    }
                }
                connection.commit();
            } finally {
                releaseConnection(connection);
            }
            return added;
        } catch (RepositoryException re) {
            logger.warn("RepositoryException: {}", re);
            throw new RuntimeException(re);
        }
    }

    @Override
    public Iterator<Triple> performFilter(NonLiteral subject, UriRef predicate, Resource object) {
        try {
            org.openrdf.model.Resource sesameSubject = getSesameResourceForNonLiteral(subject);
            org.openrdf.model.URI sesamePredicate = getSesameURIForUriRef(predicate);
            org.openrdf.model.Value sesameObject = getSesameValueForResource(object);

            RepositoryConnection con = aqcuireConnection();
            try {
                RepositoryResult<Statement> statements;

                if(context == null) {
                    statements = con.getStatements(sesameSubject, sesamePredicate, sesameObject, true);
                } else {
                    statements = con.getStatements(sesameSubject, sesamePredicate, sesameObject, true, context);
                }
                return repositoryResultToTriple(statements);
            } finally {
                // commit connection to suppress warnings
                con.commit();
                releaseConnection(con);
            }
        } catch (RepositoryException re) {
            logger.warn("RepositoryException: {}", re);
            throw new RuntimeException(re);
        }
    }

    @Override
    public boolean performAdd(Triple e) {
        try {
            RepositoryConnection con = aqcuireConnection();
            try {
                boolean result = add(e, con);
                con.commit();
                return result;
            } finally {
                releaseConnection(con);
            }
        } catch (RepositoryException re) {
            logger.warn("RepositoryException: {}", re);
            throw new RuntimeException(re);
        }
    }

    private boolean contains(Triple t, RepositoryConnection con) throws RepositoryException {
        org.openrdf.model.Resource subject = getSubjectFromTriple(t);
        org.openrdf.model.URI predicate = getPredicateFromTriple(t);
        org.openrdf.model.Value object = getObjectFromTriple(t);
        return con.hasStatement(subject,predicate,object,true);
    }

    private boolean add(Triple e, RepositoryConnection con) throws RepositoryException {
        if (!contains(e,con)) {
            org.openrdf.model.Resource subject = getSubjectFromTriple(e);
            org.openrdf.model.URI predicate = getPredicateFromTriple(e);
            org.openrdf.model.Value object = getObjectFromTriple(e);
            if(context == null) {
                con.add(subject, predicate, object);
            } else {
                con.add(subject,predicate,object,context);
            }
            return true;
        }
        return false;
    }


    @Override
    public boolean performRemove(Triple o) {
        try {
            RepositoryConnection con = aqcuireConnection();
            try {
                boolean result = remove(o, con);
                con.commit();
                return result;
            } finally {
                releaseConnection(con);
            }
        } catch (RepositoryException re) {
            logger.warn("RepositoryException: {}", re);
            throw new RuntimeException(re);
        }
    }

    private boolean remove(Object o, RepositoryConnection con) throws RepositoryException {
        if (contains((Triple)o,con)) {
            try {
                Triple t = (Triple) o;
                org.openrdf.model.Resource subject = getSubjectFromTriple(t);
                org.openrdf.model.URI predicate = getPredicateFromTriple(t);
                org.openrdf.model.Value object = getObjectFromTriple(t);
                if(context == null) {
                    con.remove(subject, predicate, object);
                } else {
                    con.remove(subject, predicate, object, context);
                }
            } catch (RepositoryException re) {
                logger.warn("RepositoryException: {}", re);
                throw new RuntimeException(re);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> col) {
        if (col == null) {
            return false;
        }
        boolean result = false;
        try {
            RepositoryConnection con = aqcuireConnection();
            try {
                for (Object o : col) {
                    boolean trippleRemoved = remove(o, con);
                    if (trippleRemoved) {
                        result = true;
                        dispatchEvent(new RemoveEvent(this, (Triple) o));
                    }
                }
                con.commit();
            } finally {
                releaseConnection(con);
            }
        } catch (RepositoryException re) {
            logger.warn("RepositoryException: {}", re);
            throw new RuntimeException(re);
        }
        return result;
    }


    /**
     *  convert openrdf resource to clerezza
     */
    protected NonLiteral getNonLiteralFromSesameResource(org.openrdf.model.Resource r) {

        if (r instanceof org.openrdf.model.BNode) {
            org.openrdf.model.BNode sesameBNode = (org.openrdf.model.BNode) r;
            return getBNodeFromSesameBNode(sesameBNode);
        } else {
            //r is a uri
            URI uri = (URI) r;
            return getUriRefFromSesameUri(uri);
        }
    }

    /**
     *  create or get bnode for sesame bnode
     */
    protected org.apache.clerezza.rdf.core.BNode getBNodeFromSesameBNode(org.openrdf.model.BNode sesameBNode) {
        return new SesameBNode(sesameBNode);
    }

    /**
     *  convert openrdf uri
     */
    protected static UriRef getUriRefFromSesameUri(org.openrdf.model.URI uri) {
        return new SesameUriRef(uri);
    }

    /**
     * convert openrdf value to clerezza resource
     */
    protected Resource getResourceFromSesameValue(org.openrdf.model.Value value) {
        if (value instanceof org.openrdf.model.Literal) {
            return getLiteralFromSesameLiteral((org.openrdf.model.Literal) value);
        } else {
            return getNonLiteralFromSesameResource((org.openrdf.model.Resource) value);
        }
    }

    protected Resource getLiteralFromSesameLiteral(org.openrdf.model.Literal literal) {
        return new SesameLiteral(literal);
    }


    protected org.openrdf.model.Resource getSubjectFromTriple(Triple t) {
        return getSesameResourceForNonLiteral(t.getSubject());
    }

    protected org.openrdf.model.Resource getSesameResourceForNonLiteral(NonLiteral nl) {

        if (nl == null) {
            return null;
        }

        if(nl instanceof SesameBNode) {
            return ((SesameBNode) nl).getBNode();
        } else if(nl instanceof SesameUriRef) {
            return ((SesameUriRef) nl).getUri();
        } else  if (org.apache.clerezza.rdf.core.BNode.class.isAssignableFrom(nl.getClass())) {
            org.apache.clerezza.rdf.core.BNode clerezzaBNode = (org.apache.clerezza.rdf.core.BNode) nl;
            org.openrdf.model.BNode bNode = tria2SesameBNodes.get(clerezzaBNode);

            if (bNode == null) {
                bNode = valueFactory.createBNode();
                tria2SesameBNodes.put(clerezzaBNode, bNode);
            }

            return bNode;
        } else {

            //it is an uri
            String value = ((UriRef) nl).getUnicodeString();
            return valueFactory.createURI(value);
        }

    }

    protected org.openrdf.model.URI getPredicateFromTriple(Triple t) {
        return getSesameURIForUriRef(t.getPredicate());
    }

    protected org.openrdf.model.URI getSesameURIForUriRef(UriRef ref) {
        if (ref == null) {
            return null;
        }

        if(ref instanceof SesameUriRef) {
            return ((SesameUriRef) ref).getUri();
        }

        return valueFactory.createURI(ref.getUnicodeString());
    }

    protected org.openrdf.model.Value getObjectFromTriple(Triple t) {
        return getSesameValueForResource(t.getObject());
    }

    protected org.openrdf.model.Value getSesameValueForResource(Resource resource) {

        if (resource == null) {
            return null;
        }

        //a clerezza resource is either a non literal or a a literal
        if (NonLiteral.class.isAssignableFrom(resource.getClass())) {
            return getSesameResourceForNonLiteral((NonLiteral) resource);
        } else {

            //its a literal
            if(resource instanceof SesameLiteral) {
                return ((SesameLiteral) resource).getSesameLiteral();
            } else if (resource instanceof PlainLiteral) {
                PlainLiteral l = (PlainLiteral) resource;
                final Language language = l.getLanguage();
                if (language == null) {
                    return valueFactory.createLiteral(l.getLexicalForm());
                }
                return valueFactory.createLiteral(l.getLexicalForm(), language.toString());
            } else {
                TypedLiteral l = (TypedLiteral) resource;
                return valueFactory.createLiteral(l.getLexicalForm(),
                        new URIImpl(l.getDataType().getUnicodeString()));
            }
        }
    }

    protected Iterator<Triple> repositoryResultToTriple(RepositoryResult<Statement> statements) throws RepositoryException {
        List<Triple> triples = new ArrayList<Triple>();

        while(statements.hasNext()) {
            triples.add(new SesameTriple(statements.next()));
        }
        statements.close();

        //the following ensures that the remove-method acts on the MGraph
        final Iterator<Triple> convertedIter =  triples.iterator();
        return new Iterator<Triple>() {

            Triple lastReturned = null;

            @Override
            public boolean hasNext() {
                return convertedIter.hasNext();
            }

            @Override
            public Triple next() {
                lastReturned = convertedIter.next();
                return lastReturned;
            }

            @Override
            public void remove() {
                if (lastReturned == null) {
                    throw new IllegalStateException();
                }
                performRemove(lastReturned);
                lastReturned = null;
            }

        };
    }

    /**
     * An efficient wrapper class implementation to wrap Sesame Literals in Clerezza Literals
     */
    private static class SesameLiteral implements TypedLiteral, PlainLiteral {

        private Literal l;

        private SesameLiteral(Literal l) {
            this.l = l;
        }

        @Override
        public Language getLanguage() {
            String languageString = l.getLanguage();
            return languageString == null ? null : new Language(languageString);
        }

        /**
         * Returns the data type which is a UriRef.
         * Note that the return value is not a node in the graph
         *
         * @return UriRef
         */
        @Override
        public UriRef getDataType() {
            if(l.getDatatype() != null) {
                return getUriRefFromSesameUri(l.getDatatype());
            } else {
                return null;
            }
        }

        /**
         * @return the text of this literal
         */
        @Override
        public String getLexicalForm() {
            return l.stringValue();
        }

        /**
         * Get the wrapped Sesame Literal
         * @return
         */
        public Literal getSesameLiteral() {
            return l;
        }

        @Override
        public int hashCode() {
            // unfortunately we need to copy the stupid implementation of Clerezza ...

            int hash = getLexicalForm().hashCode();

            if (l.getLanguage() != null) {
                hash += getLanguage().hashCode();
            }

            if(l.getDatatype() != null) {
                hash += getDataType().hashCode();
            }
            return hash;

        }

        @Override
        public boolean equals(Object obj) {
            if(this == obj) {
                return true;
            }

            if(obj == null) {
                return false;
            }

            if(! (obj instanceof org.apache.clerezza.rdf.core.Literal)) {
                return false;
            }

            if(! ((org.apache.clerezza.rdf.core.Literal)obj).getLexicalForm().equals(this.getLexicalForm())) {
                return false;
            }

            if(obj instanceof TypedLiteral) {
                TypedLiteral t = (TypedLiteral) obj;


                if(t.getDataType() == null && this.getDataType() != null) {
                    return false;
                }

                if(t.getDataType() != null && !t.getDataType().equals(this.getDataType())) {
                    return false;
                }
            }

            if(obj instanceof PlainLiteral) {
                PlainLiteral p = (PlainLiteral) obj;

                if(p.getLanguage() == null && this.getLanguage() != null) {
                    return false;
                }

                if(p.getLanguage() != null && !p.getLanguage().equals(this.getLanguage())) {
                    return false;
                }
            }

            return true;
        }
    }

    /**
     * An efficient wrapper around Sesame BNodes that avoids creating an (unnecessary) mapping table
     */
    private static class SesameBNode extends BNode {

        private org.openrdf.model.BNode bnode;

        private SesameBNode(org.openrdf.model.BNode bnode) {
            this.bnode = bnode;
        }

        public org.openrdf.model.BNode getBNode() {
            return bnode;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SesameBNode that = (SesameBNode) o;

            if (!bnode.equals(that.bnode)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return bnode.hashCode();
        }
    }

    /**
     * An efficient implementation of a Clerezza UriRef wrapping a Sesame URI.
     */
    private static class SesameUriRef extends UriRef {

        private org.openrdf.model.URI uri;

        private SesameUriRef(URI uri) {
            super(uri.stringValue());
            this.uri = uri;
        }


        public URI getUri() {
            return uri;
        }
    }

    /**
     * An efficient implementation wrapping Sesame Statements as Clerezza Triples
     */
    private class SesameTriple implements Triple {

        private Statement stmt;

        private SesameTriple(Statement stmt) {
            this.stmt = stmt;
        }

        @Override
        public Resource getObject() {
            return getResourceFromSesameValue(stmt.getObject());
        }

        @Override
        public NonLiteral getSubject() {
            return getNonLiteralFromSesameResource(stmt.getSubject());
        }

        @Override
        public UriRef getPredicate() {
            return getUriRefFromSesameUri(stmt.getPredicate());
        }

        public Statement getStatement() {
            return stmt;
        }
    }
}
