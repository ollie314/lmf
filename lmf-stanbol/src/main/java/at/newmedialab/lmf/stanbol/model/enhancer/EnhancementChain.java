/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.enhancer;

/**
 * Description of an Enhancement Chain of the Stanbol enhancer
 * <p/>
 * Author: Sebastian Schaffert
 */
public class EnhancementChain {

    /** the URI for accessing this enhancement chain */
    private String endpoint;

    /** the label of this enhancement chain */
    private String name;

    /** whether this enhancement chain is the default chain or not */
    private boolean defaultChain;

    /** The profile that was used for creating this list chain configuration */
    private String inheritedProfile;

    /** The context (named graph) this enhancement chain is attached to */
    private String context;


    public EnhancementChain(String name, String endpoint) {
        this.endpoint = endpoint;
        this.name = name;
        this.defaultChain = false;
    }

    public EnhancementChain(String name, String endpoint, boolean defaultChain) {
        this.endpoint = endpoint;
        this.name = name;
        this.defaultChain = defaultChain;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDefaultChain() {
        return defaultChain;
    }

    public void setDefaultChain(boolean defaultChain) {
        this.defaultChain = defaultChain;
    }

    public String getInheritedProfile() {
        return inheritedProfile;
    }

    public void setInheritedProfile(String inheritedProfile) {
        this.inheritedProfile = inheritedProfile;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}
