/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.webservices;

import at.newmedialab.lmf.stanbol.api.*;
import at.newmedialab.lmf.stanbol.exception.ChainAlreadyExistsException;
import at.newmedialab.lmf.stanbol.exception.EngineAlreadyExistsException;
import at.newmedialab.lmf.stanbol.model.config.*;
import at.newmedialab.lmf.stanbol.model.enhancer.Enhancement;
import at.newmedialab.lmf.stanbol.model.enhancer.EnhancementChain;
import at.newmedialab.lmf.stanbol.model.enhancer.EntityReference;
import at.newmedialab.lmf.stanbol.model.entityhub.StanbolSiteDescription;
import net.sf.ehcache.Ehcache;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.marmotta.commons.sesame.repository.ResourceUtils;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.triplestore.ContextService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.qualifiers.cache.MarmottaCache;
import org.apache.stanbol.enhancer.topic.api.ClassifierException;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
@Path("/stanbol")
public class StanbolWebService {

    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private StanbolService stanbolService;

    @Inject
    private StanbolSiteService entityService;

    @Inject
    private StanbolEnhancerService enhancerService;

    @Inject
    private StanbolConfigurationService stanbolConfigurationService;


    @Inject
    private StanbolSyncService stanbolSyncService;

    @Inject
    private SesameService sesameService;

    @Inject
    private ContextService contextService;

    @Inject
    private SlingLauncherService slingLauncherService;

    @Inject
    private TopicClassifierService topicClassifierService;

    @Inject @MarmottaCache("stanbol-cache")
    private Ehcache stanbolCache;



    @Path("/ping")
    @GET
    public Response checkAvailable() {
        if(!stanbolService.ping()) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Stanbol server not available").build();
        } else {
            return Response.ok("Stanbol server is ready").build();
        }
    }


    /**
     * List the currently installed enhancers, optionally giving a context URI to filter only enhancers associated
     * with the managed site of the context passed as argument. In this case, the method will "normalize" the name
     * back to not include the site id.
     *
     * @param context
     * @return
     * @throws IOException
     */
    @Path("/enhancers")
    @GET
    @Produces("application/json")
    public Response getEnhancers(@QueryParam("context") String context) throws IOException {
        if(!stanbolService.ping()) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Stanbol server not available").build();
        }


        try {
            RepositoryConnection conn = sesameService.getConnection();

            try {
                URI rcontext = null;
                if(context != null) {
                    rcontext = ResourceUtils.getUriResource(conn, context);
                    if(rcontext == null) {
                        return Response.status(Response.Status.NOT_FOUND).entity("the context with URI "+context+" does not exist").build();
                    }
                    if(!stanbolSyncService.isPublished(rcontext)) {
                        return Response.status(Response.Status.NOT_FOUND).entity("the context with URI "+context+" is not synchronized with Stanbol").build();
                    }
                }

                // site id in case the context is given
                String siteId = context != null && rcontext != null ? stanbolSyncService.getSiteName(rcontext) : null;

                List<Map<String,Object>> result = new ArrayList<Map<String, Object>>();
                for(EnhancementChain enhancer : stanbolConfigurationService.getEnhancementChains()) {

                    Map<String,Object> enhancerMap = new HashMap<String, Object>();
                    enhancerMap.put("endpoint",enhancer.getEndpoint());
                    enhancerMap.put("default",enhancer.isDefaultChain());
                    enhancerMap.put("name",enhancer.getName());
                    if(enhancer.getContext() != null) {
                        enhancerMap.put("context",enhancer.getContext());
                    }
                    if(enhancer.getInheritedProfile() != null) {
                        enhancerMap.put("profile", enhancer.getInheritedProfile());
                    }

                    // filter by site id / context
                    if(siteId == null || enhancer.getName().endsWith(StringUtils.capitalize(siteId))) {
                        result.add(enhancerMap);
                    } // else ignore
                }
                return Response.status(Response.Status.OK).entity(result).build();
            } finally {
                conn.commit();
                conn.close();
            }
        } catch(RepositoryException ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }

    }


    @Path("/enhancers/available")
    @GET
    @Produces("application/json")
    public Response getAvailableEnhancers() throws IOException {
        if(!stanbolService.ping()) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Stanbol server not available").build();
        }

        List<Map<String,Object>> result = new ArrayList<Map<String, Object>>();
        for(StanbolChainConfiguration enhancer : stanbolConfigurationService.getAvailableEnhancementChains().values()) {
            Map<String,Object> enhancerMap = new HashMap<String, Object>();
            enhancerMap.put("name",enhancer.getId());
            enhancerMap.put("description",enhancer.getDescription());
            enhancerMap.put("engines",enhancer.getEngines());
            result.add(enhancerMap);
        }
        return Response.status(Response.Status.OK).entity(result).build();

    }

    /**
     * Create a new enhancement configuration using the profile given as argument. In case the argument "context" is
     * given, the enhancement chain will use the given LMF context as backend for lookups; otherwise, it will use
     * the manually maintained entity hub.
     * <p/>
     * The following profiles are currently available:
     * <ul>
     * <li>SKOSEnhancer: creates a SKOSEnhancer, supporting keyword enhancement using skos:prefLabel and skos:altLabel from the entityhub</li>
     * <li>DBPediaEnhancer: creates a DBPediaEnhancer, supporting named entity enhancement using DBPedia as backend site</li>
     * <li>RDFSEnhancer: creates a RDFSEnhancer, supporting keyword enhancement using rdfs:label from the entityhub or managed site</li>
     * </ul>
     *
     * @param profile the name of the profile to use (e.g. SKOSEnhancer, ...)
     * @param name    the name to use for the enhancement chain to create (optional, in this case will generate the name out of profile and context)
     * @param context the URI of the context to use for lookups in this enhancement configuration
     *                (optional, in this case will use the standard entityhub)
     *
     * @return
     */
    @POST
    @Path("/enhancers/create")
    public Response createEnhancementConfiguration(@QueryParam("profile") String profile, @QueryParam("name") String name, @QueryParam("context") String context) {

        try {
            Map<String,StanbolChainConfiguration> configurations = stanbolConfigurationService.getAvailableEnhancementChains();
            StanbolChainConfiguration config = configurations.get(profile);

            if(config != null) {
                RepositoryConnection conn = sesameService.getConnection();

                try {
                    URI rcontext = null;
                    if(context != null) {
                        rcontext =  ResourceUtils.getUriResource(conn,context);
                        if(rcontext == null) {
                            return Response.status(Response.Status.NOT_FOUND).entity("the context with URI "+context+" does not exist").build();
                        }
                        if(!stanbolSyncService.isPublished(rcontext)) {
                            return Response.status(Response.Status.NOT_FOUND).entity("the context with URI "+context+" is not synchronized with Stanbol").build();
                        }
                    }


                    if(name != null) {
                        config.setId(name);
                    }
                    if(context != null) {
                        config.setContext(context);
                    }

                    // first create all needed engines
                    Map<String,StanbolEngineConfiguration> engines = stanbolConfigurationService.getAvailableEnhancementEngines();
                    List<String> newEngines = new ArrayList<String>();
                    for(String engine : config.getEngines()) {
                        StanbolEngineConfiguration engineConfiguration = engines.get(engine);

                        if(engineConfiguration != null) {
                            // if context is given and the site used by the engine is "entityhub", create an engine configuration
                            // only for this context with the synchronized managed site as backend and a newly generated name
                            if(context != null && engineConfiguration.getSite().equals("entityhub")) {
                                engineConfiguration.setSite(stanbolSyncService.getSiteName(rcontext));
                                engineConfiguration.setId(engineConfiguration.getId() + StringUtils.capitalize(stanbolSyncService.getSiteName(rcontext)));
                            }

                            // create engine if it does not exist yet
                            try {
                                if(engineConfiguration instanceof KeywordLinkingEngineConfig) {
                                    stanbolConfigurationService.createKeywordLinkingEngine((KeywordLinkingEngineConfig) engineConfiguration);
                                } else if(engineConfiguration instanceof EntityhubLinkingEngineConfig) {
                                    stanbolConfigurationService.createEntityhubLinkingEngine((EntityhubLinkingEngineConfig) engineConfiguration);
                                } else if(engineConfiguration instanceof EntityTaggingEngineConfig) {
                                    stanbolConfigurationService.createEntityTaggingEngine((EntityTaggingEngineConfig) engineConfiguration);
                                } else if(engineConfiguration instanceof TopicClassificationEngineConfig) {
                                    stanbolConfigurationService.createTopicClassificationEngine((TopicClassificationEngineConfig) engineConfiguration);
                                } else {
                                    log.warn("not creating engine {}; type not supported",engineConfiguration.getId());
                                }
                            } catch (EngineAlreadyExistsException ex) {
                                log.info("not creating engine {}, it already exists",engine);
                            }

                            newEngines.add(engineConfiguration.getId());
                        } else {
                            // else: might be a stanbol internal engine like langid or ner ...
                            newEngines.add(engine);
                        }
                    }
                    config.setEngines(newEngines);

                    // update the chain name in case it is associated with a specific context
                    if(rcontext != null && name == null) {
                        config.setId(config.getId() + StringUtils.capitalize(stanbolSyncService.getSiteName(rcontext)));
                    }

                    // then try creating the chain
                    if(config instanceof ListChainConfiguration) {
                        stanbolConfigurationService.createListChain((ListChainConfiguration) config);
                    } else {
                        log.warn("not creating chain {}; type not supported",config.getId());
                    }

                    stanbolService.flushCache();

                    return Response.ok("enhancement configuration created").build();
                } finally {
                    conn.commit();
                    conn.close();
                }
            } else {
                return Response.status(Response.Status.NOT_FOUND).entity("profile with name "+profile+" does not exist").build();
            }

        } catch (ChainAlreadyExistsException e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        } catch (RepositoryException ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }



    @Path("/enhance")
    @POST
    @Produces("application/json")
    public Response executeEnhancer(@QueryParam("chain") String chain, @Context HttpServletRequest request) throws IOException {
        if(!stanbolService.ping()) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Stanbol server not available").build();
        }

        String text = IOUtils.toString(request.getInputStream());

        if(text != null && !"".equals(text)) {
            List<Map<String,Object>> result = new ArrayList<Map<String, Object>>();
            List<Enhancement> enhancements;
            if(chain == null) {
                enhancements = enhancerService.getEnhancements(text);
            } else {
                enhancements = enhancerService.getEnhancements(text,chain);
            }
            for(Enhancement enhancement : enhancements) {
                Map<String, Object> eMap = new HashMap<String, Object>();
                eMap.put("selectedText",enhancement.getSelectedText());
                eMap.put("confidence",enhancement.getConfidence());
                eMap.put("startPosition",enhancement.getStartPosition());
                eMap.put("endPosition", enhancement.getEndPosition());
                if(enhancement.getType() != null)
                    eMap.put("type", enhancement.getType().toString());

                List<Map<String,Object>> entities = new ArrayList<Map<String, Object>>();
                for(EntityReference reference : enhancement.getEntities()) {
                    Map<String,Object> rMap = new HashMap<String, Object>();
                    rMap.put("uri",reference.getReference().toString());
                    rMap.put("confidence",reference.getConfidence());
                    rMap.put("types",reference.getTypes());
                    entities.add(rMap);
                }
                eMap.put("entities",entities);
                result.add(eMap);
            }
            return Response.status(Response.Status.OK).entity(result).build();
        } else {
            return Response.status(Response.Status.PRECONDITION_FAILED).entity("no text given for enhancement").build();
        }


    }


    @Path("/sites")
    @GET
    @Produces("application/json")
    public Response getSites(@QueryParam("uri") String uri) throws IOException {
        if(!stanbolService.ping()) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Stanbol server not available").build();
        }

        if(uri == null) {
            List<Map<String,Object>> results = new ArrayList<Map<String, Object>>();
            for(String endpoint : entityService.listStanbolSiteUris()) {
                StanbolSiteDescription desc = entityService.getSiteDescription(endpoint, null);
                Map<String,Object> result = new HashMap<String,Object>();

                result.put("name",desc.getName());
                result.put("description", desc.getDescription());
                result.put("local", ""+desc.isLocal());
                result.put("prefixes", desc.getEntityPrefixes());
                results.add(result);
            }

            return Response.status(Response.Status.OK).entity(results).build();
        } else {
            StanbolSiteDescription desc = entityService.getSiteDescription(uri, null);

            if(desc != null) {
                Map<String,Object> result = new HashMap<String,Object>();

                result.put("name",desc.getName());
                result.put("description", desc.getDescription());
                result.put("local", ""+desc.isLocal());
                result.put("prefixes", desc.getEntityPrefixes());

                return Response.status(Response.Status.OK).entity(result).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        }

    }


    @Path("/sites/available")
    @GET
    @Produces("application/json")
    public Response getAvailableSites() throws IOException {
        if(!stanbolService.ping()) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Stanbol server not available").build();
        }

        List<Map<String,Object>> results = new ArrayList<Map<String, Object>>();
        for(ReferencedSiteConfiguration desc : entityService.listInstallableSites()) {
            Map<String,Object> result = new HashMap<String,Object>();

            result.put("id",desc.getId());
            result.put("name",desc.getName());
            result.put("description", desc.getDescription());
            result.put("bundleUrl", desc.getBundleLocation().toString());
            result.put("indexUrl", desc.getIndexLocation().toString());
            result.put("size",formatSize(desc.getSize()));
            results.add(result);
        }

        return Response.status(Response.Status.OK).entity(results).build();

    }

    @Path("/sites/{siteid}")
    @POST
    public Response installSite(@PathParam("siteid") String siteid) throws IOException {
        if(!stanbolService.ping()) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Stanbol server not available").build();
        }

        if(siteid != null) {
            // look for site id in available sites, and check whether it was already installed previously
            for(String existing : entityService.listStanbolSiteUris()) {
                if(existing.equals(siteid)) {
                    return Response.status(Response.Status.BAD_REQUEST).entity("stanbol site "+siteid+" already exists").build();
                }
            }

            for(ReferencedSiteConfiguration config : entityService.listInstallableSites()) {
                if(config.getId().equals(siteid)) {
                    stanbolConfigurationService.createReferencedSite(config);
                    return Response.ok("stanbol site "+siteid+" installing; observe progress in task manager").build();
                }
            }
        }

        return Response.status(Response.Status.NOT_FOUND).entity("the stanbol site "+siteid+" was not found").build();
    }


    /**
     * List the status of the LMF contexts (optionally specify a context URI as parameter). The return structure is
     * a JSON list of the following form:
     * <code>
     *     [
     *       {
     *           label: "CONTEXT_LABEL",
     *           uri: "CONTEXT_URI",
     *           synchronized: true/false,
     *           site: "SITE NAME",
     *           endpoint: "SITE ENDPOINT"
     *       }
     *     ]
     * </code>
     *
     * @param uri optional URI of a context to get the status for
     * @return
     */
    @Path("/contexts/status")
    @GET
    @Produces("application/json")
    public Response listContexts(@QueryParam("uri") String uri) {
        if(!stanbolService.ping()) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Stanbol server not available").build();
        }

        List<Map<String,Object>> results = new ArrayList<Map<String, Object>>();

        if(uri == null) {
            // list all contexts

            for(org.openrdf.model.URI context : contextService.listContexts()) {
                boolean sync = stanbolSyncService.isPublished(context);

                Map<String,Object> result = new HashMap<String,Object>();

                result.put("uri",context.stringValue());
                result.put("label", contextService.getContextLabel(context));
                result.put("synchronized",sync);

                if(sync) {
                    result.put("site",     stanbolSyncService.getSiteName(context));
                    result.put("endpoint", stanbolSyncService.getSiteEndpoint(context));
                }

                results.add(result);
            }
        } else {
            URI context = contextService.createContext(uri);

            boolean sync = stanbolSyncService.isPublished(context);

            Map<String,Object> result = new HashMap<String,Object>();

            result.put("uri",context.stringValue());
            result.put("label", contextService.getContextLabel(context));
            result.put("synchronized",sync);

            if(sync) {
                result.put("site",     stanbolSyncService.getSiteName(context));
                result.put("endpoint", stanbolSyncService.getSiteEndpoint(context));
            }

            results.add(result);

        }

        return Response.status(Response.Status.OK).entity(results).build();
    }

    /**
     * Publish the context with the given URI to Stanbol. This method will check if the context exists and add it
     * to the configuration option stanbol.published_contexts, triggering the creation of a managed site and
     * automatic synchronization with Stanbol.
     *
     * @param uri URI of the context to publish
     * @return
     */
    @Path("/contexts/publish")
    @POST
    public Response publishContext(@QueryParam("uri") String uri) {
        if(uri != null && contextService.getContext(uri) != null) {
            List<String> enabledContexts = new ArrayList<String>(configurationService.getListConfiguration("stanbol.published_contexts"));

            if(!enabledContexts.contains(uri)) {
                enabledContexts.add(uri);
                configurationService.setListConfiguration("stanbol.published_contexts",enabledContexts);

                return Response.ok().build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("context is already published").build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    /**
     * Publish the context with the given URI to Stanbol. This method will check if the context exists and add it
     * to the configuration option stanbol.published_contexts, triggering the creation of a managed site and
     * automatic synchronization with Stanbol.
     *
     * @param uri URI of the context to publish
     * @return
     */
    @Path("/contexts/classifier")
    @POST
    public Response createContextClassifier(@QueryParam("uri") String uri) {
        if(uri != null && contextService.getContext(uri) != null) {
            List<String> enabledClassifiers = new ArrayList<String>(configurationService.getListConfiguration("stanbol.topic_contexts"));

            if(!enabledClassifiers.contains(uri)) {
                try {
                    topicClassifierService.createTopicClassifier(contextService.getContext(uri));
                    return Response.ok().build();
                } catch (ClassifierException e) {
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
                } catch (RepositoryException e) {
                    return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
                }

            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("context is already published").build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    /**
     * Stop publishing the context with the given URI to Stanbol. This method will check if the context exists and
     * remove it from the configuration option stanbol.published_contexts, triggering the deletion of the associated
     * managed site.
     *
     * @param uri URI of the context to stop publishing
     * @return
     */
    @Path("/contexts/unpublish")
    @POST
    public Response unpublishContext(@QueryParam("uri") String uri) {
        if(uri != null && contextService.getContext(uri) != null) {
            List<String> enabledContexts = configurationService.getListConfiguration("stanbol.published_contexts");

            if(enabledContexts.contains(uri)) {
                enabledContexts.remove(uri);
                configurationService.setListConfiguration("stanbol.published_contexts",enabledContexts);

                return Response.ok().build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity("context is not published").build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    /**
     * Flush the internal Stanbol enhancement cache
     *
     * @return
     */
    @POST
    @Path("/cache/flush")
    public Response flushCache() {
        stanbolService.flushCache();

        return Response.ok().entity("cache flushed successfully").build();
    }


    /**
     * Get the current cache size
     *
     * @return
     */
    @GET
    @Path("/cache/size")
    @Produces("application/json")
    public Response getCacheSize() {
        return Response.ok().entity(stanbolCache.getSize()).build();
    }

    /**
     * Get the current cache size
     *
     * @return
     */
    @GET
    @Path("/cache/expired")
    @Produces("application/json")
    public Response getCacheExpired() {
        return Response.ok().entity(stanbolCache.getLiveCacheStatistics().getExpiredCount()).build();
    }

    /**
     * Get the current cache size
     *
     * @return
     */
    @GET
    @Path("/cache/hits")
    @Produces("application/json")
    public Response getCacheHits() {
        return Response.ok().entity(stanbolCache.getLiveCacheStatistics().getCacheHitCount()).build();
    }

    /**
     * Get the current cache size
     *
     * @return
     */
    @GET
    @Path("/cache/misses")
    @Produces("application/json")
    public Response getCacheMisses() {
        return Response.ok().entity(stanbolCache.getLiveCacheStatistics().getCacheMissCount()).build();
    }

    /**
     * Get the current cache size
     *
     * @return
     */
    @GET
    @Path("/cache/statistics")
    @Produces("application/json")
    public Response getCacheStatistics() {
        Map<String,Integer> result = new HashMap<String, Integer>();
        result.put("size",stanbolCache.getSize());
        result.put("hits",Long.valueOf(stanbolCache.getLiveCacheStatistics().getCacheHitCount()).intValue());
        result.put("misses",Long.valueOf(stanbolCache.getLiveCacheStatistics().getCacheMissCount()).intValue());
        result.put("expired",Long.valueOf(stanbolCache.getLiveCacheStatistics().getExpiredCount()).intValue());

        return Response.ok().entity(result).build();
    }


    @GET
    @Path("/embedded/enhancers/list")
    @Produces("application/json")
    public Response getEmbeddedChains() {
        return Response.ok().entity(slingLauncherService.getChainManager().getActiveChainNames()).build();
    }



    private String formatSize(long size) {
        if(size >= 1024*1024*1024) {
            // GB
            return String.format("%.2f GB",(double)size/(1024*1024*1024));
        } else if(size >= 1024*1024) {
            // MB
            return String.format("%.2f MB",(double)size/(1024*1024));
        } else if(size > 1024) {
            // kB
            return String.format("%.2f kB",(double)size/1024);
        } else {
            return String.format("%d bytes",size);
        }

    }

}
