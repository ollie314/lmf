/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.services;

import at.newmedialab.lmf.stanbol.api.*;
import at.newmedialab.lmf.stanbol.exception.SiteAlreadyExistsException;
import at.newmedialab.lmf.stanbol.model.clerezza.SesameConnectionMGraph;
import at.newmedialab.lmf.stanbol.model.config.ManagedSiteConfiguration;
import at.newmedialab.stanbol.entityhub.api.EntityhubHelper;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.clerezza.rdf.core.UriRef;
import org.apache.commons.lang.StringUtils;
import org.apache.marmotta.commons.util.HashUtils;
import org.apache.marmotta.kiwi.transactions.model.TransactionData;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.task.Task;
import org.apache.marmotta.platform.core.api.task.TaskManagerService;
import org.apache.marmotta.platform.core.api.triplestore.ContextService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.events.ConfigurationChangedEvent;
import org.apache.marmotta.platform.core.events.SesameStartupEvent;
import org.apache.marmotta.platform.core.qualifiers.event.transaction.AfterCommit;
import org.apache.stanbol.entityhub.servicesapi.EntityhubException;
import org.apache.stanbol.entityhub.servicesapi.model.Representation;
import org.apache.stanbol.entityhub.servicesapi.site.ManagedSite;
import org.apache.stanbol.entityhub.servicesapi.site.Site;
import org.apache.stanbol.entityhub.servicesapi.site.SiteManager;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This service handles registration and automatic publishing of LMF contexts to Stanbol managed sites. It maintains
 * a list of published LMF contexts in the configuration variable stanbol.published_contexts (list of URIs).
 * <p/>
 * On startup, it checks whether a managed site exists for each published context; if not, a new site is created and
 * the complete RDF data of this context is published to Stanbol.
 * <p/>
 * On transaction commit, it lists all affected resources and updates the relevant triple data from the published
 * contexts to the managed sites.
 * <p/>
 * The names for the managed sites are derived from the context URI by building a hash (md5 or similar).
 *
 *
 *
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class StanbolSyncServiceImpl implements StanbolSyncService {

    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private StanbolService stanbolService;

    @Inject
    private StanbolConfigurationService stanbolConfigurationService;

    @Inject
    private StanbolSiteService stanbolEntityService;

    @Inject
    private ContextService contextService;


    @Inject
    private SesameService sesameService;

    @Inject
    private SlingLauncherService slingLauncherService;

    @Inject
    private TaskManagerService taskManagerService;

    private Set<URI> activatedContexts;

    private ReentrantLock siteCreatorLock;

    /**
     * Ensure that all enabled contexts have corresponding managed sites
     */
    public void initialize(@Observes SesameStartupEvent event) {
        log.info("Starting LMF-Stanbol Synchronization Service ...");


        siteCreatorLock = new ReentrantLock();

        activatedContexts = new HashSet<URI>();
        long start = System.currentTimeMillis();
        for(URI context : listEnabledContexts()) {
            if(isPublished(context)) {
                activateContext(context, false); // publish context, but do not enforce re-creation
            } else {
                // in case a new context is added via configuration, ensure to activate
                activateContext(context, true);
            }
        }

        log.info("LMF-Stanbol Synchronization started successfully ({} ms)",System.currentTimeMillis()-start);
    }



    /**
     * React on transaction commit and synchronize the transaction data of the published contexts.
     *
     * @param data
     */
    public void updateSites(@Observes @AfterCommit final TransactionData data) {
        final Map<URI,Set<URI>> resourceMap = new HashMap<URI, Set<URI>>();

        for(URI context : activatedContexts) {
            Set<URI> resources = new HashSet<URI>();

            for(Statement triple : data.getAddedTriples().listTriples(null,null,null,context, false)) {
                if(triple.getSubject() instanceof URI) {
                    resources.add((URI) triple.getSubject());
                }
            }
            for(Statement triple : data.getRemovedTriples().listTriples(null, null, null, context, false)) {
                if(triple.getSubject() instanceof URI) {
                    resources.add((URI) triple.getSubject());
                }
            }

            if(resources.size() > 0) {
                resourceMap.put(context,resources);
            }
        }

        if(resourceMap.size() > 0) {
            // this can be done asynchronously
            Runnable runner = new Runnable() {
                @Override
                public void run() {
                    Task task = taskManagerService.createTask("Stanbol Synchronizer", "Stanbol");
                    try {
                        task.updateMessage("synchronizing resources");
                        task.updateTotalSteps(resourceMap.size());
                        long ctxCount = 0;
                        for(URI context : resourceMap.keySet()) {
                            task.updateDetailMessage("context", context.stringValue());
                            Task subTask = taskManagerService.createSubTask("Stanbol Context Synchronizer", "Stanbol");
                            try {
                                subTask.updateMessage("synchronizing context <" + context.stringValue() + ">");

                                Set<URI> resources = resourceMap.get(context);
                                log.info("synchronizing {} resources for context {}",resources.size(),context.stringValue());
                                subTask.updateTotalSteps(resources.size());
                                long start = System.currentTimeMillis();

                                long rscCount = 0;
                                for(URI resource : resources) {
                                    publishResourceContext(resource,context);
                                    subTask.updateProgress(++rscCount);
                                }
                                log.info("synchronized after {} ms", System.currentTimeMillis()-start);
                            } finally {
                                subTask.endTask();
                            }
                            task.updateProgress(++ctxCount);
                        }
                    } finally {
                        task.endTask();
                    }

                }
            };
            Thread t = new Thread(runner,"Stanbol Synchronizer");
            t.setDaemon(true);
            t.start();
        }
    }


    /**
     * Activate/deactivate managed sites in case the configuration is updated
     * @param event
     */
    public void configurationChangedEvent(@Observes ConfigurationChangedEvent event) {
        if (event.containsChangedKey("stanbol.published_contexts")) {
            List<URI> enabled = listEnabledContexts();

            // activate contexts that have been added
            for(URI context : enabled) {
                if(!isPublished(context)) {
                    activateContext(context,true);
                }
            }

            // deactivate contexts that have been removed
            for(URI context : new HashSet<URI>(activatedContexts)) {
                if(!enabled.contains(context)) {
                    deactivateContext(context,true);
                }
            }
        }
    }

    /**
     * Return a list of URIs of the contexts that are published as managed sites from the system configuration.
     * <p/>
     * Note that this method does not check whether the contexts are also available in Stanbol. See
     * listPublishedContexts() instead.
     *
     * @return
     */
    @Override
    public List<URI> listEnabledContexts() {
        List<String> enabledContexts = configurationService.getListConfiguration("stanbol.published_contexts");

        return Lists.transform(enabledContexts, new Function<String, URI>() {
            @Override
            public URI apply(String input) {
                return contextService.createContext(input);
            }
        });
    }

    /**
     * Return a list of URIs of the contexts that are actually published as managed sites in Stanbol.
     *
     * @return
     */
    @Override
    public List<URI> listPublishedContexts() {

        // build intersection between all contexts and managed sites
        List<String> stanbolSites = stanbolEntityService.listStanbolSiteUris();


        List<URI> result = new ArrayList<URI>();

        for(URI context : contextService.listContexts()) {
            for(String site : stanbolSites) {
                if(site.equals(getSiteName(context)) || site.endsWith(getSiteName(context)+"/")) {
                    result.add(context);
                }
            }
        }
        return result;
    }

    /**
     * Return true if the context given as argument is published as managed site in Stanbol.
     * ‚
     *
     *
     * @param context
     * @return
     */
    @Override
    public boolean isPublished(URI context) {
        for(String site : stanbolEntityService.listStanbolSiteIds()) {
            if(site.equals(getSiteName(context))) return true;
        }
        return false;
    }



    /**
     * Publish the context with the given URI as Stanbol managed site. If the boolean argument "create" is true,
     * the managed site will be (re-)created and the data republished from the data contained in the LMF. Otherwise,
     * this method will just register the context for syncing on transaction commit.
     *
     * @param uri
     * @param create
     */
    @Override
    public void activateContext(final URI uri, boolean create) {
        final String siteId = generateSiteName(uri.stringValue());

        log.info("activating synchronization of context {}; (siteid={}, create={})", uri, siteId, create);

        if(create) {
            if(isPublished(uri)) {
                stanbolConfigurationService.deleteManagedSite(siteId);
            }
            try {
                stanbolConfigurationService.createManagedSite(new ManagedSiteConfiguration(siteId,"LMF Context "+uri.stringValue(),"Autogenerated by LMF Stanbol Synchronization",new HashMap<String, String>()));
            } catch (SiteAlreadyExistsException e) {
                log.error("cannot create managed site for context; aborting publication of context {}",uri,e);
            }

            // wait until site becomes available
            while(!isPublished(uri)) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }


            Runnable runner = new Runnable() {
                @Override
                public void run() {
                    Task task = taskManagerService.createTask("Stanbol Initial Publisher", "Stanbol");

                    // get the URI of the site generated thus and publish all triples of this context to it
                    try {
                        task.updateMessage("publishing context " + uri.stringValue() + " to Stanbol");
                        task.updateDetailMessage("context", uri.stringValue());
                        while(!publishContext(uri, task)) {
                            log.info("stanbol site {} not yet available, retrying in 1 second ...", siteId);
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                            }
                        };
                    } finally {
                        task.endTask();
                    }
                }
            };
            Thread t = new Thread(runner);
            t.setDaemon(true);
            t.start();
        }

        activatedContexts.add(uri);
    }

    /**
     * Stop publishing the context with the given URI as Stanbol managed site; If the boolean argument "destroy" is true,
     * the managed site will be destroyed as well. Otherwise, it is just removed from the internal list for transaction
     * notification.
     *
     * @param uri
     * @param destroy
     */
    @Override
    public void deactivateContext(URI uri, boolean destroy) {
        String siteId = generateSiteName(uri.stringValue());

        log.info("deactivating synchronization of context {}; (siteid={}, destroy={})", uri, siteId, destroy);

        if(destroy && isPublished(uri)) {
            stanbolConfigurationService.deleteManagedSite(siteId);
        }

        activatedContexts.remove(uri);
    }


    /**
     * Return the URL of the managed site for this context. Does not check for existence, just creates the URL.
     *
     * @param uri
     * @return
     */
    @Override
    public String getSiteEndpoint(URI uri) {
        return stanbolService.getStanbolUri()+"/entityhub/site/"+getSiteName(uri);
    }

    /**
     * Return the name of the managed site for this context. Does not check for existence, just creates the name.
     *
     * @param uri
     * @return
     */
    @Override
    public String getSiteName(URI uri) {
        return "lmfSite"+StringUtils.capitalize(generateSiteName(uri.toString()));
    }


    private String generateSiteName(String context) {
        /*
        try {
            return URLEncoder.encode(context.toString(),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("unsupported encoding UTF-8, cannot happen",e);
            return null;
        }
         */

        return HashUtils.md5sum(context);
    }


    /**
     * Internal method for using the http client to push a whole context to a managed site
     * @param context
     * @param task
     */
    private boolean publishContext(final URI context, Task task) {
        if(context != null) {
            log.info("publishing LMF context {} to Apache Stanbol ...",context.stringValue());

            try {
                RepositoryConnection con = sesameService.getConnection();
                try {
                    if(!con.hasStatement(null, null, null, true, context)) return true;
                } finally {
                    con.close();
                }
            } catch (RepositoryException e) {
                return false;
            }
        } else {
            log.info("publishing LMF triplestore to Apache Stanbol ...");
        }

        // clear enhancement cache, since we publish new enhancement data
        stanbolService.flushCache();

        final long start = System.currentTimeMillis();


        SiteManager siteManager = slingLauncherService.getSiteManager();
        EntityhubHelper helper = slingLauncherService.getEntityhubHelper();

        if(siteManager != null && helper != null) {
            Site site = siteManager.getSite(getSiteName(context));

            if(site != null && site instanceof ManagedSite) {
                ManagedSite managedSite = (ManagedSite)site;

                try {
                    RepositoryConnection con = sesameService.getConnection();

                    try {
                        con.begin();

                        // create a Clerezza graph where the entityhub can take the triples from
                        SesameConnectionMGraph graph = new SesameConnectionMGraph(con, context);

                        // now iterate over all subjects in the named graph and send them to the entityhub
                        Set<URI> resources = new HashSet<URI>();

                        task.updateMessage("Listing resources");
                        RepositoryResult<Statement> triples = con.getStatements(null,null,null,true,context);
                        try {
                            while(triples.hasNext()) {
                                Statement triple = triples.next();
                                if(triple.getSubject() instanceof URI) {
                                    resources.add((URI) triple.getSubject());
                                }
                            }
                        } finally {
                            triples.close();
                            con.commit();
                        }
                        long count = 0;
                        task.updateMessage("Publishing resources");
                        task.updateTotalSteps(resources.size());
                        for(URI uri : resources) {
                            Representation r = helper.createRepresentation(new UriRef(uri.stringValue()),graph);
                            try {
                                managedSite.store(r);
                            } catch (EntityhubException e) {
                                log.error("error while storing resource in entity hub",e);
                            }
                            task.updateProgress(++count);
                        }
                        con.commit();
                    } finally {
                        con.close();
                    }
                } catch (RepositoryException e) {
                    log.error("error while retrieving resources from triple store",e);
                } catch (Exception e) {
                    log.error("exception while publishing resources in Stanbol: {}", e.getMessage());
                    return false;
                }

                long end = System.currentTimeMillis();
                log.info("LMF context {} published successfully after {}ms", context.stringValue(), end-start);

                return true;
            } else {
                log.error("site {} not found or not a managed site", getSiteName(context));

                return false;
            }

        } else {
            log.warn("Embedded Stanbol was not available; could not publish LMF data");

            return false;
        }
    }

    /**
     * Internal method for using the http client to push a whole context to a managed site
     * @param context
     */
    private void publishResourceContext(final URI resource, final URI context) {
        // check whether there is actually any data
        if(context != null) {
            log.info("publishing LMF context {} to Apache Stanbol ...",context.stringValue());

            try {
                RepositoryConnection con = sesameService.getConnection();
                try {
                    if(!con.hasStatement(null, null, null, true, context)) return;
                } finally {
                    con.close();
                }
            } catch (RepositoryException e) {
                return;
            }
        } else {
            log.info("publishing LMF triplestore to Apache Stanbol ...");
        }


        if(context != null) {
            log.info("publishing LMF context {} to Apache Stanbol ...",context.stringValue());
        } else {
            log.info("publishing LMF triplestore to Apache Stanbol ...");
        }

        // clear enhancement cache, since we publish new enhancement data
        stanbolService.flushCache();

        final long start = System.currentTimeMillis();

        SiteManager siteManager = slingLauncherService.getSiteManager();
        EntityhubHelper helper = slingLauncherService.getEntityhubHelper();

        if(siteManager != null && helper != null) {
            Site site = siteManager.getSite(getSiteName(context));

            if(site != null && site instanceof ManagedSite) {
                ManagedSite managedSite = (ManagedSite)site;

                try {
                    RepositoryConnection con = sesameService.getConnection();
                    try {
                        con.begin();
                        // create a Clerezza graph where the entityhub can take the triples from
                        SesameConnectionMGraph graph = new SesameConnectionMGraph(con, context);

                        Representation r = helper.createRepresentation(new UriRef(resource.stringValue()),graph);
                        try {
                            managedSite.store(r);
                        } catch (EntityhubException e) {
                            log.error("error while storing resource in entity hub",e);
                        }
                        long end = System.currentTimeMillis();
                        log.info("LMF resource {}, context {} published successfully after {}ms", resource.stringValue(), context.stringValue(), end-start);
                        con.commit();
                    } finally {
                        con.close();
                    }
                } catch (RepositoryException ex) {
                    log.error("error while publishing LMF resource to Stanbol",ex);
                }
            } else {
                log.error("site {} not found or not a managed site", getSiteName(context));
            }

        } else {
            log.warn("Embedded Stanbol was not available; could not publish LMF data");
        }
    }



}
