/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.api;

import at.newmedialab.lmf.stanbol.exception.DeploymentFailedException;

import java.io.File;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
public interface StanbolService {


    /**
     * Return the full URI of the Apache Stanbol server registered with the module.
     * @return
     */
    public String getStanbolUri();


    /**
     * Return the directory where Stanbol stores its data.
     *
     * @return
     */
    public String getStanbolHome();

    /**
     * Check whether the Stanbol server is ready. Issues an HTTP request to the /sites endpoint
     * @return
     */
    public boolean ping();



    /**
     * Flush the internal enhancement cache (e.g. in case the entity chains have been republished)
     */
    public void flushCache();

    /**
     * Deploy an OSGi bundle to the Apache Stanbol container. Used e.g. to install referenced site configurations.
     * The method expects a file as argument. The file will be moved to the deployment directory of the Felix File
     * Installer.
     *
     * @param bundleLocation
     */
    void deployBundle(File bundleLocation) throws DeploymentFailedException;
}
