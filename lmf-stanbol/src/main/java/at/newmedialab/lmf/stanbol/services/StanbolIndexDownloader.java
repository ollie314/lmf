/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.services;

import at.newmedialab.lmf.stanbol.api.StanbolService;
import at.newmedialab.lmf.stanbol.exception.DeploymentFailedException;
import at.newmedialab.lmf.stanbol.model.config.ReferencedSiteConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.http.HttpClientService;
import org.apache.marmotta.platform.core.api.task.Task;
import org.apache.marmotta.platform.core.api.task.TaskManagerService;
import org.apache.marmotta.platform.core.util.CDIContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.UUID;

/**
 * Perform asynchronous downloading and installation of a stanbol referenced site.
 * <p/>
 * Author: Sebastian Schaffert
 */
public class StanbolIndexDownloader extends Thread {

    private static Logger log = LoggerFactory.getLogger(StanbolIndexDownloader.class);

    private static int indexerCounter = 0;

    private StanbolService stanbolService;

    private HttpClientService httpService;

    private TaskManagerService taskManagerService;

    private ConfigurationService configurationService;

    private ReferencedSiteConfiguration siteConfiguration;


    public StanbolIndexDownloader(ReferencedSiteConfiguration siteConfiguration) {
        super("Stanbol Index Downloader " + ++indexerCounter);

        this.siteConfiguration = siteConfiguration;

        httpService        = CDIContext.getInstance(HttpClientService.class);
        stanbolService     = CDIContext.getInstance(StanbolService.class);
        configurationService = CDIContext.getInstance(ConfigurationService.class);
        taskManagerService = CDIContext.getInstance(TaskManagerService.class);

        setDaemon(true);
        start();
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        Task task = taskManagerService.createTask("Stanbol Site Installer (" + siteConfiguration.getName() + ")", "Stanbol Installers");

        try {
            // step 1: download the index and bundle file
            File tmpDir = new File(configurationService.getLMFHome()+File.separator+"tmp"+File.separator+UUID.randomUUID().toString());
            tmpDir.mkdirs();

            task.updateMessage("downloading index file (size: " + formatSize(siteConfiguration.getSize()) + ")");
            File tmpIndexFile = new File(tmpDir,getFile(siteConfiguration.getIndexLocation()));
            log.info("starting download of index file {}; temporary file location is {}",getFile(siteConfiguration.getIndexLocation()),tmpIndexFile);
            downloadToFile(siteConfiguration.getIndexLocation(),tmpIndexFile, siteConfiguration.getSize(), true);

            task.updateMessage("downloading bundle file");
            File tmpBundleFile = new File(tmpDir,getFile(siteConfiguration.getBundleLocation()));
            log.info("starting download of bundle file {}; temporary file location is {}",getFile(siteConfiguration.getIndexLocation()),tmpBundleFile);
            downloadToFile(siteConfiguration.getBundleLocation(),tmpBundleFile, -1, false);

            // step 2: install index file into stanbol datafiles directory
            String dataDirectoryName = stanbolService.getStanbolHome() + File.separator + "datafiles";
            task.updateMessage("installing index file to " + dataDirectoryName);
            log.info("installing index file to {}",dataDirectoryName);
            FileUtils.moveFileToDirectory(tmpIndexFile, new File(dataDirectoryName),false);

            // step 3: install bundle file into OSGi console
            task.updateMessage("deploying bundle file to Stanbol OSGi console");
            log.info("deploying bundle file to Stanbol OSGi console");
            stanbolService.deployBundle(tmpBundleFile);
            //installBundle(tmpBundleFile);

            // step 4: cleanup
            FileUtils.deleteDirectory(tmpDir);
        } catch (IOException e) {
            log.error("could not download and install stanbol site {}; I/O error: {}",siteConfiguration.getName(),e.getMessage());
        } catch (DeploymentFailedException e) {
            log.error(e.getMessage(),e);
        }
        task.endTask();
    }


    private long downloadToFile(URI location, File destination, final long defaultSize, final boolean showProgress) throws IOException {
        final FileOutputStream outputStream = new FileOutputStream(destination);

        HttpGet get = new HttpGet(location);


        ResponseHandler<Long> handler = new ResponseHandler<Long>() {
            @Override
            public Long handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                if(response.getEntity() != null) {
                    long bytes;
                    long count = 0;

                    long maxCount = response.containsHeader("Content-Length") ? Long.parseLong(response.getFirstHeader("Content-Length").getValue()) : defaultSize;

                    if(showProgress) {
                        taskManagerService.getTask().updateTotalSteps(maxCount);
                    }

                    do {
                        bytes = IOUtils.copyLarge(response.getEntity().getContent(), outputStream, 0, 4096);
                        count += bytes;

                        if(showProgress) {
                            taskManagerService.getTask().updateProgress(bytes);
                        }
                    } while(bytes == 4096);

                    outputStream.flush();
                    outputStream.close();
                    return count;
                } else {
                    outputStream.close();
                    return (long)-1;
                }
            }
        };
        try {
            return httpService.execute(get,handler);
        } finally {
            get.releaseConnection();
        }
    }

    private String formatSize(long size) {
        if(size >= 1024*1024*1024)
            // GB
            return String.format("%.2f GB",(double)size/(1024*1024*1024));
        else if(size >= 1024*1024)
            // MB
            return String.format("%.2f MB",(double)size/(1024*1024));
        else if(size > 1024) // kB
            return String.format("%.2f kB",(double)size/1024);
        else
            return String.format("%d bytes",size);

    }


    private String getFile(URI uri) {
        String path = uri.getPath();
        return path.substring(path.lastIndexOf('/')+1);
    }


}
