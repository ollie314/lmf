/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.services;

import at.newmedialab.lmf.stanbol.api.SlingLauncherService;
import at.newmedialab.lmf.stanbol.api.StanbolService;
import at.newmedialab.lmf.stanbol.api.StanbolSiteService;
import at.newmedialab.lmf.stanbol.model.config.ReferencedSiteConfiguration;
import at.newmedialab.lmf.stanbol.model.entityhub.StanbolSiteDescription;
import at.newmedialab.lmf.stanbol.provider.StanbolSiteEndpoint;
import com.google.common.collect.Lists;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.events.SesameStartupEvent;
import org.apache.marmotta.platform.ldcache.services.ldcache.LDCacheSailProvider;
import org.apache.stanbol.entityhub.servicesapi.site.Site;
import org.apache.stanbol.entityhub.servicesapi.site.SiteManager;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.Collator;
import java.util.*;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class StanbolSiteServiceImpl implements StanbolSiteService {

    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private StanbolService stanbolService;


    @Inject
    private SlingLauncherService slingLauncherService;

    @Inject
    private SesameService sesameService;

    @Inject
    private LDCacheSailProvider endpointService;


    public void initialize(@Observes SesameStartupEvent event) {
        for(String siteId : configurationService.getListConfiguration("stanbol.cached_sites", Collections.EMPTY_LIST)) {
            registerCacheEndpoint(siteId);
        }
    }


    /**
     * Return a list of the stanbol sites that are available in the local stanbol entityhub. Will return URIs in
     * case the service uses HTTP, or just the site names otherwise.
     *
     * @return
     */
    @Override
    @Deprecated
    public List<String> listStanbolSites() {
        return listStanbolSiteUris();
    }

    /**
     * Return a list of the IDs of stanbol referenced sites that are available in the local stanbol entityhub
     *
     * @return
     */
    @Override
    public List<String> listStanbolSiteIds() {
        SiteManager manager = slingLauncherService.getSiteManager();

        if(manager != null) {
            return Lists.newArrayList(manager.getSiteIds());
        } else {
            log.warn("Embedded Stanbol was not available; could not determine list of sites");
            return Collections.emptyList();
        }
    }

    /**
     * Return a list of the URIs of stanbol referenced sites that are available in the local stanbol entityhub
     *
     * @return
     */
    @Override
    public List<String> listStanbolSiteUris() {
        SiteManager manager = slingLauncherService.getSiteManager();

        if(manager != null) {
            List<String> sites = new ArrayList<String>();
            for(String id : Lists.newArrayList(manager.getSiteIds())) {
                sites.add(stanbolService.getStanbolUri()+"entityhub/site/"+id+"/");
            }

            return sites;
        } else {
            log.warn("Embedded Stanbol was not available; could not determine list of sites");
            return Collections.emptyList();
        }
    }

    /**
     * Return a reference to the stanbol site with the given name, or null if it does not exist
     *
     * @param name
     * @return
     */
    @Override
    public Site getStanbolSite(String name) {
        SiteManager manager = slingLauncherService.getSiteManager();

        if(manager != null) {
            return manager.getSite(name);
        } else {
            log.warn("Embedded Stanbol was not available; could not determine list of sites");
            return null;
        }
    }

    /**
     * Return a list of the stanbol referenced sites that can be installed in the local stanbol entityhub.
     *
     * @return
     */
    @Override
    public List<ReferencedSiteConfiguration> listInstallableSites() {
        Set<ReferencedSiteConfiguration> result = new HashSet<ReferencedSiteConfiguration>();
        try {
            Enumeration<URL> modulePropertiesEnum = this.getClass().getClassLoader().getResources("stanbol-sites.properties");

            while(modulePropertiesEnum.hasMoreElements()) {
                URL moduleUrl = modulePropertiesEnum.nextElement();

                try {
                    PropertiesConfiguration stanbolProperties = new PropertiesConfiguration(moduleUrl);

                    List<Object> prefixes = stanbolProperties.getList("configurations");

                    for(Object prefix : prefixes) {

                        try {
                            ReferencedSiteConfiguration site =
                                    new ReferencedSiteConfiguration(
                                            stanbolProperties.getString(prefix+".id"),
                                            stanbolProperties.getString(prefix+".name"),
                                            stanbolProperties.getString(prefix+".description","no description available"),
                                            stanbolProperties.getString(prefix+".bundleUrl"),
                                            stanbolProperties.getString(prefix+".indexUrl"),
                                            stanbolProperties.getLong(prefix+".size",-1)
                                    );
                            result.add(site);
                        } catch(URISyntaxException ex) {
                            log.error("The bundle/index URL of the referenced site description {} could not be parsed",prefix);
                        }

                    }
                } catch (ConfigurationException e) {
                    log.error("the properties file {} was invalid, cannot parse configuration (message: {})",moduleUrl,e.getMessage());
                }
            }
        } catch (IOException e) {
            log.error("I/O error while reading properties configuration: {}",e.getMessage());
        }

        ArrayList<ReferencedSiteConfiguration> list = new ArrayList<ReferencedSiteConfiguration>(result);
        Collections.sort(list,new Comparator<ReferencedSiteConfiguration>() {
            @Override
            public int compare(ReferencedSiteConfiguration o1, ReferencedSiteConfiguration o2) {
                return Collator.getInstance().compare(o1.getName(), o2.getName());
            }
        });

        return list;
    }

    /**
     * Return a site description of the stanbol site with the URI or name passed as argument
     *
     *
     * @param uri
     * @param id
     * @return
     */
    @Override
    public StanbolSiteDescription getSiteDescription(String uri, String id) throws IOException {
        SiteManager manager = slingLauncherService.getSiteManager();

        if(manager != null) {
            // extract id out of uri if necessary
            if(id == null) {
                String[] components = uri.split("/");
                if(!"".equals(components[components.length-1])) {
                    id = components[components.length-1];
                } else {
                    id = components[components.length-2];
                }
            }



            Site site = manager.getSite(id);

            if(site != null) {

                StanbolSiteDescription result =
                        new StanbolSiteDescription(
                                site.getConfiguration().getName(),
                                site.getConfiguration().getDescription(),
                                stanbolService.getStanbolUri()+"/entityhub/site/"+site.getId()+"/",
                                site.supportsLocalMode());
                result.setEntityPrefixes(new HashSet<String>());
                return result;
            } else {
                log.warn("Site with name {} could not be found",uri);
                return null;
            }

        } else {
            log.warn("Embedded Stanbol was not available; could not determine site description");
            return null;
        }

    }


    /**
     * Register the LDCache endpoint for the referenced site so that LDCache will retrieve resources directly from
     * Stanbol in case the Stanbol site contains cached data.
     */
    @Override
    public void registerCacheEndpoint(String siteId) {

        SiteManager manager = slingLauncherService.getSiteManager();

        Site site = manager.getSite(siteId);

        if(site != null) {
            if(site.supportsLocalMode()) {
                StanbolSiteEndpoint endpoint = new StanbolSiteEndpoint("Stanbol Site: "+siteId, site);
                endpointService.addVolatileEndpoint(endpoint);
                log.info("added Stanbol cache endpoint: {}",endpoint.getName());

                List<String> cachedSites = new ArrayList<String>(configurationService.getListConfiguration("stanbol.cached_sites", Collections.EMPTY_LIST));
                if(!cachedSites.contains(siteId)) {
                    cachedSites.add(siteId);
                    configurationService.setListConfiguration("stanbol.cached_sites",cachedSites);
                }
            } else {
                log.warn("Stanbol Site {} does not support local mode", siteId);
            }
        } else {
            throw new IllegalArgumentException("the site with ID "+siteId+" does not exist");
        }
    }


}
