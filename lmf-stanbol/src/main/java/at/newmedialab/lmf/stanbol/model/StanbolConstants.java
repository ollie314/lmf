/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model;

/**
 * A collection of constants to be used with Stanbol data.
 * <p/>
 * Author: Sebastian Schaffert
 */
public class StanbolConstants {

    /** label of site descriptions */
    public static final String ENTITYHUB_LABEL  = "http://www.w3.org/2000/01/rdf-schema#label";

    /** description of site descriptions */
    public static final String ENTITYHUB_DESC   = "http://www.w3.org/2000/01/rdf-schema#description";

    /** has the site local mode turned on? */
    public static final String ENTITYHUB_LOCAL  = "http://stanbol.apache.org/ontology/entityhub/entityhub#localMode";

    /** which entity prefixes does the site manage? */
    public static final String ENTITYHUB_PREFIX = "http://stanbol.apache.org/ontology/entityhub/entityhub#entityPrefix";

    public static final String ENTITYHUB_NS     = "http://stanbol.apache.org/ontology/entityhub/entityhub#";


    public static final String ENHANCER_NS      = "http://stanbol.apache.org/ontology/enhancer/enhancer#";


    public static final String EXTRACTOR_NS     = "http://fise.iks-project.eu/ontology/";
}
