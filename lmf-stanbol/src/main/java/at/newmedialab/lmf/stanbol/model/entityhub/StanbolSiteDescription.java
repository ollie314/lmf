/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.entityhub;

import java.util.Set;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
public class StanbolSiteDescription {

    /** name of the Stanbol site as returned by the descriptor */
    private String name;

    /** URI of the stanbol endpoint referenced by the descriptor */
    private String endpoint;

    /** description (human-readable) of the endpoint */
    private String description;

    /** a collection of URI prefixes this stanbol site manages */
    private Set<String> entityPrefixes;

    /** whether this stanbol site contains local data (suitable as cache endpoint) or not */
    private boolean local;


    public StanbolSiteDescription(String name, String endpoint, String description, boolean local) {
        this.name = name;
        this.endpoint = endpoint;
        this.description = description;
        this.local = local;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<String> getEntityPrefixes() {
        return entityPrefixes;
    }

    public void setEntityPrefixes(Set<String> entityPrefixes) {
        this.entityPrefixes = entityPrefixes;
    }

    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }
}
