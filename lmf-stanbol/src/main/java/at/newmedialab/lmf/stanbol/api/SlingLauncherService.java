/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.api;

import at.newmedialab.stanbol.configurator.api.ConfiguratorService;
import at.newmedialab.stanbol.entityhub.api.EntityhubHelper;
import org.apache.stanbol.enhancer.servicesapi.ChainManager;
import org.apache.stanbol.enhancer.servicesapi.ContentItemFactory;
import org.apache.stanbol.enhancer.servicesapi.EnhancementEngineManager;
import org.apache.stanbol.enhancer.servicesapi.EnhancementJobManager;
import org.apache.stanbol.enhancer.topic.api.TopicClassifier;
import org.apache.stanbol.enhancer.topic.api.training.TrainingSet;
import org.apache.stanbol.entityhub.servicesapi.Entityhub;
import org.apache.stanbol.entityhub.servicesapi.site.SiteManager;
import org.osgi.framework.Bundle;

import java.util.List;

/**
 * This service starts up an embedded Sling installation for Apache Stanbol in case the configuration option
 * stanbol.embedded.enabled is set to true. Sling bundles an embedded Jetty server which will
 * start up on port stanbol.embedded.port (default 8081).
 * <p/>
 * Author: Sebastian Schaffert
 */
public interface SlingLauncherService {


    /**
     * Startup and configure an embedded Apache Sling instance and all OSGi bundles. Set the appropriate LMF
     * configuration options.
     */
    public void initialise();


    /**
     * Shutdown the embedded Apache Sling instance and all bundles.
     */
    public void shutdown();

    /**
     * Return the chain manager of the embedded stanbol installation if available, or null otherwise.
     * @return
     */
    ChainManager getChainManager();

    /**
     * Return the enhancement job manager of the embedded stanbol installation if available, or null otherwise.
     * The enhancement job manager can be used for running the different enhancement chains directly.
     * @return
     */
    EnhancementJobManager getEnhancementJobManager();

    /**
     * Return the engine manager of the embedded stanbol installation if available, or null otherwise.
     * The engine manager can provide information about the different enhancement engines.
     * @return
     */
    EnhancementEngineManager getEngineManager();

    /**
     * Return the content item factory of the embedded stanbol installation if available, or null otherwise.
     * The content item factory provides methods for injecting content into Stanbol.
     * @return
     */
    ContentItemFactory getContentItemFactory();

    /**
     * Return the URL where the embedded Stanbol installation is running.
     * @return
     */
    String getEmbeddedStanbolURL();

    /**
     * Return the helper service used to create entityhub representations. This is a bridging service used for accessing
     * Stanbol internals.
     * @return
     */
    EntityhubHelper getEntityhubHelper();

    /**
     * Return the stanbol site manager, used for listing and accessing Stanbol sites.
     * @return
     */
    SiteManager getSiteManager();

    boolean ping();

    /**
     * Return the configuration helper service that can be used to create typical stanbol configurations
     * @return
     */
    ConfiguratorService getConfiguratorService();

    Entityhub getEntityhub();

    Bundle getBundle(String symbolicName);

    List<TopicClassifier> getTopicClassifiers();

    List<TrainingSet> getTrainingSets();
}
