/**
 * Extend String with a format function
 * @returns {string}
 */
String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function (m, n) { return args[n]; });
};

/**
 * Check availability of Stanbol
 */
var ping = function() {
    $.ajax({
        url: _SERVER_URL+"stanbol/ping",
        error: function() { alert("Apache Stanbol service is not available. Is Stanbol configured properly?")}
    });
};

/**
 * Context Publishing
 */
var refreshContexts = function() {
    // list the contexts and their synchronization status
    $.getJSON(_SERVER_URL+"stanbol/contexts/status", function(data) {
        var rowTemplate = '<tr id="ctx_{4}"><td>{0}</td><td>{1}</td><td>{2}</td><td><input id="cb_{4}" type="checkbox" {3}/></td></tr>'
        var fnJoin      = function(previousValue, currentValue, index, array){
            return previousValue + ", " + currentValue;
        };

        $("#published_contexts tbody").empty();

        var count = 0;
        $.each(data, function(index,contextData) {
            count++;

            $(rowTemplate.format(
                contextData.label,
                contextData.uri,
                contextData.site != null ? contextData.site : "",
                contextData['synchronized'] ? 'checked="checked"' : "",
                index
            )).appendTo("#published_contexts tbody");

            if(contextData['synchronized']) {
                $("#cb_"+index).click(function() {
                    $(this).parent().addClass("processing");
                    deactivateContext(contextData.uri, this);
                });
            } else {
                $("#cb_"+index).click(function() {
                    $(this).parent().addClass("processing");
                    activateContext(contextData.uri, this);
                });
            }
        })

    });

};

var activateContext = function(uri, source) {
    $.post(_SERVER_URL+"stanbol/contexts/publish?uri="+encodeURIComponent(uri), function(data) {
        refreshContexts();
    }).complete(function() {
            if (source) $(source).parent().removeClass("processing");
        });
};

var deactivateContext = function(uri, source) {
    $.post(_SERVER_URL+"stanbol/contexts/unpublish?uri="+encodeURIComponent(uri), function(data) {
        refreshContexts();
    }).complete(function() {
            if (source) $(source).parent().removeClass("processing");
        });
};


/*
 * Enhancer Configuration and Testing
 */
var fnEnhance = function() {
    var endpoint = $("#select_chains").val();
    var content  = $("#enhancement_text").val();

    var rowTemplate = '<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>'

    var fnJoin      = function(previousValue, currentValue, index, array){
        return previousValue + ", " + currentValue;
    };

    var fnUris      = function(entityData) {
        return entityData.uri;
    };

    $("#results").append("<tr><td>Enhancing ...</td></tr>");


    $.ajax({
        type:    'POST',
        url:     _SERVER_URL+"stanbol/enhance?chain="+endpoint,
        data:    content,
        contentType: 'text/plain',
        dataType:    'json',
        success: function(data, textStatus, jqXHR) {
            console.dir(data);
            $("#results").empty();
            $("#results").append("<tr><th>Text</th><th>Start</th><th>End</th><th>Confidence</th><th>Entities</th></tr>");
            $.each(data, function(index,siteData) {
                if(siteData.entities.length > 0) {
                    $(rowTemplate.format(siteData.selectedText, siteData.startPosition, siteData.endPosition, siteData.confidence, siteData.entities.map(fnUris).reduce(fnJoin))).appendTo("#results");
                } else {
                    $(rowTemplate.format(siteData.selectedText, siteData.startPosition, siteData.endPosition, siteData.confidence, "")).appendTo("#results");
                }
            });
        }
    });
}


var fnListEnhancers = function() {
    $.getJSON(_SERVER_URL+"stanbol/enhancers", function(data) {
        var rowTemplate = '<tr><td>{0}</td><td>{1}</td><td>{2}</td><td><a href="{3}">{0}</a></td></tr>'

        var enhancers = [];

        $("#enhancement_chains tbody").empty();
        $("#select_chains").empty();
        $.each(data, function(index,siteData) {
            $(rowTemplate.format(siteData.name, siteData.profile != null ? siteData.profile : "", siteData.context != null ? siteData.context : "entityhub", siteData.endpoint)).appendTo("#enhancement_chains tbody");

            if(""+siteData['default'] == "true") {
                $('<option value="'+siteData.name+'" selected="selected">'+siteData.name+'</option>').appendTo("#select_chains");
            } else {
                $('<option value="'+siteData.name+'">'+siteData.name+'</option>').appendTo("#select_chains");
            }
            enhancers.push(siteData.name);
        })




        // add available LMF enhancers as option to add
        $.getJSON(_SERVER_URL+"stanbol/enhancers/available", function(available) {
            var rowTemplateInstall = '<tr><td>{0}</td><td>{1}</td><td style="white-space: nowrap; text-align: right;"><input id="name_install_{0}" type="text" size="8"/> <select id="ctx_install_{0}"/> <br/> <button id="btn_install_{0}">Install</button></td></tr>';

            $("#available_enhancers tbody").empty();
            $.each(available, function(index,siteCfg) {
                $(rowTemplateInstall.format(siteCfg.name, siteCfg.description)).appendTo("#available_enhancers tbody");
                $('#btn_install_'+siteCfg.name).click(function() {
                    var name = $("#name_install_"+siteCfg.name).val();
                    var ctx  = $("#ctx_install_"+siteCfg.name).val();

                    if(name != null && name != "") {
                        $.post(_SERVER_URL+"stanbol/enhancers/create?profile="+siteCfg.name
                            +"&name="+encodeURIComponent(name)
                            +(ctx != "NONE" ? "&context=" + encodeURIComponent(ctx) : ""), function(data) {
                            alert("enhancer added successfully");
                            fnListEnhancers();
                        });
                    } else {
                        alert("Please provide a name for the enhancement configuration!");
                    }
                });
            });


            // list the contexts and their synchronization status
            $("#available_enhancers tbody select").empty();
            $('<option value="NONE">entityhub</option>').appendTo("#available_enhancers tbody select");
            $.getJSON(_SERVER_URL+"stanbol/contexts/status", function(data) {
                $.each(data, function(index,contextData) {
                    if(contextData['synchronized']) {
                        $('<option value="'+contextData.uri+'">'+contextData.label+'</option>').appendTo("#available_enhancers tbody select");
                    }
                })

            });

        });




        var btnEnhance = $("#btn_enhance").click(fnEnhance);
    });
}


var refreshEnhancerCacheStats = function() {
    $.getJSON(_SERVER_URL+"stanbol/cache/statistics", function(data) {
        $("#cache_size").html(data.size);
        $("#cache_hits").html(data.hits);
        $("#cache_misses").html(data.misses);
        $("#cache_expired").html(data.expired);
    });

    $("#btn_flush_cache").click(function() {
        $.post(_SERVER_URL+"stanbol/cache/flush", function(data) {
            alert("cache flushed");

            $.getJSON(_SERVER_URL+"stanbol/cache/statistics", function(data) {
                $("#cache_size").html(data.size);
                $("#cache_hits").html(data.hits);
                $("#cache_misses").html(data.misses);
                $("#cache_expired").html(data.expired);
            });
        });
    });
};


var listSites = function() {
    $.getJSON(_SERVER_URL+"stanbol/sites", function(data) {
        var rowTemplate = '<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>'
        var fnJoin      = function(previousValue, currentValue, index, array){
            return previousValue + ", " + currentValue;
        };

        var installedSites = [];
        $.each(data, function(index,siteData) {
            $(rowTemplate.format(siteData.name, siteData.description, siteData.local, siteData.prefixes.length > 0 ? siteData.prefixes.reduce(fnJoin) : "")).appendTo("#entityhub_sites");
            installedSites.push(siteData.name);
        })

        // initialise list of available sites
        $.getJSON(_SERVER_URL+"stanbol/sites/available", function(available) {
            var rowTemplateInstall = '<tr><td>{0}</td><td>{1}</td><td>{2}</td><td><button id="btn_install_{3}">Install</button></td></tr>';
            var rowTemplateInstalled = '<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>Installed</td></tr>';

            $.each(available, function(index,siteCfg) {
                if($.inArray(siteCfg.id,installedSites) == -1) {
                    $(rowTemplateInstall.format(siteCfg.name, siteCfg.description, siteCfg.size, siteCfg.id)).appendTo("#available_sites");
                    $('#btn_install_'+siteCfg.id).click(function() {
                        $.post(_SERVER_URL+"stanbol/sites/"+siteCfg.id, function(data) {
                            alert("site is installing; observe progress in task manager");
                        });
                    });
                } else {
                    $(rowTemplateInstalled.format(siteCfg.name, siteCfg.description, siteCfg.size)).appendTo("#available_sites");
                }
            });
        });
    });
}

