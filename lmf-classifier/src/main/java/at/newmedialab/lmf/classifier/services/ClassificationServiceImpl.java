/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.classifier.services;

import at.newmedialab.lmf.classifier.api.ClassificationService;
import at.newmedialab.lmf.classifier.exception.ClassificationException;
import at.newmedialab.lmf.classifier.exception.ClassifierNotFoundException;
import at.newmedialab.lmf.classifier.model.Classification;
import at.newmedialab.lmf.classifier.model.Classifier;
import com.google.common.base.Preconditions;
import kiwi.core.api.config.ConfigurationService;
import kiwi.core.api.triplestore.ResourceService;
import kiwi.core.model.rdf.KiWiUriResource;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Implementation of {@link at.newmedialab.lmf.classifier.api.ClassificationService}. Manages classifiers.
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class ClassificationServiceImpl implements ClassificationService {

    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private ResourceService resourceService;

    private Map<String,Classifier> classifiers;
    private Map<String,CommitTimer> timers;


    @PostConstruct
    public void initialise() {
        log.info("LMF Classification Service starting up ...");
        
        classifiers = new HashMap<String, Classifier>();
        timers      = new HashMap<String, CommitTimer>();
        
        for(String classifierName : configurationService.getListConfiguration("classifiers.enabled")) {
            if(!"".equals(classifierName.trim())) {
                try {
                    enableClassifier(classifierName); 
                } catch(IllegalArgumentException ex) {
                    log.error("could not initialise classifier {}: {}",classifierName,ex.getMessage());
                } catch (IOException e) {
                    log.error("I/O error while initialising classifier {}: {}",classifierName,e.getMessage());
                }
            }
        }
    }

    
    private void enableClassifier(String name) throws IOException {
        Preconditions.checkArgument(name.matches("^\\p{Alnum}+$"));

        log.info("-- enabling classifier {}", name);
        
        String workDir = configurationService.getWorkDir() + File.separator + "classifier" + File.separator + name.toLowerCase();
        
        String modelFileName = workDir + File.separator + "model.bin";
        String trainingFileName = workDir + File.separator + "training.txt";
        
        File workDirFile = new File(workDir);
        workDirFile.mkdirs();
        
        File modelFile = new File(modelFileName);

        File trainingFile = new File(trainingFileName);
        trainingFile.createNewFile();
        
        Classifier classifier = new Classifier(name,trainingFile,modelFile,resourceService);
        classifiers.put(name,classifier);
        
        CommitTimer timer = new CommitTimer(name);
        timer.start();
        timers.put(name,timer);
        
    }
    

    /**
     * Create a new classifier with the given name. The service will take care of creating the appropriate
     * configuration entries and work files in the LMF work directory.
     *
     * @param name a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     */
    @Override
    public void createClassifier(String name) throws ClassificationException {
        Preconditions.checkArgument(name.matches("^\\p{Alnum}+$"));

        if(classifiers.containsKey(name)) {
            throw new ClassificationException("classifier already exists");
        }

        if (!configurationService.getListConfiguration("classifiers.enabled").contains(name)) {
            List<String> activeClassifiers = new LinkedList<String>(configurationService.getListConfiguration("classifiers.enabled"));
            activeClassifiers.add(name);
            configurationService.setListConfiguration("classifiers.enabled", activeClassifiers);
        }

        try {
            enableClassifier(name);
        } catch (IOException e) {
            log.error("I/O error while creating classifier {}: {}",name,e.getMessage());
            throw new ClassificationException("I/O error while creating classifier "+name,e);
        }
    }

    /**
     * Remove the classifier with the given name from the system configuration.
     *
     * @param name a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     */
    @Override
    public void removeClassifier(String name, boolean removeData) throws ClassificationException {
        if(classifiers.containsKey(name)) {
            CommitTimer timer = timers.remove(name);
            timer.shutdown();

            classifiers.remove(name);

            List<String> activeClassifiers = new LinkedList<String>(configurationService.getListConfiguration("classifiers.enabled"));
            activeClassifiers.remove(name);
            configurationService.setListConfiguration("classifiers.enabled", activeClassifiers);



            if(removeData) {
                String workDir = configurationService.getWorkDir() + File.separator + "classifier" + File.separator + name.toLowerCase();

                String modelFileName = workDir + File.separator + "model.bin";
                String trainingFileName = workDir + File.separator + "training.txt";

                File workDirFile = new File(workDir);

                File modelFile = new File(modelFileName);
                File trainingFile = new File(trainingFileName);

                modelFile.delete();
                trainingFile.delete();
                workDirFile.delete();
            }
        } else {
            log.warn("could not delete classifier {}, it does not exist",name);
            throw new ClassifierNotFoundException("could not delete classifier "+name+", it does not exist");
        }
    }


    /**
     * Return the classifier with the name given as argument if it exists.
     *
     * @param name name of the classifier to return
     * @return the Classifier instance with the given name
     * @throws at.newmedialab.lmf.classifier.exception.ClassificationException
     *          in case the classifier does not exit
     */
    @Override
    public Classifier getClassifier(String name) throws ClassificationException {
        if(classifiers.containsKey(name)) {
            return classifiers.get(name);
        } else {
            throw new ClassifierNotFoundException("classifier "+name+" does not exist");
        }
    }

    /**
     * List all classifiers registered in the classification service.
     *
     * @return a collection of Classifier instances representing all registered classifiers
     */
    @Override
    public Collection<Classifier> listClassifiers() {
        return classifiers.values();
    }

    /**
     * Add training data to the classifier identified by the given name and for the concept passed as argument. Note
     * that training data is not immediately taken into account by the classifier. Retraining of the classifier will
     * take place when a certain threshold of training datasets has been added or when a certain (configurable) time has
     * passed.
     *
     * @param name       a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param concept    the concept which to train with the sample text
     * @param sampleText the sample text for the concept
     */
    @Override
    public void trainClassifier(String name, KiWiUriResource concept, String sampleText) throws ClassificationException {
        Classifier classifier = classifiers.get(name);
        if(classifier != null) {
            classifier.addTrainingData(concept,sampleText);

            CommitTimer timer = timers.get(name);
            timer.resetTimer();

        } else {
            throw new ClassifierNotFoundException("the classifier with the name "+name+" does not exist");
        }
        
        //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Retrain the classifier with the given name immediately. Will read in the training data and create a new
     * classification model.
     *
     * @param name
     * @throws at.newmedialab.lmf.classifier.exception.ClassificationException
     *
     */
    @Override
    public void retrainClassifier(String name) throws ClassificationException {
        Classifier classifier = classifiers.get(name);
        if(classifier != null) {
            classifier.retrain();
        } else {
            throw new ClassifierNotFoundException("the classifier with the name "+name+" does not exist");
        }
    }

    /**
     * Get classifications from the given classifier for the given text. The classifications will be ordered by
     * descending probability, so that classifications with higher probability will be first. A classification object
     * consists of a KiWiUriResource identifying the classified concept and a probability indicating how likely it is
     * that the text matches the given concept.
     *
     * @param classifier a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param text       the text to classify
     * @return a list of classifications ordered by descending probability
     */
    @Override
    public List<Classification> getAllClassifications(String classifier, String text) throws ClassificationException {
        Classifier result = classifiers.get(classifier);

        if(result != null) {
            return result.getAllClassifications(text);
        } else {
            throw new ClassifierNotFoundException("no such classifier '"+classifier+"'");
        }
    }

    /**
     * Get classifications from the given classifier for the given text. The classifications will be ordered by
     * descending probability, so that classifications with higher probability will be first. Only classifications with
     * a probability higher than the threshold will be considered. A classification object
     * consists of a KiWiUriResource identifying the classified concept and a probability indicating how likely it is
     * that the text matches the given concept.
     *
     * @param classifier a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param text       the text to classify
     * @param threshold  the minimum probability of a classification to be considered in the result
     * @return a list of classifications ordered by descending probability, all having higher probability than threshold
     */
    @Override
    public List<Classification> getAllClassifications(String classifier, String text, double threshold) throws ClassificationException {
        Classifier result = classifiers.get(classifier);

        if(result != null) {
            return result.getAllClassifications(text,threshold);
        } else {
            throw new ClassifierNotFoundException("no such classifier '"+classifier+"'");
        }
    }

    /**
     * Return the best classification from the given classifier for the given text. This method will only return the
     * KiWiUriResource representing the concept that matched best with the text given as argument.
     *
     * @param classifier a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param text       the text to classify
     * @return the resource representing the concept that had the best match for the given text
     */
    @Override
    public KiWiUriResource getBestClassification(String classifier, String text) throws ClassificationException {
        Classifier result = classifiers.get(classifier);

        if(result != null) {
            return result.getBestClassification(text);
        } else {
            throw new ClassifierNotFoundException("no such classifier '"+classifier+"'");
        }
    }

    
    @PreDestroy
    public void shutdown() {
        for(Map.Entry<String,CommitTimer> timerEntry : timers.entrySet()) {
            timerEntry.getValue().shutdown();
        }
    }


    private class CommitTimer extends Thread {

        private final String coreName;

        private long         startTime;

        private boolean      shutdown = false;

        private CommitTimer(String coreName) {
            super("Commit Timer for Classifier " + coreName);
            this.coreName = coreName;
            startTime = Long.MAX_VALUE;
            setDaemon(true);
        }

        /**
         * Restart the timer. Will run the execution after the timeout has
         * passed.
         */
        public synchronized void resetTimer() {
            startTime = System.currentTimeMillis();
            this.notify();
        }

        /**
         * Cancel the timer. The execution will not run.
         */
        public synchronized void cancel() {
            startTime = Long.MAX_VALUE;
        }

        public void shutdown() {
            shutdown = true;
            this.interrupt();
        }

        @Override
        public void run() {
            log.info("commit timer for core {} starting up ...", coreName);
            try {
                while (!shutdown) {
                    if (startTime < System.currentTimeMillis() - configurationService.getIntConfiguration("classifiers.training_timer", 60000)) {
                        log.info("committing trained samples for classifier {} after timeout", coreName);
                        
                        Classifier classifier = classifiers.get(coreName);
                        if(classifier != null) {
                            if(classifier.getTrainingUpdates() > 0) {
                                try {
                                    classifier.retrain();
                                } catch (ClassificationException e) {
                                    log.error("retraining of classifier {} failed: ",coreName,e);
                                }
                            }
                        }
                        startTime = Long.MAX_VALUE;
                    }
                    synchronized (this) {
                        this.wait(configurationService.getIntConfiguration("classifiers.training_timer", 60000));
                    }
                }
            } catch (InterruptedException ex) {
                // log.info("commit timer for core {} interrupted; forcing commit", coreName);
                // commit(coreName, true);
            }
            log.info("commit timer for classifier {} shutting down ...", coreName);
        }
    }

}
