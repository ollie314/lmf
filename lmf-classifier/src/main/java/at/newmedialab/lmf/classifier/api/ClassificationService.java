/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.classifier.api;

import at.newmedialab.lmf.classifier.exception.ClassificationException;
import at.newmedialab.lmf.classifier.model.Classification;
import at.newmedialab.lmf.classifier.model.Classifier;
import kiwi.core.model.rdf.KiWiUriResource;

import java.util.Collection;
import java.util.List;

/**
 * A service for managing classifiers in the LMF system. Provides methods for creating and removing classifiers
 * as well as for getting classifications for text content from individual classifiers.
 * <p/>
 * Author: Sebastian Schaffert
 */
public interface ClassificationService {


    /**
     * Create a new classifier with the given name. The service will take care of creating the appropriate 
     * configuration entries and work files in the LMF work directory.
     * 
     * @param name a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     */
    public void createClassifier(String name) throws ClassificationException;


    /**
     * Remove the classifier with the given name from the system configuration.
     *
     * @param name a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param removeData also remove all training and model data of this classifier from the file system
     */
    public void removeClassifier(String name, boolean removeData) throws ClassificationException;

    /**
     * Return the classifier with the name given as argument if it exists.
     *
     * @param name  name of the classifier to return
     * @return the Classifier instance with the given name
     * @throws ClassificationException  in case the classifier does not exit
     */
    public Classifier getClassifier(String name) throws ClassificationException;
    
    /**
     * List all classifiers registered in the classification service.
     * 
     * @return a collection of Classifier instances representing all registered classifiers
     */
    public Collection<Classifier> listClassifiers();
    
    
    /**
     * Add training data to the classifier identified by the given name and for the concept passed as argument. Note
     * that training data is not immediately taken into account by the classifier. Retraining of the classifier will
     * take place when a certain threshold of training datasets has been added or when a certain (configurable) time has
     * passed.
     *
     * @param name a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param concept the concept which to train with the sample text
     * @param sampleText the sample text for the concept
     */
    public void trainClassifier(String name, KiWiUriResource concept, String sampleText) throws ClassificationException;


    /**
     * Retrain the classifier with the given name immediately. Will read in the training data and create a new
     * classification model.
     *
     * @param name
     * @throws ClassificationException
     */
    public void retrainClassifier(String name) throws ClassificationException;
    
    
    /**
     * Get classifications from the given classifier for the given text. The classifications will be ordered by
     * descending probability, so that classifications with higher probability will be first. A classification object
     * consists of a KiWiUriResource identifying the classified concept and a probability indicating how likely it is
     * that the text matches the given concept.
     *
     * @param classifier a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param text the text to classify
     * @return a list of classifications ordered by descending probability
     */
    public List<Classification> getAllClassifications(String classifier, String text) throws ClassificationException;


    /**
     * Get classifications from the given classifier for the given text. The classifications will be ordered by
     * descending probability, so that classifications with higher probability will be first. Only classifications with
     * a probability higher than the threshold will be considered. A classification object
     * consists of a KiWiUriResource identifying the classified concept and a probability indicating how likely it is
     * that the text matches the given concept.
     *
     * @param classifier a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param text the text to classify
     * @param threshold the minimum probability of a classification to be considered in the result
     * @return a list of classifications ordered by descending probability, all having higher probability than threshold
     */
    public List<Classification> getAllClassifications(String classifier, String text, double threshold) throws ClassificationException;

    /**
     * Return the best classification from the given classifier for the given text. This method will only return the
     * KiWiUriResource representing the concept that matched best with the text given as argument.
     *
     * @param classifier a string identifying the classifier; should only consist of alphanumeric characters (no white spaces)
     * @param text the text to classify
     * @return the resource representing the concept that had the best match for the given text
     */
    public KiWiUriResource getBestClassification(String classifier, String text) throws ClassificationException;
}
