/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.impl;

import java.io.IOException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.openrdf.model.BNode;
import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.newmedialab.lmf.client.ClientConfiguration;
import at.newmedialab.lmf.client.clients.SPARQLClient;
import at.newmedialab.lmf.client.exception.LMFClientException;
import at.newmedialab.lmf.importer.api.LMFClientConnector;

/**
 * An implementation of a LMF Client Connector using the LMF SPARQL Update endpoint to update newsitems.
 * The updateNewsItem method will execute two update queries:
 * <ul>
 *     <li>a SPARQL DELETE for all triples containing the resource as subject in the defined context</li>
 *     <li>a SPARQL INSERT DATA fot all new triples passed over as argument</li>
 * </ul>
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class SPARQLLMFClientConnector implements LMFClientConnector {

    private static Logger log = LoggerFactory.getLogger(SPARQLLMFClientConnector.class);

    private final String uri;
    private final String user;
    private final String pass;

    private BasicHttpParams httpParams;
    private PoolingClientConnectionManager connectionManager;

    public SPARQLLMFClientConnector(String uri, String user, String pass) {
        super();
        this.uri = uri;
        this.user = user;
        this.pass = pass;

        initHttpClient();
    }

    private void initHttpClient() {
        httpParams = new BasicHttpParams();
        httpParams.setParameter(CoreProtocolPNames.USER_AGENT, "SNML SN Importer");
        httpParams.setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, true);
        httpParams.setIntParameter(ClientPNames.MAX_REDIRECTS, 3);
        httpParams.setBooleanParameter(CoreConnectionPNames.SO_KEEPALIVE, true);

        connectionManager = new PoolingClientConnectionManager();
        connectionManager.setMaxTotal(50);
        connectionManager.setDefaultMaxPerRoute(50);
    }



    private ClientConfiguration buildConfiguration(String context) {
        ClientConfiguration conf = new ClientConfiguration(uri);
        conf.setConectionManager(connectionManager);
        if (StringUtils.isNotBlank(user)) {
            conf.setLmfUser(user);
        }
        if (StringUtils.isNotBlank(pass)) {
            conf.setLmfPassword(pass);
        }
        if (StringUtils.isNotBlank(context)) {
            conf.setLmfContext(context);
        }
        return conf;
    }

    public PoolingClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    /**
     * Update the news item with the URI given as first argument using the data (all triples) provided by the repository
     * connection given as third argument. The update will be carried out in the context (named graph) with the URI given
     * as second argument.
     *
     * @param itemUri    the URI of the news item (news article, wiki article, blog posting, image, video) to update
     * @param contextUri the context URI where to store the triples in the LMF
     * @param connection the repository connection containing the updated triples
     * @throws java.io.IOException in case there is an error connecting to the LMF
     * @throws org.openrdf.repository.RepositoryException
     *                             in case there is an error accessing the triples
     */
    @Override
    public boolean updateContentItem(URI itemUri, URI contextUri, RepositoryConnection connection) throws IOException, RepositoryException {
    	
    	//TODO: remove the delete from here or keep it?
    	//		one single query is more effective
    	//		but less flexible from the api perspective
    	//		see the new method deleteContentItem(URI)
        StringBuilder builder = new StringBuilder();
        builder.append("DELETE { GRAPH <");
        builder.append(contextUri.stringValue());
        builder.append("> { <");
        builder.append(itemUri.stringValue());
        builder.append("> ?p ?o } };\n\n");

        builder.append("INSERT DATA { GRAPH <");
        builder.append(contextUri.stringValue());
        builder.append("> {");

        RepositoryResult<Statement> triples = connection.getStatements(null,null,null,true);
        while(triples.hasNext()) {
            formatSparql(triples.next(), builder);
        }
        triples.close();

        builder.append("} };\n");

        String query = builder.toString();

        log.debug("Saving item '{}' ...", itemUri);

        SPARQLClient sparqlClient = new SPARQLClient(buildConfiguration(contextUri.stringValue()));
        try {
            sparqlClient.update(query);

            return true;
        } catch (LMFClientException e) {
            log.error("error while executing SPARQL update",e);
            return false;
        }

    }

    private void formatSparql(Statement statement, StringBuilder builder) {
        formatSparql(statement.getSubject(),builder);
        builder.append(" ");
        formatSparql(statement.getPredicate(), builder);
        builder.append(" ");
        formatSparql(statement.getObject(), builder);
        builder.append(".\n");
    }

    private void formatSparql(Value v, StringBuilder builder) {
        if(v instanceof URI) {
            builder.append("<");
            builder.append(v.stringValue());
            builder.append(">");
        } else if(v instanceof BNode) {
            builder.append("_:");
            builder.append(v.stringValue());
        } else if(v instanceof Literal) {
            Literal l = (Literal)v;
            builder.append("\"");
            builder.append(StringEscapeUtils.escapeJava(l.stringValue()));
            builder.append("\"");
            if(l.getLanguage() != null) {
                builder.append("@");
                builder.append(l.getLanguage());
            }
            if(l.getDatatype() != null) {
                builder.append("^^<");
                builder.append(l.getDatatype().stringValue());
                builder.append(">");
            }
        }
    }

	@Override
	public boolean deleteContentItem(URI itemUri) {
		log.debug("Deleting item '{}' ...", itemUri.stringValue());
		
        StringBuilder builder = new StringBuilder();
        builder.append("DELETE { GRAPH <");
        builder.append(itemUri.stringValue());
        builder.append("> { <");
        builder.append(itemUri.stringValue());
        builder.append("> ?p ?o } };\n");
        
        SPARQLClient sparqlClient = new SPARQLClient(buildConfiguration(itemUri.stringValue()));
        try {
            sparqlClient.update(builder.toString());
            return true;
        } catch (LMFClientException | IOException e) {
        	log.error("Error deleting item {}: {}", itemUri.stringValue(), e.getMessage());
            return false;
        }
	}
	
	@Override
	public boolean resetContext(URI contextUri) throws IOException, RepositoryException {
		// TODO Auto-generated method stub
		return false;
	}
	
}
