/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.MapConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.InvariantReloadingStrategy;
import org.apache.commons.lang.StringUtils;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.newmedialab.lmf.importer.api.BaseContentItem;
import at.newmedialab.lmf.importer.api.BaseImporter;
import at.newmedialab.lmf.importer.api.LMFClientConnector;
import at.newmedialab.lmf.importer.impl.FileDumpClientConnector;
import at.newmedialab.lmf.importer.impl.ImportLMFClientConnector;
import at.newmedialab.lmf.importer.impl.SPARQLLMFClientConnector;
import at.newmedialab.lmf.importer.mock.MockLMFClientConnector;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;

/**
 * This is an abstract superclass for all command line interface classes that are used to start importers. It offers
 * support for the common command line arguments.
 * <p/>
 * Subclasses need to implement a main method as follows:
 * <pre>
 *     public static void main(String[] args) {
 *          try {
 *              BaseCommandLine cli = new SubClass(args);
 *              cli.runImports();
 *          } catch(ParseException ex) {
 *              log.error("error while parsing command line");
 *          }
 *     }
 * </pre>
 * The runImports method will do nothing in case the importer could not be initialised properly
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public abstract class BaseCommandLine<T extends BaseContentItem> {

    protected static Logger log = LoggerFactory.getLogger(BaseCommandLine.class);

    private BaseImporter<T> importer;


    private LMFClientConnector connector;

    private Configuration configuration;

    /**
     * No-arg constructor, used by the tests
     */
    protected BaseCommandLine() {
    	
    }

    protected BaseCommandLine(String[] args) throws ParseException {
        CommandLine line  = buildCommandLine(args);
        configuration = buildConfiguration(line);

        configureLogging(configuration);

        if(line.hasOption("help")) {
            displayUsage();
        } else {
            // initialise client connector
            if(line.hasOption("connector")) {
                if("mock".equalsIgnoreCase(line.getOptionValue("connector"))) {
                    connector = new MockLMFClientConnector(false);
                } else if("sparql".equalsIgnoreCase(line.getOptionValue("connector"))) {
                    connector = new SPARQLLMFClientConnector(configuration.getString("lmf.serviceurl"), configuration.getString("lmf.user"), configuration.getString("lmf.pass"));
                } else if ("file".equalsIgnoreCase(line.getOptionValue("connector"))) {
                    connector = new FileDumpClientConnector(new File(configuration.getString("dump.file", "triples")));
                } else {
                    connector = new ImportLMFClientConnector(configuration.getString("lmf.serviceurl"), configuration.getString("lmf.user"), configuration.getString("lmf.pass"));
                }
            } else {
                connector = new ImportLMFClientConnector(configuration.getString("lmf.serviceurl"), configuration.getString("lmf.user"), configuration.getString("lmf.pass"));
            }

            // initialise importer
            importer = buildImporter(connector, configuration);
        }
    }

    /**
     * Execute the importer, using the configuration passed as arguments of the constructor.
     * @throws RepositoryException 
     */
    public void runImport(T newsItem) throws RepositoryException {
        if(importer != null) {
            importer.runImport(newsItem);
        }
    }

    /**
     * Execute the importer, using the configuration passed as arguments of the constructor.
     */
    public void runImports() {
        if(importer != null) {
            importer.runImports();
        }
        dump();
    }
    
    /**
     * Dump the current status of the connector (only for those with can be dump)
     * 
     */
    public void dump() {
        if(connector instanceof MockLMFClientConnector) {
            if(StringUtils.isNotEmpty(configuration.getString("output.file"))) {
                try {
                    OutputStream out;
                    if("-".equals(configuration.getString("output.file"))) {
                        out = System.out;
                    } else {
                        File file = new File(configuration.getString("output.file"));
                        out = new FileOutputStream(file);
                    }

                    try {
                        RepositoryConnection con = ((MockLMFClientConnector) connector).getRepositoryConnection();
                        try {
                            con.begin();
                            RDFWriter w = Rio.createWriter(RDFFormat.TURTLE, out);
                            con.export(w);
                            con.commit();
                        } finally {
                            con.close();
                        }
                    } catch (RDFHandlerException e) {
                        log.error("error while writing output: {}",e.getMessage());
                    } catch(RepositoryException ex) {
                        log.error("error accessing triplestore: {}",ex.getMessage());
                    }
                } catch (FileNotFoundException e) {
                    log.error("output file could not be created: {}",e.getMessage());
                }
            }
        } else if (connector instanceof FileDumpClientConnector) {
            FileDumpClientConnector fdcc = (FileDumpClientConnector) connector;
            try {
                fdcc.writeFile();
            } catch (IOException e) {
                log.error("error while writing outputfile: {}", e.getMessage());
            }
        }
    }

    /**
     * Build the importer handled by this command line from the configuration passed as argument.
     *
     * @param configuration
     * @return
     */
    protected abstract BaseImporter<T> buildImporter(LMFClientConnector connector, Configuration configuration);

    /**
     * Parse the command line from the arguments given. The method will use the options returned by buildOptions(). If
     * subclasses want to provide additional options, they should therefore overwrite buildOptions().
     *
     * @param args  command line arguments
     * @return  the parsed commons-cli command line object
     */
    public CommandLine buildCommandLine(String[] args) throws ParseException {
        CommandLineParser parser = new PosixParser();

        return parser.parse(buildOptions(), args);
    }

    /**
     * Build the complete configuration based on the options passed on the command line. The method will first try
     * to load the configuration file passed as argument --config, and then overwrite in-memory all those configuration
     * options passed as explicit parameter with -D.
     *
     * @param cli
     * @return
     */
    public Configuration buildConfiguration(CommandLine cli) {
        CompositeConfiguration configuration = new CompositeConfiguration();

        // add in-memory configuration
        MapConfiguration memory = new MapConfiguration(new HashMap<String, Object>());
        configuration.addConfiguration(memory,true);

        // check if config option has been given
        if(cli.hasOption("config")) {
            String filename = cli.getOptionValue("config");

            log.debug("loading configuration file from {}",filename);

            try {
                File cfgfile    = new File(filename);
                if(cfgfile.exists() && cfgfile.canRead()) {
                    PropertiesConfiguration properties   = new PropertiesConfiguration(cfgfile);
                    properties.setAutoSave(false);
                    properties.setReloadingStrategy(new InvariantReloadingStrategy());
                    configuration.addConfiguration(properties);
                } else {
                    log.error("configuration file {} does not exist",filename);
                }
            } catch (ConfigurationException e) {
                log.error("error while reading onfiguration file: {}", e.getMessage());
            }
        }

        // read all property definitions
        if(cli.hasOption('D')) {
            for(Map.Entry<String,String> entry : asMap(cli.getOptionProperties("D")).entrySet()) {
                configuration.setProperty(entry.getKey(), entry.getValue());

                log.debug("setting configuration property {}={}", entry.getKey(), entry.getValue());
            }
        }

        // internal properties
        if(cli.hasOption("logfile")) {
            configuration.setProperty("logging.logfile", cli.getOptionValue("logfile"));
        }
        if(cli.hasOption("loglevel")) {
            configuration.setProperty("logging.level", cli.getOptionValue("loglevel"));
        }
        if(cli.hasOption("output")) {
            configuration.setProperty("output.file", cli.getOptionValue("output"));
        }

        return configuration;
    }


    /**
     * Create the base command line options for all importers. Subclasses can extend the set of options by adding
     * additional options to the result, e.g. via super.buildOptions().addOption()...
     * <p/>
     * The base options are:
     * <ul>
     *     <li>-Dproperty=value: overwrite the configuration value "property" with a new value</li>
     *     <li>-c config.properties: load the configuration from config.properties</li>
     *     <li>-l logfile.log: write all logging output to logfile.log</li>
     *     <li>--dryRun: perform a dry run, i.e. do not update the LMF server</li>
     * </ul>
     * @return
     */
    @SuppressWarnings("static-access")
    public Options buildOptions() {      
		Option property  = OptionBuilder.withArgName("property=value")
                .hasArgs(2)
                .withValueSeparator()
                .withDescription( "use value for given property, overwriting any settings configured in the configuration file " )
                .create( "D" );

        Option configuration = OptionBuilder.withArgName("file")
                .hasArg()
                .withDescription("location of the configuration file to use")
                .withLongOpt("config")
                .create("c");

        Option connector = OptionBuilder.withArgName("name")
                .hasArg()
                .withDescription("the connector to use for connecting with the LMF (one of import,sparql,mock,file; default: import)")
                .withLongOpt("connector")
                .create("C");


        Option logfile   = OptionBuilder.withArgName( "file" )
                .hasArg()
                .withDescription(  "use given file for logging output" )
                .withLongOpt("logfile")
                .create();

        Option loglevel   = OptionBuilder.withArgName( "level" )
                .hasArg()
                .withDescription(  "use given log level for logging output" )
                .withLongOpt("loglevel")
                .create();


        Option dryrun    = OptionBuilder.withDescription(  "dry run: do not write the data to the LMF server, just print it on the console" )
                .withLongOpt("dryRun")
                .create("n");

        Option debug    = OptionBuilder.withDescription(  "debug: add additional logging output" )
                .withLongOpt("debug")
                .create("v");

        Option help    = OptionBuilder.withDescription(  "display help message" )
                .withLongOpt("help")
                .create("h");

        Option outfile   = OptionBuilder.withArgName( "file" )
                .hasArg()
                .withDescription(  "use given file for output (mock connector only)" )
                .withLongOpt("output")
                .create("o");


        Options options = new Options();
        options.addOption(configuration);
        options.addOption(connector);
        options.addOption(logfile);
        options.addOption(outfile);
        options.addOption(loglevel);
        options.addOption(property);
        options.addOption(dryrun);
        options.addOption(debug);
        options.addOption(help);

        return options;
    }

    public void displayUsage() {
        // automatically generate the help statement
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "java -jar filename.jar", buildOptions() );
    }

    /**
     * Configure logback logging according to command line arguments / configuration.
     * @param cfg
     */
    protected void configureLogging(Configuration cfg) {
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();

        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        if(StringUtils.isNotEmpty(cfg.getString("logging.logfile"))) {
            FileAppender<ILoggingEvent> appender = new FileAppender<ILoggingEvent>();
            appender.setContext(lc);
            appender.setAppend(true);
            appender.setFile(cfg.getString("logging.logfile"));
            PatternLayoutEncoder pl = new PatternLayoutEncoder();
            pl.setPattern("%d %level %logger{15} - %m%n)");
            pl.setContext(lc);
            pl.start();
            appender.setEncoder(pl);
            appender.start();

            root.addAppender(appender);

            root.getAppender("CONSOLE").stop();
            log.info("log file changed to {}", cfg.getString("logging.logfile"));
        }
        if(StringUtils.isNotEmpty(cfg.getString("logging.level"))) {
            root.setLevel(Level.toLevel(cfg.getString("logging.level"), Level.INFO));
            log.info("log level switched to {}", root.getLevel());
        }

    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Map<String,String> asMap(Properties prop) {
        return new HashMap<String, String>((Map) prop);
    }
}
