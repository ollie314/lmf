package at.newmedialab.lmf.importer.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.newmedialab.lmf.importer.api.LMFClientConnector;

/**
 * Simple {@link LMFClientConnector} that dumps all triples to RDF-Files (default: Turtle).
 * After the last modification call {@link #writeFile()} explicitly to dump the triples to the file.
 * <p/>
 * This implementation is doing some very conservative locking for concurrency, just to be on the safe side.
 * <p/>
 * Set {@link #batchSize} to values bigger than {@code 0} to automatically dump in batches, filenames will be 
 * adjusted automatically (e.g. {@code foo.ttl}, {@code foo.1.ttl}, {@code foo.2.ttl}, ...) 
 * 
 * @author Jakob Frank <jakob.frank@salzburgresearch.at>
 *
 */
public class FileDumpClientConnector implements LMFClientConnector {

    private static Logger log = LoggerFactory.getLogger(FileDumpClientConnector.class);
    
    private final File dumpFile;
    private int fCount;
    private AtomicInteger rscCount = new AtomicInteger();
    private RDFFormat format;
    private SailRepository repository;
    private int batchSize = -1;
    
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    /**
     * Create a {@link FileDumpClientConnector} writing the triples to a dumpFile in {@link RDFFormat#TURTLE}.
     * @param dumpFile the file to write the triples to.
     * @see #FileDumpClientConnector(File, RDFFormat)
     */
    public FileDumpClientConnector(File dumpFile) {
        this(dumpFile, RDFFormat.TURTLE);
    }
    
    /**
     * Create a {@link FileDumpClientConnector} writing the triples to a dumpFile.
     * @param dumpFile the file to write the triples to.
     * @param format the {@link RDFFormat} to use.
     * @see #FileDumpClientConnector(File, RDFFormat)
     */
    public FileDumpClientConnector(File dumpFile, RDFFormat format) {
        this.dumpFile = dumpFile;
        this.fCount = 0;
        this.format = format;
        
        this.repository = new SailRepository(new MemoryStore());
        try {
            repository.initialize();
        } catch (RepositoryException e) {
            log.error("could not initialise in-memory repository",e);
        }

        
    }

    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    @Override
    public boolean updateContentItem(URI itemUri, URI contextUri,
            RepositoryConnection connection) throws IOException,
            RepositoryException {
        
        final Lock l = lock.readLock();
        l.lock();
        RepositoryConnection conn = repository.getConnection();
        try {
            conn.remove(itemUri, null, null, contextUri);

            conn.add(connection.getStatements(null,null,null,true), contextUri);
            if (rscCount.incrementAndGet() > batchSize && batchSize > 0) {
                writeFile();
            }
        } finally {
            conn.commit();
            conn.close();
            l.unlock();
        }

        return true;
    }

    @Override
    public boolean deleteContentItem(URI itemUri) {
        
        final Lock l = lock.readLock();
        l.lock();
        try {
            RepositoryConnection conn = repository.getConnection();
            try {
                conn.remove(itemUri, null, null);
                rscCount.decrementAndGet();
            } finally {
                conn.commit();
                conn.close();
            }
            return true;
        } catch (RepositoryException e) {
            log.error("Could not delete content item {}: {}", itemUri.stringValue(), e.getMessage());
            return false;
        } finally {
            l.unlock();
        }
    }

    @Override
    public boolean resetContext(URI contextUri) throws IOException,
            RepositoryException {
        final Lock l = lock.readLock();
        l.lock();
        try {
            RepositoryConnection conn = repository.getConnection();
            conn.clear(contextUri);
            conn.commit();
            conn.close();
            return true;
        } finally {
            l.unlock();
        }
    }
    
    /**
     * Write the triples that were added using {@link #updateContentItem(URI, URI, RepositoryConnection)}
     * to the specified {@link #dumpFile}.
     * @return the file the triples were written to.
     */
    public synchronized File writeFile() throws IOException {
        /* prepare some things */
        final File dir = dumpFile.getParentFile();
        dir.mkdirs();
        String fName = dumpFile.getName();
        final String fExt = "." + format.getDefaultFileExtension();
        if (!fName.endsWith(fExt)) {
            fName = fName + fExt;
        }
        if (fCount > 0) {
            fName.replaceFirst(Pattern.quote(fExt)+"$", Matcher.quoteReplacement("."+ (++fCount) +fExt));
        }
        
        final File file = new File(dir, fName);
        
        final Lock l = lock.writeLock();
        l.lock();
        try {
            FileWriter writer = new FileWriter(file);
            try {
                RepositoryConnection con = repository.getConnection();
                try {
                    con.begin();
                    RDFWriter w = Rio.createWriter(format, writer);
                    con.export(w);
                    con.commit();
                } catch(RepositoryException ex) {
                    con.rollback();
                } finally {
                    con.close();
                }
                writer.close();
                log.debug("Dumped triples to {}", file.getAbsolutePath());
                
                this.repository.shutDown();
                this.repository = new SailRepository(new MemoryStore());
                repository.initialize();
                rscCount.set(0);
            } catch (RDFHandlerException e) {
                log.error("error writing triples to string",e);
            } catch(RepositoryException ex) {
                log.error("error accessing in-memory repository", ex);
            }
        } finally {
            l.unlock();
        }
        return file;
    }

}
