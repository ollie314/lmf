/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.mock;

import java.io.CharArrayWriter;
import java.io.IOException;

import org.openrdf.model.URI;
import org.openrdf.query.BooleanQuery;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.newmedialab.lmf.importer.api.LMFClientConnector;

/**
 * Mock implementation of an LMFClientConnector, useful for testing. It will simply store the triples
 * in an in-memory repository that can then be accessed via the getRepositoryConnection method.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class MockLMFClientConnector implements LMFClientConnector {

    private static Logger log = LoggerFactory.getLogger(MockLMFClientConnector.class);

    private Repository repository;

    private boolean clearOnUpdate = true;

	public MockLMFClientConnector() {
        this(true);
    }

    public MockLMFClientConnector(boolean clearOnUpdate) {
        this.repository = new SailRepository(new MemoryStore());
        try {
            repository.initialize();
        } catch (RepositoryException e) {
            log.error("could not initialise in-memory repository",e);
        }
        this.clearOnUpdate = clearOnUpdate;
    }

    /**
     * Update the news item with the URI given as first argument using the data (all triples) provided by the repository
     * connection given as third argument. The update will be carried out in the context (named graph) with the URI given
     * as second argument.
     *
     * @param itemUri    the URI of the news item (news article, wiki article, blog posting, image, video) to update
     * @param contextUri the context URI where to store the triples in the LMF
     * @param connection the repository connection containing the updated triples
     * @throws java.io.IOException in case there is an error connecting to the LMF
     * @throws org.openrdf.repository.RepositoryException
     *                             in case there is an error accessing the triples
     */
    @Override
    public boolean updateContentItem(URI itemUri, URI contextUri, RepositoryConnection connection) throws IOException, RepositoryException {
        RepositoryConnection conn = repository.getConnection();
        if(clearOnUpdate) {
            //conn.remove(itemUri, null, null, contextUri);
        	conn.clear(contextUri);
        }
        conn.add(connection.getStatements(null,null,null,true), contextUri);
        conn.commit();
        conn.close();

        log.trace("triples: \n{}", exportTriples(RDFFormat.TURTLE));
        return true;
    }
    
    public boolean isClearOnUpdate() {
		return clearOnUpdate;
	}

	public void setClearOnUpdate(boolean clearOnUpdate) {
		this.clearOnUpdate = clearOnUpdate;
	}

    /**
     * Return a connection to the repository used by this mock client connector.
     *
     * @return
     * @throws RepositoryException
     */
    public RepositoryConnection getRepositoryConnection() throws RepositoryException {
        return repository.getConnection();
    }
    
    /**
     * Evaluate a SPARQL ASK query against the triples stored by this mock client. Useful for testing.
     *
     * @param query
     * @return
     */
    public boolean testSparqlAsk(String query) throws RepositoryException, MalformedQueryException, QueryEvaluationException {
        RepositoryConnection con = repository.getConnection();
        try {
            con.begin();
            BooleanQuery q = con.prepareBooleanQuery(QueryLanguage.SPARQL, query);
            return q.evaluate();
        } finally {
            con.rollback();
            con.close();
        }
    }    

    public String exportTriples(RDFFormat format) {
        CharArrayWriter writer = new CharArrayWriter();
        try {
            RepositoryConnection con = repository.getConnection();
            try {
                con.begin();
                RDFWriter w = Rio.createWriter(format, writer);
                con.export(w);
                con.commit();
            } catch(RepositoryException ex) {
                con.rollback();
            } finally {
                con.close();
            }
        } catch (RDFHandlerException e) {
            log.error("error writing triples to string",e);
        } catch(RepositoryException ex) {
            log.error("error accessing in-memory repository", ex);
        }
        return writer.toString();
    }

	@Override
	public boolean deleteContentItem(URI itemUri) {
		try {
			RepositoryConnection conn = repository.getConnection();
	        conn.remove(itemUri, null, null);
	        conn.commit();
	        conn.close();
	        return true;
		} catch (RepositoryException e) {
			log.error("Error deleting item {}: {}", itemUri.stringValue(), e.getMessage());
			return false;
		}
	}
    
    @Override
    public boolean resetContext(URI contextUri) throws IOException, RepositoryException {
        RepositoryConnection conn = repository.getConnection();
        conn.clear(contextUri);
        return true;
    }

}
