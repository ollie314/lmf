/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.api;

import java.io.IOException;

import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

/**
 * This interface specifies methods for updating the results in a remote triplestore. It can also be implemented
 * using a mock implementation for testing. The main method is updateNewsItem, which takes as arguments
 * <ul>
 *     <li>the URI of the news item to update</li>
 *     <li>the URI of the context where to store the triple data</li>
 *     <li>a Sesame RepositoryConnection</li>
 * </ul>
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public interface LMFClientConnector {

    /**
     * Update the news item with the URI given as first argument using the data (all triples) provided by the repository
     * connection given as third argument. The update will be carried out in the context (named graph) with the URI given
     * as second argument.
     *
     * @param itemUri     the URI of the news item (news article, wiki article, blog posting, image, video) to update
     * @param contextUri  the context URI where to store the triples in the LMF
     * @param connection  the repository connection containing the updated triples
     * @throws IOException in case there is an error connecting to the LMF
     * @throws RepositoryException in case there is an error accessing the triples
     */
    boolean updateContentItem(URI itemUri, URI contextUri, RepositoryConnection connection) throws IOException, RepositoryException;
 
    /**
     * Delete a resource from LMF
     * 
     * @param contextUri uri
     * @return operation success or not
     */
    boolean deleteContentItem(URI itemUri);
    
    /**
     * Completely removes a context from LMF
     * 
     * @param contextUri uri
     * @return operation success or not
     * @throws IOException
     * @throws RepositoryException
     */
    boolean resetContext(URI contextUri) throws IOException, RepositoryException;
    
}
