package at.newmedialab.lmf.importer.test;

import org.junit.Assert;
import org.junit.Test;
import org.openrdf.model.URI;
import org.openrdf.rio.RDFFormat;

import at.newmedialab.lmf.importer.model.VocabularyManager;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class VocabularyManagerTest {



    @Test
    public void testVocabularyManager() throws Exception {
        VocabularyManager manager = new VocabularyManager(this.getClass().getResourceAsStream("sn-structure-skos.n3"), RDFFormat.TURTLE);

        // test different lookups
        URI uri01 = manager.lookup("Wirtschaft",null);

        // test on prefLabel
        Assert.assertNotNull(uri01);
        Assert.assertEquals("http://lmf.salzburg.com/resource/news-ns/Wirtschaft", uri01.stringValue());

        URI uri02 = manager.lookup("Economy","en");

        Assert.assertNotNull(uri02);
        Assert.assertEquals("http://lmf.salzburg.com/resource/news-ns/Wirtschaft", uri02.stringValue());

        // test on hiddenLabel
        URI uri03 = manager.lookup("salzburg wiki",null);

        Assert.assertNotNull(uri03);
        Assert.assertEquals("http://lmf.salzburg.com/resource/news-ns/Wiki", uri03.stringValue());


    }


}
