<?php

require_once "functions.inc";
require_once "renderer.inc";
require_once "lmf.inc";


function lmf_main() {
    return LMF::getStaticPage("main");
}

function lmf_status() {
    $lmf = new LMF();
    return LMFRenderer::renderStatus($lmf->status());
}

function lmf_importation() {
    if (isset($_GET["nid"])) {
        $nid = $_GET["nid"];
        $lmf = new LMF();
        return $lmf->importNode($nid);
    }
    if(isset($_GET["type"])) {
        $type = $_GET["type"];
        $lmf = new LMF();
        return $lmf->importType($type);   
    }
    if (isset($_GET["vid"])) {
        $vid = $_GET["vid"];
        $lmf = new LMF();
        return $lmf->importTaxonomy($vid);
    }
    return LMFRenderer::renderImportation();
}

?>
