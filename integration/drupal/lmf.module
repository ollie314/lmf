<?php

require_once "functions.inc";

set_include_path(
    get_include_path() . PATH_SEPARATOR . 
    dirname(__FILE__) . "/lib/" . PATH_SEPARATOR . 
    dirname(__FILE__) . "/lib/lmf-client/" . PATH_SEPARATOR .
    dirname(__FILE__) . "/lib/solarium/library/");

/**
 * Implements hook_menu()
 */
function lmf_menu() {
    $items = array();
    $items["admin/config/lmf"] = array(
        "title" => "LMF",
        "description" => "Linked Media Framework module settings",
        "page callback" => "drupal_get_form",
        "page arguments" => array("lmf_admin"),
        "access arguments" => array("administer lmf settings"),
        "type" => MENU_NORMAL_ITEM
    );
    $items["lmf"] = array(
        "title" => "LMF",
        "description" => "Linked Media Framework",
        "page callback" => "lmf_main",
        "page arguments" => array(1),
        "access arguments" => array(1), 
        "file" => "lmf.pages.inc",
        "weight" => 10,
        "type" => MENU_NORMAL_ITEM
    );
    $items["lmf/status"] = array(
        "title" => "Status",
        "description" => "Content Status",
        "page callback" => "lmf_status",
        "page arguments" => array(1),
        "access arguments" => array(1), 
        "file" => "lmf.pages.inc",
        "weight" => 3,
        "type" => MENU_NORMAL_ITEM
    );
    $items["lmf/import"] = array(
        "title" => "Import",
        "description" => "Content Importation",
        "page callback" => "lmf_importation",
        "page arguments" => array(1),
        "access arguments" => array(1), 
        "file" => "lmf.pages.inc",
        "weight" => 5,
        "type" => MENU_NORMAL_ITEM
    );
    return $items;
}

/**
 * Implements hook_help()
 */
function lmf_help($path, $arg) {
    switch($path) {
        case "admin/help/lmf";
            require_once "lmf.inc";
            return LMF::getStaticPage("main");
  }
}

/**
 * Implements hook_admin()
 */
function lmf_admin() {
    $form = array();
    $form["lmf_uri"] = array(
        "#type" => "textfield",
        "#title" => t("LMF URI"),
        "#default_value" => variable_get("lmf_uri", "http://localhost:8080/LMF"),
        "#description" => t("The URI where LMF is running."),
        "#required" => TRUE
    );
    $form["lmf_user"] = array(
        "#type" => "textfield",
        "#title" => t("LMF User"),
        "#default_value" => variable_get("lmf_user", NULL),
        "#description" => t("The username to access LMF (when required)."),
        "#required" => FALSE
    );
    $form["lmf_pass"] = array(
        "#type" => "textfield",
        "#title" => t("LMF Password"),
        "#default_value" => variable_get("lmf_pass", NULL),
        "#description" => t("The password to access LMF (when required)."),
        "#required" => FALSE
    );
    $form["lmf_ctx"] = array(
        "#type" => "textfield",
        "#title" => t("LMF Context"),
        "#default_value" => variable_get("lmf_ctx", "http://localhost:8080/LMF/context/mytv"),
        "#description" => t("The context name where store the data into LMF."),
        "#required" => TRUE
    );
    return system_settings_form($form);
}

function lmf_admin_validate($form, &$form_state) {
    $uri = $form_state["values"]["lmf_uri"];
    if (!valid_url($uri, TRUE)) {
        form_set_error("lmf_uri", t("You must enter a valid URI for LMF."));
    }
    $ctx = $form_state["values"]["lmf_ctx"];
    if (!valid_url($ctx, TRUE)) {
        form_set_error("lmf_uri", t("You must enter a valid URI for the context name in LMF."));
    }
    $ctx_base = $uri . "/context/";
    if (substr($ctx, 0, strlen($ctx_base)) != $ctx_base) {
        form_set_error("lmf_uri", t("You must enter a valid context URI under your LMF instance: " . $ctx_base . "(...) ." ));
    }
}


/**
 * Implements hook_rdf_namespaces()
 */
function lmf_rdf_namespaces() {
    //return lmf_get_prefixes(); //FIXME recursive import
    try {
        $uri = variable_get("lmf_uri", "http://localhost:8080/LMF") . "/prefix";
        $response = drupal_http_request($uri, array("method"=> "GET", "headers" => array("Accept" => "application/json")));
        if ($response->code == 200) {
            $namespaces = json_decode($response->data, true);
            unset($namespaces["dc"]);
            return $namespaces;
        } else {
            return array();
        }
    } catch(Exception $e) {
        return array();
    }
}

?>
