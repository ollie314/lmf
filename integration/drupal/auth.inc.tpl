<?php

// Set your client id, service account name, and the path to your private key.
// For more information about obtaining these keys, visit:
// https://developers.google.com/console/help/#service_accounts
const CLIENT_ID = "INSERT_YOUR_CLIENT_ID";
const SERVICE_ACCOUNT_NAME = "INSERT_YOUR_SERVICE_ACCOUNT_NAME";

// Make sure you keep your key.p12 file in a secure location, and isn't
// readable by others.
const KEY_FILE = "/super/secret/path/to/key.p12";

?>
