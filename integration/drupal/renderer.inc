<?php

require_once "drupal.inc";

class LmfRenderer {

    public static function renderStatus($status) {
        global $base_url;
        $date_format = "d-m-Y";
        $output = "<p>This page summarizes the current status of the Drupal content currently handled by LMF.</p>\n";
        $output .= "<table>\n";
        $output .= "  <thead>\n";
        $output .= "    <tr>\n";
        $output .= "      <th>Node</th>\n";
        $output .= "      <th>URI</th>\n";
        $output .= "      <th>Last Modified Date</th>\n";
        $output .= "      <th>Action</th>\n";
        $output .= "    </tr>\n";
        $output .= "  </thead>\n";
        $output .= "  <tbody>\n";
        foreach ($status as $item) {
            $output .= "    <tr>\n";
            $output .= "      <td><a href=\"" . $item["uri"] . "\">". $item["path"] . "</a></td>\n";
            $output .= "      <td><a href=\"" . $item["raw"] . "\">". $item["uri"] . "</a></td>\n";
            if (is_null($item["date-drupal"])) {
                $status = TRUE;
                $bgcolor = "orange";
                $date = "drupal=<i>unknown</i> / lmf=" . date($date_format, $item["date-lmf"]);
            } else {
                $status = !($item["date-drupal"] > $item["date-lmf"]);
                $bgcolor = ($status ? "green": "red");
                $date = "drupal=" . date($date_format, $item["date-drupal"]) . " / lmf=" . date($date_format, $item["date-lmf"]);
            }
            $action = ($status ? "-" : "<a href=\"import?nid=" . $item["id"] . "\">re-import</a>");
            $output .= "      <td style=\"color: white; background-color: " . $bgcolor . ";\">" . $date . "</td>\n";
            $output .= "      <td>" . $action . "</td>\n";
            $output .= "    </tr>\n";
        }
        $output .= "  </tbody>\n";
        $output .= "<table>\n";
        return $output;
    }

    public static function renderImportation() {
        $output = LMF::getStaticPage("importation");

        $content_types_options = "";
        foreach(listContentTypes() as $type => $name) {
            $nodes = listNodes($type);
            $label = $name . " (" . count($nodes) . " nodes)";
            $content_types_options .= "<option value=\"" . $type . "\">". $label . "</option>";
        }
        $output = str_replace("{{CONTENT_TYPES_SELECT}}", $content_types_options, $output);

        $vocabularies_options = "";
        foreach(taxonomy_get_vocabularies() as $vocabulary) {
            $vocabularies_options .= "<option value=\"" . $vocabulary->vid . "\">". $vocabulary->name . "</option>";
        }
        $output = str_replace("{{VOCABULARIES_SELECT}}", $vocabularies_options, $output);

        return $output;
    }

}

?>
