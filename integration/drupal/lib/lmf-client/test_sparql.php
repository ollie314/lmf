<?php
/**
 * Created by IntelliJ IDEA.
 * User: sschaffe
 * Date: 27.01.12
 * Time: 10:48
 * To change this template use File | Settings | File Templates.
 */
require_once 'autoload.php';

use LMFClient\ClientConfiguration;
use LMFClient\Clients\SPARQLClient;

$config = new ClientConfiguration("http://localhost:8080/LMF");

$client = new SPARQLClient($config);

echo "TEST SPARQL SELECT:\n";
foreach($client->select("SELECT ?r ?n WHERE { ?r <http://xmlns.com/foaf/0.1/name> ?n }") as $row) {
    echo $row["r"] . " has name " . $row["n"] . "\n";
}


echo "TEST SPARQL ASK:\n";
echo "should be true: " . $client->ask("ASK { ?r <http://xmlns.com/foaf/0.1/name> ?n }") . "\n";
echo "should be false: " . $client->ask("ASK { ?r <http://xmlns.com/foaf/0.1/brzlbrnft> ?n }")  . "\n";


?>