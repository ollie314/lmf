<?php
namespace LMFClient\Model\RDF;

require_once 'RDFNode.php';
use LMFClient\Model\RDF\RDFNode;

/**
 * Represents an RDF Literal in PHP, optionally with language specification and datatype.
 *
 * User: sschaffe
 * Date: 25.01.12
 * Time: 10:14
 * To change this template use File | Settings | File Templates.
 */
class Literal extends RDFNode
{
    /** @var content of the literal */
    private $content;

    /** @var language of the literal (2-letter ISO code, optional) */
    private $language;

    /** @var datatype of the literal (URI, optional) */
    private $datatype;

    function __construct($content, $language = null, $datatype = null)
    {
        $this->content = $content;
        $this->language = $language;
        $this->datatype = $datatype;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getDatatype()
    {
        return $this->datatype;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    function __toString()
    {
        return $this->content;
    }


}
