<?php
namespace LMFClient\Model\RDF;

require_once 'RDFNode.php';
use LMFClient\Model\RDF\RDFNode;

/**
 * Represents a RDF URI Resource in PHP.
 *
 * User: sschaffe
 * Date: 25.01.12
 * Time: 10:13
 * To change this template use File | Settings | File Templates.
 */
class URI extends RDFNode
{
    /** @var URI of the URI Resource (string) */
    private $uri;

    function __construct($uri)
    {
        $this->uri = $uri;
    }



    function __toString()
    {
        return $this->uri;
    }

    public function getUri()
    {
        return $this->uri;
    }


}
