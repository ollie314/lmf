
function LMFConfigurationDialog() {}

LMFConfigurationDialog.prototype.show = function() {
	
	function fillForm(dialog, overlay) {
		dialog.find("input#lmf-uri").empty().val(overlay.uri);
		dialog.find("input#lmf-user").empty().val(overlay.user);
		dialog.find("input#lmf-pass").empty().val(overlay.pass);
		dialog.find("input#lmf-context").empty().val(overlay.context);
		dialog.find("input#lmf-reset").attr("checked", overlay.reset);  
	}

    var self = this; 
    var uribak;
    
    var dialog = $(DOM.loadHTML("lmf","html/lmf-configuration.html"));
    
    if (!theProject.overlayModels.lmf) {
    	LMF.readConfiguration( function(data) { 
	    	LMF.writeOverlay(data.uri, data.user, data.pass, data.context, data.reset);	
	    	self._overlay = theProject.overlayModels.lmf;
	    	fillForm(dialog, self._overlay);
		});
    } else {
    	self._overlay = theProject.overlayModels.lmf; 
    	fillForm(dialog, self._overlay);
    }
    
    dialog.find("input#lmf-uri").focusin(function() {
    	uribak = $(this).val();
    });

    dialog.find("input#lmf-uri").focusout(function() {
    	var newuri = normalizeUri($(this).val());
    	$(this).val(newuri);
    	if (newuri != uribak && newuri.length > 0) {
    		if (dialog.find("input#lmf-context").val().length == 0) {
    			dialog.find("input#lmf-context").val(buildContext(newuri));
    		} else {
    			dialog.find("input#lmf-context").val(dialog.find("input#lmf-context").val().replace(uribak, newuri));
    		}
    	}
    }); 

    dialog.find("button#cancel").click(function() {
        DialogSystem.dismissUntil(self._level - 1);
    });

    dialog.find("button#save").click(function() {
	    uri  = normalizeUri($("input#lmf-uri").val());
	    user = $("input#lmf-user").val();	
	    pass = $("input#lmf-pass").val();		
	    context = $("input#lmf-context").val();	
	    if (context.length == 0) {
	    	context = buildContext(uri);
	    }
	    checked = $("input#lmf-reset").attr("checked");
	    reset = (checked != undefined && checked == "checked");  
	    LMF.saveConfiguration(
	    		uri, user, pass, context, reset,
	    		function(data) {
			    	LMF.writeOverlay(uri, user, pass, context, reset);
			    	self._overlay = theProject.overlayModels.lmf;
			    	DialogSystem.dismissUntil(self._level - 1);
	            }
	    );
    });
	
	var frame = DialogSystem.createDialog();
    frame.width("500px");
    dialog.appendTo(frame);
    
	self._level = DialogSystem.showDialog(frame);
	
};
