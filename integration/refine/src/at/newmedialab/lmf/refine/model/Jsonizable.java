package at.newmedialab.lmf.refine.model;

import org.json.JSONObject;

public interface Jsonizable {
	
	JSONObject getJSON();

}
