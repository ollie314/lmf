package at.newmedialab.lmf.refine;
import java.util.ResourceBundle;


public class LmfConfiguration {
	
	private static ResourceBundle bundle = ResourceBundle.getBundle("lmf");
	
	public static String get(String key) {
		if (bundle.containsKey(key)) { 
			return bundle.getString(key);
		} else {
			return null;
		}
	}
	
	public static boolean getBoolean(String key) {
		String value = get(key);
		return ("true".equalsIgnoreCase(value) ? true : false);
	}

}
