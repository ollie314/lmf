package at.newmedialab.lmf.refine.commands;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.newmedialab.lmf.client.ClientConfiguration;
import at.newmedialab.lmf.client.clients.ContextClient;
import at.newmedialab.lmf.client.clients.ImportClient;
import at.newmedialab.lmf.client.exception.LMFClientException;
import at.newmedialab.lmf.refine.model.LmfConfigurationOverlayModel;

import com.google.refine.ProjectManager;
import com.google.refine.browsing.Engine;
import com.google.refine.commands.Command;
import com.google.refine.exporters.ExporterRegistry;
import com.google.refine.exporters.WriterExporter;
import com.google.refine.model.Project;

/**
 * LMF import command for Refine
 * 
 * @author Sergio Fernández
 *
 */
public class LmfImportCommand extends Command {
	
	private static Logger log = LoggerFactory.getLogger(LmfImportCommand.class);
	
	private static final String TURTLE = "text/turtle";
	
	/**
	 * Performs the command as a POST HTTP request,
	 * importing the data of the project into LMF
	 * 
	 */
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		LmfConfigurationOverlayModel overlay = this.getOverlay(this.getProject(req));
		log.debug("Importing using this configuration: " + overlay.toString());
		ClientConfiguration conf = this.buildConfiguration(overlay);
		try {
			if (overlay.isReset() && StringUtils.isNotBlank(overlay.getContext())) { 
				this.cleanNamedGraph(conf);
			}
			String data = this.getProjectData(req);
			this.uploadDataToLmf(data, conf);
			res.setStatus(HttpServletResponse.SC_OK);
		} catch (LMFClientException e) {
			log.error("Error uploading data to the LMF instance located at '" + conf.getLmfUri() + ": " + e.getMessage());
			throw new ServletException(e);
		}
	}
	
	/**
	 * Clean all content stored under the named graph passed
	 * 
	 * @param context named graph uri
	 * @throws IOException 
	 * @throws LMFClientException 
	 * @throws ClientProtocolException 
	 */
	private void cleanNamedGraph(ClientConfiguration conf) {
		ContextClient client = new ContextClient(conf);
		log.info("Removing all content in LMF (uri=" + conf.getLmfUri() + ", context=" + conf.getLmfContext() + ")...");
		if (client.delete(conf.getLmfContext())) { 
			log.info("Deleted LMF context '" + conf.getLmfContext());
		}
	}
	
	/**
	 * LMF Client upload interaction
	 * 
	 * @param data RDF data to import
	 * @throws LMFClientException
	 * @throws IOException
	 */
	private void uploadDataToLmf(String data, ClientConfiguration conf) throws LMFClientException, IOException {
		ImportClient client = new ImportClient(conf);
		log.debug("Uploading data to LMF (" + (StringUtils.isNotBlank(conf.getLmfContext()) ? conf.getLmfContext() : conf.getLmfUri()) + ")...");
		client.uploadDataset(data, TURTLE);
		log.info("Data stored in LMF at '" + (StringUtils.isNotBlank(conf.getLmfContext()) ? conf.getLmfContext() : conf.getLmfUri()));
	}
	
	/**
	 * Get the LMF overlay for this project
	 * 
	 * @param project project
	 * @return
	 */
	private LmfConfigurationOverlayModel getOverlay(Project project) {
		LmfConfigurationOverlayModel overlay = (LmfConfigurationOverlayModel) project.overlayModels.get(LmfConfigurationOverlayModel.NAME);
		if (overlay == null) {
			log.error("No suitable overlay model found, so creating a default one in the backend");
			overlay = new LmfConfigurationOverlayModel();
		}
		return overlay;
	}
	
	/**
	 * Builds a proper configuration for this project for working with LMF
	 * 
	 * @param project current project
	 * @return lmf client configuration
	 * @throws ServletException
	 */
	private ClientConfiguration buildConfiguration(LmfConfigurationOverlayModel overlay) throws ServletException {
		ClientConfiguration conf = new ClientConfiguration(overlay.getUri());
		String user = overlay.getUser();
		if (StringUtils.isNotBlank(user)) {
			conf.setLmfUser(user);
		}
		String pass = overlay.getPass();
		if (StringUtils.isNotBlank(pass)) {
			conf.setLmfPassword(pass);
		}		
		String context = overlay.getContext();
		if (StringUtils.isNotBlank(context)) {
			conf.setLmfContext(context);
		}
		return conf;
	}

	/**
	 * Retrieves the RDF data from the Refine exporters
	 * 
	 * @param req HTTP Request
	 * @return Turtle serialization of the project
	 * @throws ServletException
	 */
	private String getProjectData(HttpServletRequest req) throws ServletException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ProjectManager.singleton.setBusy(true);
		try { 
			WriterExporter exporter = (WriterExporter) ExporterRegistry.getExporter("Turtle");
			Project project = this.getProject(req);
			Properties options = new Properties();
			options.put("format", "ttl");
			Engine engine = getEngine(req, project);
			Writer writer = new OutputStreamWriter(out);
			exporter.export(project, options, engine, writer);
		} catch (Exception e) {
			String msg = "Error retrieving the data project: " + e.getMessage();
			log.error(msg);
			throw new ServletException(msg, e);
		} finally {
            ProjectManager.singleton.setBusy(false);
		}
		return out.toString();
	}

}
