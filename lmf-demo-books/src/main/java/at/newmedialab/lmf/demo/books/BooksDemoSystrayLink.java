/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.demo.books;


import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.ui.MarmottaSystrayLink;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class BooksDemoSystrayLink implements MarmottaSystrayLink {

    @Inject
    private ConfigurationService configurationService;

    /**
     * Get the label of the systray entry for displaying in the menu.
     *
     * @return
     */
    @Override
    public String getLabel() {
        return "LMF Books Demo";
    }

    /**
     * Get the link to point the browser to when the user selects the menu entry.
     *
     * @return
     */
    @Override
    public String getLink() {
        return "http://"+configurationService.getServerName()+":"+configurationService.getServerPort()+configurationService.getServerContext() + "/demo-books/about.html";
    }

    /**
     * Get the section where to add the systray link in the menu (currently either ADMIN or DEMO).
     *
     * @return
     */
    @Override
    public MarmottaSystrayLink.Section getSection() {
        return MarmottaSystrayLink.Section.DEMO;
    }

}
