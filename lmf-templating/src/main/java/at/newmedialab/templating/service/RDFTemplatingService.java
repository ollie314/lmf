/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.templating.service;

import at.newmedialab.ldpath.exception.LDPathParseException;
import at.newmedialab.lmf.ldpath.api.LDPathService;
import at.newmedialab.sesame.commons.model.Namespaces;
import at.newmedialab.sesame.facading.FacadingFactory;
import at.newmedialab.sesame.facading.api.Facading;
import at.newmedialab.templating.model.Template;
import at.newmedialab.templating.model.TemplateFacade;
import kiwi.core.api.config.ConfigurationService;
import kiwi.core.api.content.ContentService;
import kiwi.core.api.triplestore.SesameService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * User: Thomas Kurz
 * Date: 31.07.12
 * Time: 09:25
 */
@ApplicationScoped
public class RDFTemplatingService {

    @Inject
    private Logger log;

    @Inject
    private ContentService contentService;

    @Inject
    private LDPathService ldPathService;

    @Inject
    private SesameService sesameService;

    @Inject
    private ConfigurationService configurationService;

    /**
     * get all view templates that are related for the resource
     *
     * @param resource
     * @return
     */
    public List<Template> getTemplates(URI resource) {
        try {
            List<Template> templates = new ArrayList<Template>();

            RepositoryConnection con = sesameService.getConnection();
            try {
                Facading facadingService = FacadingFactory.createFacading(con);

                URI r_view = con.getValueFactory().createURI(Namespaces.NS_TEMPLATING+"hasView");

                RepositoryResult<Statement> triples = con.getStatements(null,r_view,null,true);

                while(triples.hasNext()) {
                    Statement triple = triples.next();
                    if(triple.getObject() instanceof URI && facadingService.isFacadeable((Resource)triple.getObject(), TemplateFacade.class)) {
                        templates.add(new Template(facadingService.createFacade((Resource) triple.getObject(), TemplateFacade.class)));
                    }
                }
            } finally {
                con.commit();
                con.close();
            }

            return templates;
        } catch (RepositoryException e) {
            log.error("error accessing RDF repository",e);
            return Collections.emptyList();
        }
    }

    public void addTemplate(URI resource, URI template) {
        try {
            RepositoryConnection con = sesameService.getConnection();

            URI r_view = con.getValueFactory().createURI(Namespaces.NS_TEMPLATING+"hasView");

            con.add(resource, r_view, template);

            con.close();
        } catch (RepositoryException e) {
            log.error("error accessing RDF repository",e);
        }
    }

    public void removeTemplate(URI resource, URI template) {
        try {
            RepositoryConnection con = sesameService.getConnection();

            URI r_view = con.getValueFactory().createURI(Namespaces.NS_TEMPLATING+"hasView");

            con.remove(resource, r_view, template);

            con.close();
        } catch (RepositoryException e) {
            log.error("error accessing RDF repository",e);
        }
    }

    /**
     * merges template and resource to OutputStream
     *
     * @param resource
     * @param template
     * @param out
     * @throws IOException
     */
    public void template(URI resource, URI template, OutputStream out) throws IOException {
        Writer writer;
        try {
            writer = new PrintWriter(new OutputStreamWriter(out, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            writer = new PrintWriter(new OutputStreamWriter(out));
        }
        this.template(resource, template, writer);
    }

    /**
     * merges resource and template and writes to writer
     *
     * @param resource
     * @param template
     * @param writer
     */
    public void template(URI resource, URI template, Writer writer) throws IOException {
        InputStream in = contentService.getContentStream(template, Namespaces.MIME_TYPE_TEMPLATE);
        Document doc = Jsoup.parse(in,"UTF-8",configurationService.getBaseUri());

        //search start elements
        searchStartElement(resource, doc.head());
        searchStartElement(resource,doc.body());

        //write out
        doc.outputSettings().charset("UTF-8").prettyPrint(true);

        writer.write(doc.toString());
        writer.flush();
        writer.close();
    }

    private void searchStartElement(URI basicResource, Element element) throws IOException {
        List<String> attributes = Arrays.asList("resource","about","src");

        if(hasAttr(element,attributes)) {
            LinkedList<Binding> stack = new LinkedList<Binding>();
            if(getAttr(element, attributes).startsWith("?")) {
                stack.add(new Binding(getAttr(element, attributes), basicResource));
            } else {
                stack.add(new Binding(null,sesameService.getValueFactory().createURI(getAttr(element,attributes))));
            }
            //set resource
            setAttr(element, attributes, stack.get(0).getResource().toString());

            for(Element e : element.children()) {
                searchAndReplace(e,stack);
            }
        } else {
            for(Element e : element.children()) {
                searchStartElement(basicResource,e);
            }
        }
    }

    private boolean hasAttr(Element e, List<String> list) {
        for(String s : list) {
            if(e.hasAttr(s)) return true;
        } return false;
    }

    private void setAttr(Element e, List<String> list, String value) {
        for(String s : list) {
            if(e.hasAttr(s)) e.attr(s,value);
        }
    }

    private String getAttr(Element e, List<String> list) {
        for(String s : list) {
            if(e.hasAttr(s)) return e.attr(s);
        }
        return null;
    }

    private void removeAttr(Element e, List<String> list) {
        for(String s : list) {
            e.removeAttr(s);
        }
    }

    private Collection<Value> cleanPredicates(Collection<Value> old, String type) {
        Collection<Value> n = new ArrayList<Value>();
        if(type.equals("uri")) {
            for(Value node : old) {
                if(node instanceof URI) n.add(node);
            }
        } else {
            for(Value node : old) {
                if(node instanceof Literal) {
                    //TODO remove other types
                    n.add(node);
                }
            }
        }
        return n;
    }

    private void searchAndReplace(Element element, LinkedList<Binding> bindings) throws IOException {
        List<String> relations = Arrays.asList("rel", "rev", "property");
        List<String> objects = Arrays.asList("resource", "about");
        List<String> others = Arrays.asList("title","link","src","href");

        if (hasAttr(element, relations)) {

            if (!hasAttr(element, objects) & !element.hasAttr("content")) throw new IOException("cannot parse");

            //get relations
            Collection<Value> predicates = null;
            //get last binding
            if (bindings.isEmpty()) throw new IOException("cannot parse");
            if (bindings.getFirst().getResource() instanceof Literal) throw new IOException("cannot parse");

            //language filter
            String path=null;
            String x = "<"+getAttr(element, relations)+">";
            if(element.hasAttr("language")) {
                StringBuilder b = new StringBuilder();
                String[] langs = element.attr("language").split("\\|");
                for(int i=0; i<langs.length; i++) {
                    b.append(x);
                    b.append("[@"+langs[i]+"]");
                    if(i+1<langs.length) b.append(",");
                }
                path="fn:first("+b.toString()+")";
                //TODO language should be appear in rdfa via xml:lang
                element.removeAttr("language");
            } else {
                path=x;
            }
            try {
                predicates = ldPathService.pathQuery(bindings.getFirst().getResource(),path,null);
            } catch (LDPathParseException e) {
                throw new IOException("cannot load predicates");
            }

            //clean predicates depening on datatype
            if(element.hasAttr("datatype")) {
                predicates = cleanPredicates(predicates,element.attr("datatype"));
            }

            if(element.hasAttr("max")) {
                int max = Integer.parseInt(element.attr("max"));
                while(max<predicates.size()) {
                    predicates.remove(predicates.iterator().next());
                }
                element.removeAttr("max");
            }

            //if there is none -> remove element or fill with default
            if (predicates.isEmpty()) {
                if (hasAttr(element, objects)) {
                    element.empty();
                    removeAttr(element, objects);
                }  else {
                    element.removeAttr("content");
                }

                if (element.hasAttr("default")) {
                    if (element.attr("default").startsWith("?")) {
                        for (Binding bind : bindings) {
                            if (bind.getVar().equals(element.attr("default"))) {
                                element.text(nodeToString(bind.getResource()));
                                break;
                            }
                        }
                    } else {
                        element.text(element.attr("default"));
                    }
                    element.removeAttr("default");
                } else {
                    element.remove();
                }
                return;
            }
            element.removeAttr("default");

            //if there is one ->
            Iterator<Value> iterator = predicates.iterator();
            int i = 0;
            while (iterator.hasNext()) {
                Value predicate = iterator.next();
                Node n = element;
                if (i < predicates.size()-1) {
                    n = n.clone();
                    element.before(n);
                    if(element.hasAttr("seperator")) {
                        element.before(element.attr("seperator"));
                        element.removeAttr("seperator");
                    }
                } else {
                    n = element;
                    if(element.hasAttr("seperator")) {
                        element.removeAttr("seperator");
                    }
                }

                if (n.hasAttr("content")) {
                    ((Element)n).html(nodeToString(predicate));
                    n.removeAttr("content");
                } else {
                    bindings.addFirst(new Binding(getAttr(((Element)n), objects), predicate));
                    setAttr(((Element)n), objects, nodeToString(predicate));

                    int j = 0;
                    for (Element child : ((Element)n).children()) {
                        searchAndReplace(child, bindings);
                    }

                    bindings.removeFirst();
                }
                i++;
            }
        } else if (element.hasAttr("content")) {
            //ceck binding
            Value b = null;
            for (Binding bind : bindings) {
                if (bind.getVar().equals(element.attr("content"))) {
                    b = bind.getResource();
                    break;
                }
            }
            if (b == null) throw new IOException("cannot parse");
            element.html(nodeToString(b));
            element.removeAttr("content");
        } else if (hasAttr(element, objects)) {
            Value b = null;
            for (Binding bind : bindings) {
                if (bind.getVar().equals(getAttr(element, objects))) {
                    b = bind.getResource();
                    break;
                }
            }
            if (b == null) throw new IOException("cannot parse");
            setAttr(element, objects, nodeToString(b));
            for (Element child : element.children()) {
                searchAndReplace(child, bindings);
            }
        } else {
            if(hasAttr(element,others)) {
                for(String s : others) {
                    if(element.hasAttr(s)) {
                        if(element.attr(s).startsWith("?")) {
                            for(Binding b : bindings) {
                                if(b.var.equals(element.attr(s))) {
                                    element.attr(s,nodeToString(b.getResource()));
                                }
                            }
                        }
                    }
                }
            }
            for(Element e : element.children()) {
                searchAndReplace(e,bindings);
            }
        }

    }

    private String nodeToString(Value node) {
        return node.stringValue();
    }

    private class Binding {
        private String var;
        private Value resource;

        private Binding(String var, Value resource) {
            this.var = var;
            this.resource = resource;
        }

        public String getVar() {
            return var;
        }

        public void setVar(String var) {
            this.var = var;
        }

        public Value getResource() {
            return resource;
        }

        public void setResource(Value resource) {
            this.resource = resource;
        }
    }
}
