/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.templating.model;

import at.newmedialab.sesame.commons.model.Namespaces;
import at.newmedialab.sesame.facading.annotations.RDF;
import at.newmedialab.sesame.facading.annotations.RDFType;
import at.newmedialab.sesame.facading.model.Facade;

/**
 * User: Thomas Kurz
 * Date: 31.07.12
 * Time: 09:56
 */
@RDFType(Namespaces.NS_TEMPLATING+"Template")
public interface TemplateFacade extends Facade {

	@RDF(Namespaces.NS_DC+"title")
	public void setTitle(String title);
	public String getTitle();

}
