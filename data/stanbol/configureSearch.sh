#!/bin/bash
URL=${LMF_URL:-"http://localhost:8080/LMF/"}
URL=${URL%/}

declare -A CONF
CONF['resources.browsercache.enabled']="false"
CONF['resources.servercache.enabled']="false"
CONF['content.triplestore.enabled']="true"
CONF['content.triplestore.pattern']=".*"
CONF['solr.enabled']="true"
CONF['solr.omit_cached']="true"
CONF['solr.local_only']="false"

for key in "${!CONF[@]}"; do
	val="${CONF["$key"]}"
	echo "Setting $key = $val"
	curl -i -X POST -H "Content-Type: application/json" -d"[\"$val\"]" ${URL}/config/data/$key && echo
done

# some initial data
curl -i -H "Content-Type: text/plain" -X POST --data-binary @rdfs.kwrl  ${URL}/reasoner/program/sn && echo
curl -i -H "Content-Type: application/rdf+xml" -X POST -d @web_annotation.owl.xml ${URL}/import/upload && echo


curl -i -H "Content-Type: text/plain" -X POST "${URL}/cache/endpoint?name=Stanbol&prefix=http%3A%2F%2Fdbpedia.org&kind=CACHE&expiry=86400&endpoint=http%3A%2F%2Fdev.iks-project.eu%3A8081%2Fentityhub%2Fsite%2Fdbpedia%2Fentity%3Fid%3D%7Buri%7D&mimetype=application%2Frdf%2Bxml" && echo



#upload search core
curl -i -X POST -H "Content-Type: text/plain" -d @web_annotation_search.txt ${URL}/solr/cores/stanbol && echo
curl -i -X GET ${URL}/solr/cores/stanbol && echo
