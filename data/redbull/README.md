REDBULL DEMO
============

To make the demo up and running:

1. move data/MI201003310017.ogv and data/MI201003310018.ogv to  lmf-webapp/src/main/webapp/DATA/redbull/
2. make sure, that the import.sh URL variable is correct
3. If you want to have linked data resources, check data/Generic/import.xml, data/MI201003310017_Metadata/import.xml and data/MI201003310018_Metadata/import.xml resource prefix 'lmf'
4. run import script
5. wait a second, until the external resources are cached
6. there is a bug? import it again, and it works...
7. got to BASE_URL/media/redbull/index.html
