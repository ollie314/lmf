Files:
 * iptc2sn-mapping.n3
   mapping of sn-ressort labels to iptc-mediatopic codes.
 * skos-snressort.n3
   propretary thesaurus of the sn-ressorts, based on the test data.
 * skos-sncore.search
   search core for semantic search based on skos-snressort.n3
 * sn-model-ontoloty.n3
   ontology for the data modes used in the SNImport-Family (News, Blog, Video)
 * sn.kwrl
   important reasoning for sncore to work
