#!/bin/bash
URL=${LMF_URL:-"http://localhost:8080/LMF/"}
URL=${URL%/}

#important config settings
declare -A CONF
CONF['resources.browsercache.enabled']="false"
CONF['resources.servercache.enabled']="false"
#CONF['security.enabled']="false"
CONF['solr.enabled']="true"
CONF['solr.omit_cached']="false"
CONF['solr.local_only']="false"

for key in "${!CONF[@]}"; do
	val="${CONF["$key"]}"
	echo "Setting $key = $val"
	curl -i -X POST -H "Content-Type: application/json" -d"[\"$val\"]" ${URL}/config/data/$key && echo
done

# LMF-Cache blacklists

curl -i -H "Content-Type: text/plain" -X POST "${URL}/cache/endpoint?name=Mediawiki&prefix=~http%3A%2F%2F%5B%5E%2F%5D*wiki%5Bpm%5Dedia%5C.org&kind=NONE&expiry=86400&endpoint=&mimetype=" && echo
curl -i -H "Content-Type: text/plain" -X POST "${URL}/cache/endpoint?name=SN%20Salzburger%20Nachrichten&prefix=~http%3A%2F%2F%5B%5E%2F%5D*salzburg%5C.com&kind=NONE&expiry=86400&endpoint=&mimetype=" && echo

# some initial data
curl -i -H "Content-Type: text/plain" -X POST --data-binary @sn.kwrl  ${URL}/reasoner/program/sn && echo
curl -i -H "Content-Type: text/n3" -X POST -d @skos-snressorts.n3 ${URL}/import/upload && echo
curl -i -H "Content-Type: text/n3" -X POST -d @sn-model-ontology.n3 ${URL}/import/upload && echo

#upload search core
curl -i -X POST -H "Content-Type: text/plain" -d @skos-sncore.search ${URL}/solr/cores/sn && echo
curl -i -X GET ${URL}/solr/cores/sn && echo

