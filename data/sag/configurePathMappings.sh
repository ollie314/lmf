#!/bin/bash
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/project.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/projects?pattern=%2FVolumes%2Fprojects%2F(%5Ba-zA-Z0-9-_%5D%2B)%2F.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/project.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/projectssnml?pattern=%2FVolumes%2Fprojects%2Fsnml%2Fprojects%2F(%5Ba-zA-Z0-9-_%5D%2B)%2F.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/project.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/projectssnmltng?pattern=%2FVolumes%2Fprojects%2Fsnml-tng%2Fprojects%2F(%5Ba-zA-Z0-9-_%5D%2B)%2F.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/word.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/word?pattern=.*docx%3F%24'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/excel.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/excel?pattern=.*xlsx%3F%24'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/powerpoint.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/powerpoint?pattern=.*pptx%3F%24'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/pdf.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/pdf?pattern=.*pdf%24'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/proposal.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/proposal?pattern=.*%2F11_antrag_proposal.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/contract.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/contract?pattern=.*%2F12_vertrag_contract.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/deliverable.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/deliverable?pattern=.*%2F21_deliverables.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/meeting.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/meeting?pattern=.*%2F22_meetings.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/budget.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/budget?pattern=.*%2F03_finanzen_budget.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/report.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/report?pattern=.*%2F04_berichte_reports.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/dissemination.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/dissemination?pattern=.*%2F06_informationsmaterial_publicrelations.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/archive.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/archive?pattern=.*%2F(archive%3F%7CARCHIVE%3F%7CArchive%3F)%2F.*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/final.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/final?pattern=.*(%2Ffinal%2F%7C_final).*'
curl -i -H "Content-Type: text/plain" -X POST \
    -d @sparql/kmt.sparql  \
    'http://localhost:8080/LMF/fsimport/mapping/kmt?pattern=%2FVolumes%2Funits%2Fkmt%2F.*'


