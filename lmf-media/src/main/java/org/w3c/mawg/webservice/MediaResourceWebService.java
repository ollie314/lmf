/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.w3c.mawg.webservice;

import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.openrdf.model.URI;
import org.openrdf.repository.Repository;
import org.w3c.mawg.model.MediaFormat;
import org.w3c.mawg.service.MultimediaMetadataService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * User: Thomas Kurz
 * Date: 15.02.12
 * Time: 14:11
 */
@ApplicationScoped
@Path("/media/mediaont-api")
public class MediaResourceWebService {

	@Inject
	MultimediaMetadataService mediaService;

    @Inject
    SesameService sesameService;

	@Path("/create")
	@POST
    @Produces("application/json")
	public Response create(@QueryParam("uri")String uri) {

		try {
            Repository repository = sesameService.getRepository();
		    URI resource = mediaService.create(uri,repository);
			return Response.ok().entity(resource).location(new java.net.URI(resource.getLocalName())).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@Path("/read")
	@Produces("application/json")
	@GET
	public Response read(@QueryParam("uri")String uri,@QueryParam("properties")String properties, @QueryParam("format")String format) {
		String[]props = properties!=null ? properties.split(","): null;
		try {
            Repository repository = sesameService.getRepository();
			String result = mediaService.read(uri,MediaFormat.valueOf(format.toUpperCase()),props,repository);
			return Response.ok().entity(result).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

    @Path("/update")
    @PUT
    public Response update(@QueryParam("uri")String uri,@QueryParam("metadata")String metadata, @QueryParam("format")String format) {

        try {
            Repository repository = sesameService.getRepository();
            mediaService.update(uri, metadata, MediaFormat.valueOf(format.toUpperCase()), repository);
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

	@Path("/delete")
	@DELETE
	public Response delete(@QueryParam("uri")String uri,@QueryParam("format")String format) {
		//TODO
		//not tested
		return Response.ok().build();
	}

	@Path("/original")
	@Produces("application/json")
	@GET
	public Response original(@QueryParam("uri")String uri,@QueryParam("format")String format) {
		try {
            Repository repository = sesameService.getRepository();
			String result = mediaService.original(uri, MediaFormat.valueOf(format.toUpperCase()),repository);
			return Response.ok().entity(result).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}
}
