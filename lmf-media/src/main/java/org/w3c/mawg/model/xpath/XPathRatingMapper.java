/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.w3c.mawg.model.xpath;

import nu.xom.Attribute;
import nu.xom.Document;
import org.apache.marmotta.commons.sesame.facading.FacadingFactory;
import org.apache.marmotta.commons.sesame.facading.api.Facading;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.w3c.mawg.model.Constants;
import org.w3c.mawg.model.LocationFacade;
import org.w3c.mawg.model.RatingFacade;

import java.util.UUID;

/**
 * User: Thomas Kurz
 * Date: 09.03.12
 * Time: 18:28
 */
public class XPathRatingMapper implements XPathMapper{

	String xpath;
	public XPathRatingMapper(String xpath) {
		this.xpath = xpath;
	}

	@Override
    public void toTriples(URI resource, nu.xom.Document document, RepositoryConnection connection, ValueFactory factory, ConfigurationService configurationService) throws RepositoryException {
		if(document.query(xpath+"@average", Constants.ytContext).size()!=0) {
			String value = document.query(xpath+"@average", Constants.ytContext).get(0).getValue();
			String max = document.query(xpath+"@max", Constants.ytContext).get(0).getValue();
			String min = document.query(xpath+"@min", Constants.ytContext).get(0).getValue();

            Facading facading = FacadingFactory.createFacading(connection);
            URI property = factory.createURI(Constants.MA_NAMESPACE+"rating");

            RatingFacade rating = facading.createFacade(configurationService.getBaseUri() + "resource/" + UUID.randomUUID(), RatingFacade.class);

			rating.setValue(Double.valueOf(value));
			rating.setMax(Integer.valueOf(max));
			rating.setMin(Integer.valueOf(min));

            connection.add(resource, property, rating.getDelegate());
		}
	}
}
