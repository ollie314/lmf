/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.w3c.mawg.model.xpath;

import nu.xom.Attribute;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Nodes;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.w3c.mawg.model.Constants;

import javax.enterprise.context.ApplicationScoped;

/**
 * User: Thomas Kurz
 * Date: 09.03.12
 * Time: 15:31
 */
public class XPathStringMapper implements XPathMapper{

	String xpath;
	String attribute;

	public XPathStringMapper(String xpath, String attribute) {
		this.xpath = xpath;
		this.attribute = attribute;
	}

	@Override
    public void toTriples(URI resource, Document document, RepositoryConnection connection, ValueFactory factory, ConfigurationService configurationService) throws RepositoryException {
		Nodes nodes = document.query(xpath, Constants.ytContext);
		for(int i=0; i<nodes.size();i++) {
			String value;
			if(nodes.get(i) instanceof Attribute) {
				value = ((Attribute)nodes.get(i)).getValue();
			} else {
				value = ((Element)nodes.get(i)).getValue();
			}

            URI property = factory.createURI(Constants.MA_NAMESPACE+attribute);
            Literal node = factory.createLiteral(value.trim());

			connection.add(resource,property,node);
		}
	}
}
