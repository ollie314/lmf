/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.w3c.mawg.model;

import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;

/**
 * User: Thomas Kurz
 * Date: 26.03.12
 * Time: 11:47
 */
@RDFType(Constants.MA_NAMESPACE+"Location")
public interface LocationFacade  extends Facade {

	@RDF(Constants.MA_NAMESPACE+"locationLatitude")
	Double getLatitude();
	void setLatitude(Double latitude);

	@RDF(Constants.MA_NAMESPACE+"locationLongitude")
	Double getLongitude();
	void setLongitude(Double longitude);

    @RDF(Constants.MA_NAMESPACE+"locationAltitude")
    Double getAltitude();
    void setAltitude(Double altitude);

}
