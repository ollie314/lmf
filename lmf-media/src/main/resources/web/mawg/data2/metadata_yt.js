/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var DATA_META = [
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y1b-identifier.xml",properties:"identifier",result:"data2/Yb-identifier_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y2b-title.xml",properties:"title",result:"data2/yb-title_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y3b-language.xml",properties:"language",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y4b-locator.xml",properties:"locator",result:"data2/yb-locator_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y5b-contributor.xml",properties:"contributor",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y6b-creator.xml",properties:"creator",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y7b-date.xml",properties:"date",result:"data2/yb-date_ma_YT.json",remark:"testdata is wrong (in dc, date specific includes 'language', in yt not)"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y8b-location.xml",properties:"location",result:"data2/yb-location_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y9b-description.xml",properties:"description",result:"data2/yb-description_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y10b-keyword.xml",properties:"keyword",result:"data2/yb-keyword_ma_YT.json",remark:"testdata is wrong ('value' and 'keyword' are not the same)"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y11b-genre.xml",properties:"genre",result:"data2/ya-genre_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y12b-rating.xml",properties:"rating",result:"data2/yb-rating_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y13b-relation.xml",properties:"relation",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y14b-collection.xml",properties:"collection",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y15b-copyright.xml",properties:"copyright",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y16b-policy.xml",properties:"policy",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y17b-publisher.xml",properties:"publisher",result:"data2/yb-publisher_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y18b-targetAudience.xml",properties:"targetAudience",result:"data2/yb-targetaudience_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y19b-fragment.xml",properties:"fragment",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y20b-namedFragment.xml",properties:"namedFragment",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y21b-frameSize.xml",properties:"frameSize",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y22b-compression.xml",properties:"compression",result:"data2/yb-compression_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y23b-duration.xml",properties:"duration",result:"data2/yb-duration_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y24b-format.xml",properties:"format",result:"data2/yb-format_ma_YT.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y25b-samplingRate.xml",properties:"samplingRate",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y26b-frameRate.xml",properties:"frameRate",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y27b-averageBitRate.xml",properties:"averageBitRate",result:"data2/no_mapping.json"},
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/Y28b-numTracks.xml",properties:"numTracks",result:"data2/no_mapping.json"}
];

var DATA_ORIG = {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/y29-getOriginalMetadata.xml",result:"data2/getOriginalMetadata-YT.json"};