/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {

AjaxSolr.theme.prototype.result = function (doc, snippet, response) {
  var title = doc.title
  if (response && response.highlighting && response.highlighting[doc.id]) {
	  var title = response.highlighting[doc.id].title;
  }

  var output = '<div class="result"><h2><a href="player.html#'+doc["lmf.uri"]+'">' + title + '</a></h2>';
  output += '<table><tr><td style="padding:5px">';
  var h3 = "";
  var src="https://internal.redbullcontentpool.com/webcache/"+doc.internalid+"/";
  if(doc["lmf.uri"].indexOf("#")>-1) {
    var hash = utils_fragments_url_HashParser(doc["lmf.uri"]);
    if(hash.time.end==4294967295) {
        h3 += '<h4>start: '+hash.time.start+'</h4>';
    }
    else h3 += '<h4>start: '+hash.time.start+', end: '+hash.time.end+'</h4>';
    src+=hash.time.start;
  } else {
    src+="10";
    h3 += "<h4>duration: "+doc.duration+"</h4>";
  }
  src+="/keyframe.jpg";
  output += "<img src='"+src+"'></td><td style='padding:5px'>";
  output += '<h4>type: '+doc.typelabel+'</h4>';
  output += h3;
  output += '<h4>creation: '+doc.date+'</h4>';
  output += '<p>' + snippet + '</p></td></tr></table></div>';
  return output;
};

AjaxSolr.theme.prototype.snippet = function (doc, response) {
  if (response && response.highlighting && response.highlighting[doc.id]) {
	  var output = response.highlighting[doc.id].summary;
	  return output;
  }
  var output = doc.summary;
  return output;
};

AjaxSolr.theme.prototype.tag = function (facet, weight, handler) {
  return $('<a href="#" class="tagcloud_item"/>').text(facet.value+"("+facet.count+") ").addClass('tagcloud_size_' + weight).click(handler);
};

AjaxSolr.theme.prototype.facet_link = function (value, handler) {
  return $('<a href="#"/>').text(value).click(handler);
};

AjaxSolr.theme.prototype.no_items_found = function () {
  return 'no items found in current selection';
};


AjaxSolr.theme.prototype.list_selected = function (list, items, separator) {
  jQuery(list).html("");
  for (var i = 0, l = items.length; i < l; i++) {
    jQuery(list).append(items[i]);
  }
};


})(jQuery);

