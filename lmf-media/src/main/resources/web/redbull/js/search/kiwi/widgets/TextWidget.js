/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {

AjaxSolr.TextWidget = AjaxSolr.AbstractFacetWidget.extend({
  appendix : '',
  init: function () {
    var self = this;
    $(this.target).find('#search-field').bind('keydown', function(e) {
      if (e.which == 13) {
        $(this.target).find('#search-submit').click();
      }
    });
    $(this.target).find('#search-submit').bind('click', function(e) {
        window.location.hash = $('#search-field').val();
        var value = $(this.target).find('#search-field').val();
        if(value=="") {alert('Please enter query string!');return;}
        self.manager.store.addByValue('q', '('+value+')'+self.appendix);
        self.manager.doRequest(0);
    });
    /*
    $(this.target).find('#reset').bind('click', function(e) {
        var loc = window.location.href.slice(0,window.location.href.indexOf('?')+1);
        if(loc.indexOf("?") != -1)loc=window.location.href.slice(0,window.location.href.indexOf('?'));
        window.location.href = loc;
        $(this.target).find('#text').val('');
    });
    */
  }
});

})(jQuery);