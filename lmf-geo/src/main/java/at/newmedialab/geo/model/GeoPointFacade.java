/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.geo.model;


import org.apache.marmotta.commons.constants.Namespace;
import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;

/**
 * A point, described using WGS84 (coordinate system relative to Earth).
 * @author Jakob Frank <jakob.frank@salzburgresearch.at>
 *
 */
@RDFType(Namespace.GEO.Point)
public interface GeoPointFacade extends Facade {

	@RDF(Namespace.GEO.lat)
	public abstract double getLatitude();
	public abstract void setLatitude(double lat);

	@RDF(Namespace.GEO.long_)
	public abstract double getLongitude();
	public abstract void setLongitude(double lng);

	@RDF(Namespace.GEO.alt)
	public abstract double getAltitude();
	public abstract void setAltitude(double alt);
	
}