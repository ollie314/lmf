/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.geo.api;

import java.util.List;

import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;

import at.newmedialab.geo.model.PlaceFacade;

/**
 * Resolve places at geonames.org
 * @author Jakob Frank <jakob.frank@salzburgresearch.at>
 *
 */
public interface GeoService {

    /**
     * Lookup the locatonString in the local repository or at geonames.org and return it as a {@link PlaceFacade}
     * @param connection the connection to use for lookup and facading
     * @param locationString the location to lookup 
     * @return the {@link PlaceFacade} or {@code null} if it was not found.
     */
    public PlaceFacade resolvePlace(RepositoryConnection connection, String locationString);

    /**
     * Lookup the locationString in the local repository or at geonames.org and return its {@link URI}.
     * @param connection the connection to use for lookup
     * @param locationString the location to lookup
     * @return the {@link URI} or {@code null} if it was not found.
     */
    public URI resolveLocation(RepositoryConnection connection, String locationString);
    
    /**
     * Lookup the locationStirng at geonames.org and return its {@link URI}.
     * @param locationString the location to lookup
     * @return the {@link URI} or {@code null} if it was not found.
     */
    public URI resolveLocation(String locationString);

    /**
     * Lookup the placeNames at geonames.org and return their {@link URI}s.
     * placeNAmes that could not be resolved are omitted.
     * @param placeNames the names to lookup.
     * @return List of URIs that were resolved.
     */
    public List<URI> resolveLocations(List<String> placeNames);

    /**
     * Lookup the placeNames in the local repository or at geonames.org and return their {@link URI}s.
     * placeNAmes that could not be resolved are omitted.
     * @param connection the connection to use for lookup.
     * @param placeNames the names to lookup.
     * @return List of URIs that were resolved.
     */
    public List<URI> resolveLocations(RepositoryConnection connection, List<String> placeNames);

    /**
     * Lookup the placeNames in the provided {@link RepositoryConnection} or at geonames.org and return the corresponding {@link PlaceFacade}s.
     * placeNAmes that could not be resolved are omitted.
     * @param connection the {@link RepositoryConnection} to use for lookup and facading.
     * @param placeNames the names to lookup.
     * @return List of URIs that were resolved.
     */
    public List<PlaceFacade> resolvePlaces(RepositoryConnection connection, List<String> placeNames);

    /**
     * Lookup the placeNames in the provided {@link RepositoryConnection} or at geonames.org and return the corresponding {@link PlaceFacade}s.
     * placeNAmes that could not be resolved are omitted.
     * @param connection the {@link RepositoryConnection} to use for lookup and facading.
     * @param placeNames the names to lookup.
     * @return Array of URIs that were resolved.
     */
    public PlaceFacade[] resolvePlaces(RepositoryConnection connection, String... placeNames);

    /**
     * Lookup the placeNames in the local repository or at geonames.org and return their {@link URI}s.
     * placeNAmes that could not be resolved are omitted.
     * @param connection the connection to use for lookup.
     * @param placeNames the names to lookup.
     * @return Array of URIs that were resolved.
     */
    public URI[] resolveLocations(RepositoryConnection connection, String... placeNames);

    /**
     * Lookup the placeNames at geonames.org and return their {@link URI}s.
     * placeNAmes that could not be resolved are omitted.
     * @param placeNames the names to lookup.
     * @return Array of URIs that were resolved.
     */
    public URI[] resolveLocations(String... placeNames);


}
