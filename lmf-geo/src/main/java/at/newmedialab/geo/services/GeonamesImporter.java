/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.geo.services;

import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.importer.Importer;
import org.apache.marmotta.platform.core.api.task.Task;
import org.apache.marmotta.platform.core.api.task.TaskManagerService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.exception.io.MarmottaImportException;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * User: Thomas Kurz
 * Date: 23.05.11
 * Time: 21:46
 */
@ApplicationScoped
public class GeonamesImporter implements Importer {

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private TaskManagerService taskManagerService;

    @Inject
    private Logger               log;

    @Inject
    private SesameService sesameService;


    /**
     * Get the name of this importer. Used for presentation to the user and for internal
     * identification.
     *
     * @return a string uniquely identifying this importer
     */
    @Override
    public String getName() {
        return "GeoNames";
    }

    /**
     * Get a description of this importer for presentation to the user.
     *
     * @return a string describing this importer for the user
     */
    @Override
    public String getDescription() {
        return "Importer for GeoNames RDF data dumps with one RDF/XML document per line";
    }

    /**
     * Get a collection of all mime types accepted by this importer. Used for automatically
     * selecting the appropriate importer in ImportService.
     *
     * @return a set of strings representing the mime types accepted by this importer
     */
    @Override
    public Set<String> getAcceptTypes() {
        return Collections.singleton("application/vnd.geonames.rdf");
    }

    /**
     * Import data from the input stream provided as argument into the KiWi database.
     *
     *
     * @param url the url from which to read the data
     * @param format the mime type of the import format
     * @param user the user to use as author of all imported data
     * @return the number of Content Items imported
     * @throws MarmottaImportException
     *             in case the import fails
     */
    @Override
    public int importData(URL url, String format, Resource user, URI context) throws MarmottaImportException {
        try {
            return importData(url.openStream(), format, user, context);
        } catch (IOException e) {
            log.error("I/O error while opening connection to {}: {}", url, e.getMessage());
            return 0;
        }
    }

    /**
     * Import data from the input stream provided as argument into the KiWi database.
     *
     *
     * @param is the input stream from which to read the data
     * @param format the mime type of the import format
     * @param user the user to use as author of all imported data
     * @return the number of Content Items imported
     * @throws MarmottaImportException
     *             in case the import cannot execute
     */
    @Override
    public int importData(InputStream is, String format, Resource user, URI context) throws MarmottaImportException {
        return importData(new InputStreamReader(is), format, user, context);
    }

    private int workerCount = 0;

    /**
     * Import data from the reader provided as argument into the KiWi database.
     *
     *
     * @param reader the reader from which to read the data
     * @param format the mime type of the import format
     * @param user the user to use as author of all imported data
     * @return the number of Content Items imported
     * @throws MarmottaImportException
     *             in case the import fails
     */
    @Override
    public int importData(Reader reader, String format, Resource user, final URI context) throws MarmottaImportException {
        long i = 0;
        Task task = taskManagerService.createSubTask("GeoNames Importer", "Importer");
        task.updateMessage("Initializing ...");

        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(1, 8, 60, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100),
                new ThreadPoolExecutor.CallerRunsPolicy());
        ThreadPoolWatcher watcher = new ThreadPoolWatcher("GeoNames Importer", threadPool);
        watcher.setDaemon(true);
        watcher.start();

        try {
            BufferedReader r = new BufferedReader(reader);
            String s;

            boolean endOfStream = false;
            long lineCounter = 0;
            task.updateMessage("reading dump...");
            while (!endOfStream) {
                // read a batch of lines
                final int batchSize = configurationService.getIntConfiguration("geonames.batchsize", 100) / 2 * 2;
                final List<String> lines = new ArrayList<String>(batchSize);
                while (lines.size() < batchSize && (s = r.readLine()) != null) {
                    // s = new String(r.readLine().getBytes(), "UTF-8");
                    lines.add(s);
                    lineCounter++;
                }

                final long currentLineCount = lineCounter;

                task.updateProgress(currentLineCount);

                threadPool.execute(new Runnable() {
                    @Override
                    public void run() {
                        final String name = "GeoNames Worker " + ++workerCount;
                        Thread.currentThread().setName(name);
                        long start = System.currentTimeMillis();

                        Task workerTask = taskManagerService.createTask(name, "Importer");
                        workerTask.updateMessage("importing a batch of " + lines.size() / 2);
                        workerTask.updateTotalSteps(lines.size());

                        try {
                            RepositoryConnection connection = sesameService.getConnection();
                            try {
                                for (int i = 0; i < lines.size(); i++) {
                                    String s = lines.get(i);
                                    if (s.startsWith("<?xml ")) {
                                        try {
                                            if (context == null) {
                                                connection.add(new StringReader(s), configurationService.getBaseUri(), RDFFormat.RDFXML);
                                            } else {
                                                connection.add(new StringReader(s), configurationService.getBaseUri(), RDFFormat.RDFXML, context);
                                            }
                                        } catch (RDFParseException e) {
                                            log.error("error while parsing resoure, skipping");
                                        } catch (RepositoryException e) {
                                            log.error("repository error while adding resource, skipping");
                                        } catch (IOException e) {
                                            log.error("I/O error while adding resource, skipping");
                                        }
                                    } else if (s.startsWith("http://sws.geonames.org/")) {
                                        workerTask.updateDetailMessage("resource", s);
                                    } else {
                                        workerTask.removeDetailMessage("resource");
                                    }
                                    workerTask.updateProgress(i + 1);
                                }
                            } finally {
                                connection.commit();
                                connection.close();
                            }
                        } catch (RepositoryException e) {
                            log.error("error initializing Sesame repository ...");
                        } finally {
                             workerTask.endTask();
                        }
                        log.info("GeoNames: {} resources imported ({} ms), total count now {}",
                                new Object[] { lines.size(), System.currentTimeMillis() - start, currentLineCount });
                    }
                });

                if (lines.size() < batchSize) {
                    endOfStream = true;
                }

            }

        } catch (IOException e) {
            log.error("I/O error while reading GeoNames RDF Dump");
        } finally {
            // The ThreadPool will continue until all queued threads are finished.
            threadPool.shutdown();
            task.endTask();
        }
        return (int) i;
    }

    private class ThreadPoolWatcher extends Thread {

        private final ThreadPoolExecutor threadPool;

        public ThreadPoolWatcher(String name, ThreadPoolExecutor threadPool) {
            super(name);
            this.threadPool = threadPool;
        }

        @Override
        public void run() {
            final Task t = taskManagerService.createTask(getName(), "Importer");

            t.updateMessage("Batch-Importing geonames.org");

            while (!threadPool.isTerminated()) {
                t.updateTotalSteps(threadPool.getTaskCount());
                t.updateProgress(threadPool.getCompletedTaskCount());

                t.updateDetailMessage("workers", "" + threadPool.getActiveCount());

                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                }
            }

            t.endTask();
        }
    }

}
