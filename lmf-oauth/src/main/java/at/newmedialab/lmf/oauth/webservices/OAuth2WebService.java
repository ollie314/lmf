/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.webservices;

import at.newmedialab.lmf.oauth.utils.KiWiJSONresponse;
import at.newmedialab.lmf.oauth.utils.KiWiNumberUtils;
import at.newmedialab.lmf.oauth.utils.KiWiUrlUtils;

import at.newmedialab.lmf.oauth.services.TokenManagerServiceImpl;

import at.newmedialab.lmf.oauth.model.AccessToken;
import at.newmedialab.lmf.oauth.model.RefreshToken;

import at.newmedialab.lmf.oauth.exception.AccessTokenNotValidException;

import at.newmedialab.lmf.oauth.api.CookieService;
import at.newmedialab.lmf.oauth.api.TokenManagerService;

import kiwi.core.api.config.ConfigurationService;
import kiwi.core.api.facading.FacadingService;
import kiwi.core.api.transaction.Transaction;
import kiwi.core.api.transaction.TransactionService;
import kiwi.core.api.user.UserService;
import kiwi.core.exception.UserDoesNotExistException;
import kiwi.core.exception.UserServiceException;
import kiwi.core.model.Constants;
import kiwi.core.model.rdf.KiWiResource;
import kiwi.core.model.rdf.KiWiUriResource;
import kiwi.core.model.user.KiWiUser;
import kiwi.core.model.user.OnlineAccount;
import nu.xom.*;
import org.apache.commons.configuration.ConversionException;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * @author Stephanie Stroka
 * User: Stephanie Stroka
 * Date: 08.08.2011
 * Time: 17:45:22
 */
@Path("auth/oauth2")
public class OAuth2WebService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private TokenManagerService tokenManagerService;

    @Inject
    private CookieService cookieService;

    @Inject
    private TransactionService transactionService;

    @Inject
    private FacadingService facadingService;

    @Inject
    private UserService userService;


    private static final String OAUTH2SERVER_PATTERN="/{oauth2server:[+-@a-zA-Z0-9]+}";
    private static final String MIME_PATTERN="/{mimetype:[^/]+/[^/]+}";
    private static final String REDIRECT_PATTERN="/{redirect_uri:^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]+[-a-zA-Z0-9+&@#/%=~_|]}";

    /**
     * loginOAuth2() is a generic method that creates requests for oauth2 service providers
     * to retrieve an authoriation code, which is used to request oauth2 access token.
     * Currently, LMF supports requests to facebook's and google's oauth2 (for youtube) services.
     *
     * Currently supported calls:
     * {lmf_base_uri}}/auth/oauth2/facebook
     * {lmf_base_uri}}/auth/oauth2/youtube
     *
     * @param oauth2server the oauth2 authentication server
     * @return the redirect url to the oauth2 service provider
     * @HTTP 303 redirect to the proper oauth2 service provider
     * @HTTP 404 requested oauth2 service provider not found
     * @HTTP 415 requested media-type not supported
     * @HTTP 500 the redirect url is corrupt
     */
    @Path("/{server}")
    @GET
    public Response loginOAuth2(@Context HttpServletRequest request,
            @PathParam("server") String oauth2server,
            @QueryParam("mime_type") String mime_type,
            @QueryParam("redirect_uri") String redirect_uri,
            @QueryParam("callback") String callback) {

        // is the oauth server configured?
        if(!configurationService.isConfigurationSet(oauth2server+".oauth_code_req"))
            return Response.status(404)
                    .entity("LMF does not support oauth2 authorization via: " + oauth2server).build();
        try {

            // check whether mime type has been specified.
            // If not, return xhtml if header accept allows it.
            // Define json in any other case.
            if(mime_type != null &&
                    (mime_type.equals(Constants.MIME_TYPE_HTML) ||
                            mime_type.equals(Constants.MIME_TYPE_XHTML) ||
                            mime_type.equals(Constants.MIME_TYPE_ALL))) {
                mime_type = Constants.MIME_TYPE_XHTML;
            } else if(mime_type != null && mime_type.equals(Constants.MIME_TYPE_JSON)
                    || callback != null) {
                mime_type = Constants.MIME_TYPE_JSON;
            } else {
                String header_accept = request.getHeader("Accept");
                log.debug("header_accept: {}", header_accept);

                if(header_accept.contains(Constants.MIME_TYPE_JSON)) {
                    log.debug("mime: {}", header_accept);
                    mime_type = Constants.MIME_TYPE_JSON;
                } else if(header_accept.contains(Constants.MIME_TYPE_HTML)
                        || header_accept.contains(Constants.MIME_TYPE_XHTML)) {
                    mime_type = Constants.MIME_TYPE_XHTML;
                } else if(header_accept.contains(Constants.MIME_TYPE_ALL)) {
                    mime_type = Constants.MIME_TYPE_JSON;
                } else
                    return Response.status(415)
                            .entity("LMF does not support mime type: " + header_accept).build();
            }

            if(redirect_uri != null) {
                try {
                    redirect_uri = URLDecoder.decode(redirect_uri, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return Response.status(500).entity("Redirect URI is corrupt.").build();
                }
            }
            OAuth2Data data = new OAuth2Data(oauth2server, mime_type, redirect_uri, callback);


            // checks whether redirect uri and callback url are registered/configured
            if(redirect_uri != null) {
                String client_id = getClientId(oauth2server, getLookupKey(data));
                String client_secret = getClientSecret(oauth2server, getLookupKey(data));

                if(client_id == null || client_secret == null) return Response.status(403).entity("Your client is not authorized to use LMF OAuth2.0." +
                        "Check the LMF configuration or contact an administrator.").build();
            }

            String oauth2request = getOAuth2codeRequestURL(data);

            log.info("oauth2request: {}", oauth2request);
            if(oauth2request == null) return Response.status(500).entity("LMF is wrongly configured. " +
                    "Login via " + oauth2server + " is not possible.").build();

            return Response.seeOther(new URI(oauth2request)).build();
        } catch (URISyntaxException e) {
            e.printStackTrace();

            return Response.status(500).entity("The redirect url is corrupt").build();
        }
    }

    @Path("/tokenRequest" + OAUTH2SERVER_PATTERN + MIME_PATTERN)
    @GET
    @Produces( {"application/json", "application/xhtml+xml"} )
    public Response oauth2tokenRequest(
            @PathParam("oauth2server") String oauth2server,
            @PathParam("mimetype") String mimetype,
            @QueryParam("code") String code,
            @QueryParam("error_reason") String error_reason,
            @QueryParam("error") String error,
            @QueryParam("error_description") String error_description,
            @Context HttpHeaders headers) {
        return oauth2tokenRequest(oauth2server, mimetype, null, code, error_reason, error, error_description, headers);
    }

    /**
     * oauth2tokenRequest() is requested by the oauth2 service provider during a second
     * handshake as a callback service. The method parameter are either an authorization code
     * or error parameters (error, error_reason, error_description).
     * If a code has been received, the method sends an oauth2 token request to an oauth2
     * service provider (youtube, facebook) and returns the access token, refresh token (optional)
     * and expiration time encoded in JSON or as a cookie.
     * 
     * @param oauth2server the oauth2 service provider (currently supported: facebook, youtube)
     * @param code the oauth2 authorization code which has been provided by the oauth2 service provider
     *        during the first handshake of the application-side authorization
     * @param error_reason Error output
     * @param error Error output
     * @param error_description A description of the error
     * @return access token, refresh token (optional) and expiration time (body is a JSON message)
     * @HTTP 200 Ok
     * @HTTP 500 Internal Server Error
     * @HTTP 401 Not Authorized
     */
    @Path("/tokenRequest" + OAUTH2SERVER_PATTERN + MIME_PATTERN + "/{rest}")
    @GET
    @Produces( {"application/json", "application/xhtml+xml"} )
    public Response oauth2tokenRequest(
            @PathParam("oauth2server") String oauth2server,
            @PathParam("mimetype") String mimetype,
            @PathParam("rest") String rest,
            @QueryParam("code") String code,
            @QueryParam("error_reason") String error_reason,
            @QueryParam("error") String error,
            @QueryParam("error_description") String error_description,
            @Context HttpHeaders headers) {

        String callback = null;
        String redirect_uri = null;
        Properties prop = extractOptionalProperties(rest);
        if(prop != null) {
            callback = prop.getProperty("callback");
            redirect_uri = prop.getProperty("redirect_uri");
        }
        log.debug("mime-type: {}", mimetype);
        log.debug("oauth2server: {}", oauth2server);
        log.debug("header cookies: {}", headers.getCookies().size());
        log.debug("redirect_uri: {}", redirect_uri);
        log.debug("callback: {}", callback);
        log.debug("code: {}", code);

        OAuth2Data data = new OAuth2Data(oauth2server, mimetype, redirect_uri, callback);
        data.auth_code = code;

        // check for errors...
        if(error != null)
            return buildErrorResponse(
                    data, error, error_description, error_reason, 401);
        else if(code == null) return buildErrorResponse(data,
                "No authorization code received",
                "No authorization code received",
                "Due to unexpected reasons, no authorization code has been received from server " + data.oauth2server,
                500);

        // no errors? Proceed with oauth token request
        HttpURLConnection con = null;
        try {
            con = requestToken(data);
        } catch(IOException e) {
            e.printStackTrace();
            log.error("I/O processing error due to: {} ", e.getMessage());
            return buildErrorResponse(data,
                    "I/O Processing failed",
                    "LMF failed due to an I/O processing error",
                    e.getMessage(),
                    500);
        }

        assert con != null;

        // read input stream from connection
        BufferedReader in = null;
        try {
            try {
                in = readTokenResponse(con);
            } catch (IOException e) {
                e.printStackTrace();
                log.error("I/O processing error due to: {} ", e.getMessage());
                return buildErrorResponse(data,
                        "I/O Processing failed",
                        "LMF failed due to an I/O processing error",
                        e.getMessage(),
                        500);
            } catch (UnsupportedOperationException e) {
                e.printStackTrace();
                log.error("readTokenResponse() failed: {} ", e.getMessage());
                return buildErrorResponse(data,
                        "LMF failed due to an internal error",
                        "LMF failed due to an internal error",
                        "LMF failed due to an internal error",
                        500);
            }

            // parse from connection, receive access_token, expiration and refresh_token
            try {
                parseTokenResponse(in, data);
            } catch (IOException e) {
                return buildErrorResponse(data,
                        "I/O Processing failed",
                        "LMF failed due to an I/O processing error",
                        "Cannot read content from input stream of "+con.getURL().toString() +
                        "Expected mime-type is 'application/x-www-form-urlencoded' or 'application/json'.",
                        500);
            }

        } finally {
            try {
                if(in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();

                return buildErrorResponse(data,
                        "I/O Processing failed",
                        "LMF failed due to an I/O processing error",
                        "Output stream of "+con.getURL().toString()+" is broken.",
                        500);
            }
        }


        // Retrieve user: The user is either the currently logged in user or a newly created user
        KiWiUser authenticatedUser = null;
        if(data.access_token != null) {
            log.debug("caching access token.. {} ..expires in: {}", data.access_token, data.expires_in);
            log.debug("refresh token.. {} ..oauth2server: {}", data.refresh_token, data.oauth2server);
            try {
                authenticatedUser = cacheAccessToken(data);
            } catch (UserServiceException e) {
                e.printStackTrace();

                return buildErrorResponse(data,
                        "User management failed",
                        "LMF failed due to a user management processing error",
                        "LMF failed due to a user management processing error",
                        500);
            } catch (UserDoesNotExistException e) {
                e.printStackTrace();

                String userName = null, onlineAccount = null;
                userName = e.getMessage();
                onlineAccount = configurationService.getStringConfiguration(data.oauth2server + ".account") + userName;
                return buildRegisterFirstForm(userName, onlineAccount, redirect_uri);
            }
            userService.setCurrentUser((KiWiUriResource)authenticatedUser.getDelegate());
        }

        Response response = buildSuccessResponse(data, authenticatedUser);

        return response;


    }

    /**
     * Establishes a connection to the token service provider and
     * sends the request.
     * @param data the OAuth2Data object contains the information about what service to request
     * @return con a HttpURLConnection that has been opened up for the requested oauth2 server
     * @throws IOException if the connection could not be established
     */
    private HttpURLConnection requestToken(OAuth2Data data) throws IOException {

        String tokenReqUrl = configurationService.getStringConfiguration(data.oauth2server+".access_token_uri");
        log.info("Trying to connect to {} ", tokenReqUrl);

        HttpsURLConnection con = null;

        try {
            con = (HttpsURLConnection) new URL(tokenReqUrl).openConnection();
        } catch (IOException e) {
            throw new IOException("LMF misconfiguration: Cannot establish connection to " + tokenReqUrl);
        }

        //        con.setHostnameVerifier(new HostnameVerifier() {
        //            @Override
        //            public boolean verify(String arg0, SSLSession arg1) {
        //                return true;
        //            }
        //        });

        String tokenRequest = getOAuth2tokenRequest(data);

        if(tokenRequest == null) throw new IOException("LMF Configuration problem for oauth2server " + data.oauth2server);

        if("lmf-test123".equals(data.auth_code)) throw new IOException("This is just a test: " + tokenRequest);

        con.setDoOutput(true);
        OutputStreamWriter out = null;

        try {
            out = new OutputStreamWriter(con.getOutputStream());
            out.write(tokenRequest);
            out.close();
        } catch (IOException e) {
            throw new IOException("Cannot write to " + tokenReqUrl);
        }

        return con;
    }

    /**
     * Gets the Inputstream of an already open HttpURLConnection and
     * returns a configured BufferedReader.
     *
     * @param con the open HttpURLConnection that should be read from
     * @return the BufferedReader for the connection
     * @throws IOException thrown when reading from the connection was not possible
     * @throws UnsupportedOperationException thrown when marking of the BufferedReader is not possible
     */
    private BufferedReader readTokenResponse(HttpURLConnection con) throws IOException, UnsupportedOperationException {
        BufferedReader in = null;
        try {
            in = new BufferedReader(
                    new InputStreamReader(
                            con.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("Cannot read from input stream of "+con.getURL().toString());
        }
        if(in.markSupported()) {
            try {
                in.mark(1024);
            } catch (IOException e) {
                e.printStackTrace();

                throw new IOException("Cannot set read ahead mark for input stream of "+con.getURL().toString());
            }
        } else {
            log.error("Marking of BufferedReader not supported. Fix this!");
            throw new UnsupportedOperationException("Marking of BufferedReader not supported. Fix this!");
        }
        return in;
    }

    /**
     * Parses the BufferedReader from a connection and extracts and stores its
     * data in the OAuth2Data object. The access token, the refresh token and
     * the expires_in time are added to the OAuth2Data object.
     * 
     * @param in the BufferedReader from the connection InputStream
     * @param data the OAuth2Data object that is extended with the
     *        access and refreh token and the expiration time
     * @throws IOException
     */
    private void parseTokenResponse(BufferedReader in, OAuth2Data data) throws IOException {
        // store falues in responseArgs
        HashMap<String,Object> responseArgs = new HashMap<String,Object>();
        try {
            String inputLine;
            // try to parse x-www-form-urlencoded content
            while((inputLine = in.readLine()) != null) {
                log.debug("read from connection: {}", inputLine);
                String[] args = inputLine.split("\\&");
                for(String s : args) {
                    String[] keyVal = s.split("=");
                    if(keyVal.length != 2) {
                        log.warn("Error while parsing response arguments. Trying to parse JSON...");
                        in.reset();
                        // if parsing x-www-form-urlencoded fails, throw exception and try to parse json
                        throw new IOException("Error while parsing response arguments. Trying to parse JSON...");
                    } else {
                        responseArgs.put(keyVal[0], keyVal[1]);
                    }
                }
            }

        } catch(IOException e) {
            // try to parse json if x-www-form-urlencoded failed
            try {
                ObjectMapper mapper = new ObjectMapper();
                responseArgs = mapper.readValue(in, HashMap.class);
            } catch (IOException e1) {
                e1.printStackTrace();
                throw new IOException(
                        "Expected mime-type 'application/x-www-form-urlencoded' or 'application/json'.");
            }
        }

        // enrich OAuth2Data object
        if(responseArgs.containsKey("expires")) {
            if(responseArgs.get("expires").getClass() == String.class) {
                data.expires_in = (String) responseArgs.get("expires");
            } else if(responseArgs.get("expires").getClass() == Integer.class) {
                data.expires_in = ((Integer) responseArgs.get("expires")).toString();
            }
        } else if(responseArgs.containsKey("expires_in")) {
            if(responseArgs.get("expires_in").getClass() == String.class) {
                data.expires_in = (String) responseArgs.get("expires_in");
            } else if(responseArgs.get("expires_in").getClass() == Integer.class) {
                data.expires_in = ((Integer) responseArgs.get("expires_in")).toString();
            }
        }
        if(responseArgs.containsKey("access_token")) {
            data.access_token = (String) responseArgs.get("access_token");
        }
    }

    /**
     * Called by oauth2tokenRequest to extract the redirect_uri and
     * callback_uri from the encoded "rest" - the end of the
     * tokenRequest url
     * (/tokenRequest"+OAUTH2SERVER_PATTERN + MIME_PATTERN + "/{rest}).
     * 
     * @param encoded_string the rest of the tokenRequest url, containing
     *        optional parameters like the redirect_uri and the callback_uri.
     * @return the optional parameters as Properties.
     */
    private Properties extractOptionalProperties(String encoded_string) {
        if(encoded_string == null) return null;
        Properties prop = new Properties();

        try {
            String str = URLDecoder.decode(encoded_string, "UTF-8");
            String keyvaluepairs[] = str.split("&");
            for(String kvpair : keyvaluepairs) {
                String kv[] = kvpair.split("=");
                if(kv.length == 2) {
                    prop.setProperty(kv[0], kv[1]);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return prop;
    }

    /**
     * Build an XHTML representation for the error response if the token request was unsuccessful
     * @param oauth2server the service provider that failed to authorize the user
     * @param error_description an error description in human readable format for the user
     * @return An XHTML error response entity
     */
    private byte[] buildXHtmlErrorResponseEntity(String oauth2server, String error_description) {

        // Tx apparently needed to query user data
        Transaction tx = transactionService.getTransaction();
        transactionService.begin(tx);
        StringBuilder sb = new StringBuilder();

        sb.append("<?xml version=\"1.0\"?> \n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"> \n");
        sb.append("<title>\n");
        sb.append("LMF - Error \n");
        sb.append("<style type=\"text/css\" media=\"screen\">@import \"" + configurationService.getServerUri() + "css/basic.css\";</style> \n" +
                "\t<style type=\"text/css\" media=\"screen\">@import \"" + configurationService.getServerUri() + "css/tabs.css\";</style> \n");
        sb.append("</title> \n");
        sb.append("<body> \n");
        sb.append("<h1> \n");
        sb.append("LMF - Linked Media Framework \n");
        sb.append("</h1> \n");
        sb.append("<h2> \n");

        sb.append("An error happened\n");

        sb.append("</h2> \n");
        sb.append("<div id=\"main\"> \n");
        sb.append("<div id=\"contents\">  \n");

        sb.append("Could not authenticate with server: ");
        sb.append(oauth2server);
        sb.append("<br/>");
        sb.append("Reason: ");
        sb.append(error_description);

        sb.append("</div> \n");
        sb.append("</div> \n");
        sb.append("</body> \n");
        sb.append("</html> \n");

        transactionService.commit(tx);

        String xhtml;
        xhtml = sb.toString();

        log.info("{}", xhtml);

        return xhtml.getBytes();
    }


    /**
     * Build an XHTML representation for a register form if the user logged in for
     * the first time with his/her (external) oauth2 account.
     * @param username the user name that was extracted from the online account data
     * @param onlineaccount the online account with which the user tried to log in
     * @return An XHTML response entity
     */
    private Response buildRegisterFirstForm(String username, String onlineaccount, String redirect_uri) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\"?> \n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"> \n");
        sb.append("<head>");
        sb.append("<title>\n");
        sb.append("LMF - Welcome " + username + "\n");
        sb.append("<style type=\"text/css\" media=\"screen\">@import \"" + configurationService.getServerUri() + "css/basic.css\";</style> \n" +
                "\t<style type=\"text/css\" media=\"screen\">@import \"" + configurationService.getServerUri() + "css/tabs.css\";</style> \n");
        sb.append("</title> \n");
        sb.append("</head>");

        sb.append("<body>");
        sb.append("<div id='registerForm'>");
        sb.append("<h3>");
        sb.append("Please register your account with LMF");
        sb.append("</h3>");
        sb.append("<br/>");
        if(redirect_uri != null) {
            sb.append("<form method='POST' action='" + configurationService.getServerUri() + "auth/register?redirect_uri=" + redirect_uri + "'>");
        } else {
            sb.append("<form method='POST' action='" + configurationService.getServerUri() + "auth/register'>");
        }
        sb.append("Username: ");
        sb.append("<input name='username' type='text' value='"+username+"'></input>");
        sb.append("<br/>");
        sb.append("Password: ");
        sb.append("<input name='password' type='password' value=''></input>");
        sb.append("<input name='onlineaccount' type='hidden' value='"+onlineaccount+"'></input>");
        sb.append("<br/>");
        sb.append("<input name='Register new LMF account' type='submit'></input>");
        sb.append("</form>");
        sb.append("</div>");
        sb.append("</body>");

        sb.append("</html>");

        return Response.status(200).entity(sb.toString().getBytes()).build();
    }

    /**
     * Build an XHTML representation for a successful token request
     * @param oauth2server the service provider that failed to authorize the user
     * @param user the user that was authorized by the service provider
     * @return An XHTML response entity
     */
    private byte[] buildXHtmlResponseEntity(String oauth2server, KiWiUser user) {

        // Tx apparently needed to query user data
        Transaction tx = transactionService.getTransaction();
        transactionService.begin(tx);
        StringBuilder sb = new StringBuilder();

        sb.append("<?xml version=\"1.0\"?> \n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"> \n");
        sb.append("<title>\n");
        if(user.getFirstName() != null && user.getLastName() != null) {
            sb.append("LMF - Welcome " + user.getFirstName() + " " + user.getLastName() + "\n");
        } else {
            sb.append("LMF - Welcome " + user.getNick() + "\n");
        }
        sb.append("<style type=\"text/css\" media=\"screen\">@import \"" + configurationService.getServerUri() + "css/basic.css\";</style> \n" +
                "\t<style type=\"text/css\" media=\"screen\">@import \"" + configurationService.getServerUri() + "css/tabs.css\";</style> \n");
        sb.append("</title> \n");
        sb.append("<body> \n");
        sb.append("<h1> \n");
        sb.append("LMF - Linked Media Framework \n");
        sb.append("</h1> \n");
        sb.append("<h2> \n");
        if(user.getFirstName() != null && user.getLastName() != null) {
            sb.append("Welcome " + user.getFirstName() + " " + user.getLastName() + "\n");
        } else if(user.getNick() != null) {
            sb.append("Welcome " + user.getNick() + "\n");
        } else {
            //userService.get
        }
        sb.append("</h2> \n");
        sb.append("<div id=\"main\"> \n");
        sb.append("<div id=\"contents\">  \n");

        String onlineAccountNick = null;
        for(OnlineAccount account : user.getOnlineAccounts()) {
            if(account.getAccountServiceHomepage() != null &&
                    account.getAccountServiceHomepage().equals(
                            configurationService.getStringConfiguration(oauth2server + ".serviceHomepage")
                            )) {
                onlineAccountNick = account.getAccountName();
                break;
            }
        }
        if(onlineAccountNick == null) {
            onlineAccountNick = user.getNick();
        }

        sb.append("<a href=\""+
                configurationService.getServerUri() +
                "social/"+oauth2server+"/import/user?username="+onlineAccountNick
                +"\">Import</a> user data. \n");
        sb.append("</div> \n");
        sb.append("</div> \n");
        sb.append("</body> \n");
        sb.append("</html> \n");

        transactionService.commit(tx);

        String xhtml;
        xhtml = sb.toString();

        log.info("{}", xhtml);

        return xhtml.getBytes();
    }

    /**
     * extractKiWiUserFromOAuthServer() extracts the username from the data returned by the OAuth2 server
     * @param authServer the oauth2 server which is queried for data
     * @param accessToken the oauth2 access token
     * @return an existing or a new KiWiUser with the username returned by the OAuth2 server
     * @throws MalformedURLException thrown when the authServer.APIurl is malformed or empty
     * @throws UserServiceException thrown when the user could not be extracted from
     *         the remote server or if he could not be created.
     */
    private KiWiUser extractKiWiUserFromOAuthServer(String authServer, String accessToken)
            throws MalformedURLException, UserServiceException, UserDoesNotExistException {

        if(configurationService.getStringConfiguration(authServer+".APIurl") == null) {
            log.error("Authorization server {} is unknown or URL to access the API is corrupt. " +
                    "Extracting user data is not possible.", authServer);
            return null;
        }

        String userName = null;
        // query the user data of the local user
        URL url = new URL(configurationService.getStringConfiguration(authServer+".APIurl") + accessToken);
        try {
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });

            InputStream in = con.getInputStream();

            // facebook returns user data as JSON msg
            if(authServer.equals("facebook")) {
                ObjectMapper mapper = new ObjectMapper();
                HashMap<String,String> userData = mapper.readValue(in, HashMap.class);
                userName = userData.get("username");
                if(userName == null) {
                    userName = userData.get("id");
                }

            }
            // youtube returns user data as XML msg
            else if(authServer.equals("youtube")) {
                Builder builder = new Builder();
                try {
                    Document xom = builder.build(in);

                    XPathContext context = new XPathContext("base", xom.getRootElement().getNamespaceURI());
                    context.addNamespace("yt", "http://gdata.youtube.com/schemas/2007");

                    Nodes nodes = xom.query("/base:entry/yt:username", context);

                    if(nodes.size() != 1) {
                        log.error("Could not extract username from youtube data document. " +
                                "Youtube data document is corrupt");
                        throw new UserServiceException("Could not extract username from youtube data document");
                    }
                    Node n = nodes.get(0);
                    assert n.getValue() != null;
                    if(n.getValue().contains("@")) {
                        userName = n.getValue().split("@")[0];
                    } else {
                        userName = n.getValue();
                    }

                } catch (ParsingException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Transaction tx = transactionService.getTransaction();

        transactionService.begin(tx);

        // TODO: test getUserByOnlineAccount();
        String account = configurationService.getStringConfiguration(authServer+".account") + userName;
        KiWiResource userResource = userService.getUser(account);
        KiWiUser currentUser = null;
        if(userResource == null) {
            currentUser = facadingService.createFacade(userService.getCurrentUser(), KiWiUser.class);
            if (userService.isAnonymous(userService.getCurrentUser())) {
                // TODO: throw new "no user was found. new user can be created with the extracted username"
                //userResource = userService.createUser(userName, PasswordGenerator.generatePassword());
                transactionService.commit(tx);
                throw new UserDoesNotExistException(userName);
            }
        } else {
            currentUser = facadingService.createFacade(userResource, KiWiUser.class);
        }
        log.info("Added online account {} to user",
                configurationService.getStringConfiguration(authServer+".account") + userName);
        OnlineAccount onlineAccount = facadingService.createFacade(
                configurationService.getStringConfiguration(authServer+".account") + userName, OnlineAccount.class);

        onlineAccount.setAccountServiceHomepage(
                configurationService.getStringConfiguration(authServer + ".serviceHomepage"));
        onlineAccount.setAccountName(userName);

        Set<OnlineAccount> onlineAccounts = currentUser.getOnlineAccounts();
        if(onlineAccounts == null) {
            onlineAccounts = new HashSet<OnlineAccount>();
        }
        onlineAccounts.add(onlineAccount);
        currentUser.setOnlineAccounts(onlineAccounts);

        transactionService.commit(tx);
        return currentUser;
    }

    /**
     * cacheAccessToken() caches oauth2 token with LMF user name.
     * access_token, refresh_token (if available) and expiration time is cached.
     * @param data oauth2 specific data object containing access token, oauth2server and more
     * @throws UserServiceException is thrown when the user could not be queried
     * or when it was null.
     */
    private KiWiUser cacheAccessToken(OAuth2Data data)
            throws UserServiceException, UserDoesNotExistException {

        // get the current user or create a new user with the same username
        KiWiUser user = null;
        try {
            user = extractKiWiUserFromOAuthServer(data.oauth2server, data.access_token);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new UserServiceException("Could not query remote user.");
        }

        if(user == null) throw new UserServiceException("Could not create user.");

        // add session token
        int expires;
        try {
            expires = configurationService.getIntConfiguration("oauth2.defaultExpiration");
        } catch(ConversionException e) {
            // hard coded default value if LMF configuration was not set or corrupt
            expires = 3600;
        }
        if(data.expires_in != null) {
            try {
                expires = new Integer(data.expires_in).intValue();
            } catch(NumberFormatException e) {
                log.error("Could not parse expiration time.");
            }
        }
        try {
            TokenManagerServiceImpl.TokenBundle tb =
                    tokenManagerService.setToken(user, data.oauth2server, data.access_token, data.refresh_token, expires);
            if(tb == null) throw new UserServiceException("Token could not be cached.");
            AccessToken at = tb.getAccessToken();
            if(at != null) {
                data.lmf_access_token = at.getAccessToken();
                if(at.getExpirationDate() != null) {
                    Date expiration_date = at.getExpirationDate();
                    String diff = Long.valueOf((expiration_date.getTime() - System.currentTimeMillis()) / 1000).toString();
                    data.lmf_expires_in = diff;
                }
            }
            RefreshToken rt = tb.getRefreshToken();
            if(rt != null) {
                data.lmf_refresh_token = rt.getRefreshToken();
            }
        } catch (AccessTokenNotValidException e) {
            e.printStackTrace();
            throw new UserServiceException("User authentication failed due to an internal error.");
        }

        return user;
    }

    /**
     * Build a servlet response for error messages
     * @param data the erroneous oauth2.0 data object
     * @param error the short name of the error
     * @param error_description a description of the error
     * @param error_reason the reason that explains why the error happened
     * @param status the status code for the response
     * @return the response object
     */
    private Response buildErrorResponse(OAuth2Data data,
            String error,
            String error_description,
            String error_reason,
            int status) {
        Object responseEntity = null;
        byte[] entity = null;
        if(data.mime_type.equals(Constants.MIME_TYPE_JSON)) {
            entity = buildJsonErrorResponseEntity(error, error_description, error_reason);
        } else {
            entity = buildXHtmlErrorResponseEntity(data.oauth2server, error_description + ": " + error_reason);
        }

        if(data.callback_from_LMF != null) {

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(data.callback_from_LMF);
            post.addHeader("Content-Type","application/json");
            post.addHeader("Location", data.callback_from_LMF);

            post.setEntity(new ByteArrayEntity(entity));

            try {
                HttpResponse r = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        if(data.mime_type.equals(Constants.MIME_TYPE_JSON)) {
            if(data.redirect_from_LMF != null) {

                try {
                    return Response.seeOther(new URI(data.redirect_from_LMF)).entity(entity).build();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
        return Response.status(status).entity(entity).build();
    }

    /**
     * Builds a Response for successful OAuth2 calls
     * @param data oauth2.0 relevant data
     * @param authenticated_user the authenticated user to provide information about the service user
     *
     * @return a servlet response containing the access token and additional information.
     * The returned values depend on the mime type. E.g., for XHTML requests, the access token
     * is returned as a cookie, but not displayed in the XHTML.
     */
    private Response buildSuccessResponse(OAuth2Data data,
            KiWiUser authenticated_user) {
        byte[] entity = null;
        List<NewCookie> cookies = new ArrayList<NewCookie>();

        if(data.mime_type.equals(Constants.MIME_TYPE_XHTML)) {
            entity = buildXHtmlResponseEntity(data.oauth2server, authenticated_user);
            NewCookie c = cookieService.buildCookie("oauth_token", data.lmf_access_token, data.lmf_expires_in);
            cookies.add(c);

        } else if(data.mime_type.equals(Constants.MIME_TYPE_JSON)) {
            entity = buildJsonResponseEntity(data.lmf_access_token, data.lmf_refresh_token, data.lmf_expires_in, authenticated_user);
        }

        Response response = null;
        if(data.callback_from_LMF != null) {

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(data.callback_from_LMF);
            post.addHeader("Content-Type","application/json");

            post.setEntity(new ByteArrayEntity(entity));
            try {
                HttpResponse r = client.execute(post);

                if(r.getStatusLine().getStatusCode() == 200) {

                    if(r.getEntity() != null) {
                        InputStream instream = r.getEntity().getContent();

                        BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
                        String line = null;
                        if((line = reader.readLine()) != null) {
                            // sanitize location url
                            if(line.contains("\"")) {
                                line = line.replace("\"", "");
                            }
                            if(line.contains("\\")) {
                                line = line.replace("\\", "");
                            }
                            // TODO: retrieve redirect uri from client here?!
                            //new_location = line;
                        }
                    }

                    Header[] header_cookies = r.getHeaders("Set-Cookie");
                    for(Header h : header_cookies) {
                        String cookieString = h.getValue();
                        String[] cookieProperties = cookieString.split(";");

                        String c_name = null;
                        String c_value = null;
                        String c_expires = null;
                        String c_path = null;
                        String c_domain = null;
                        String c_version = null;

                        for(String s : cookieProperties) {
                            String[] cookieKeyValue = s.split("=");
                            if(cookieKeyValue.length != 2) {
                                log.error("Received corrupt cookie");
                            } else {
                                String key = cookieKeyValue[0].trim();
                                String value = cookieKeyValue[1].trim();
                                if(key.equals("expires")) {
                                    c_expires = value;
                                } else if(key.equals("path")) {
                                    c_path = value;
                                } else if(key.equals("domain")) {
                                    c_domain = value;
                                } else if(key.equals("version")) {
                                    c_version = value;
                                } else {
                                    c_name = key;
                                    c_value = value;
                                }
                            }
                        }
                        NewCookie c = cookieService.buildCookie(
                                c_name, c_value, c_path, c_domain, c_version, c_expires, false);
                        cookies.add(c);
                    }
                }
                response = Response.seeOther(new URI(data.redirect_from_LMF)).build();
            } catch (IOException e) {
                e.printStackTrace();
                return buildErrorResponse(data,
                        "Redirect error",
                        "The redirect to " + data.redirect_from_LMF + " failed",
                        "I/O Exception",
                        Response.Status.BAD_REQUEST.ordinal());
            } catch (URISyntaxException e) {
                e.printStackTrace();
                return buildErrorResponse(data,
                        "Redirect error",
                        "The redirect to " + data.redirect_from_LMF + " failed",
                        "I/O Exception",
                        Response.Status.BAD_REQUEST.ordinal());
            }
        } else if(data.redirect_from_LMF != null) {
            try {
                response = Response.seeOther(new URI(data.redirect_from_LMF)).build();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                return buildErrorResponse(data,
                        "Redirect error",
                        "The redirect to " + data.redirect_from_LMF + " failed",
                        "I/O Exception",
                        Response.Status.BAD_REQUEST.ordinal());
            }
        } else {
            response = Response.status(200).entity(entity).build();
        }

        if(cookies != null) {
            NewCookie[] cookie_array = new NewCookie[cookies.size()];
            for(int i=0; i<cookies.size(); i++) {
                cookie_array[i] = cookies.get(i);
            }
            response = Response.fromResponse(response).cookie(cookie_array).build();
        }
        return response;
    }

    /**
     * Build a response entity in JSON to send it back to the token requester (client)
     * @param error the error parameter according to OAuth2.0 spec
     * @param error_description the error_description parameter according to OAuth2.0 spec
     * @param error_reason  the error_reason parameter according to OAuth2.0 spec
     * @return
     */
    private byte[] buildJsonErrorResponseEntity(String error,
            String error_description,
            String error_reason) {
        HashMap<String,String> errorResponse = new HashMap<String,String>(3);
        if(error != null) {
            errorResponse.put("error", error);
        }
        if(error_reason != null) {
            errorResponse.put("error_reason", error_reason);
        }
        if(error_description != null) {
            errorResponse.put("error_description", error_description);
        }
        return KiWiJSONresponse.buildJSONresponseEntity(errorResponse);
    }

    /**
     * buildJsonResponse() constructs a token or error response in JSON format
     * @param access_token the access token provided by the oauth2 server
     * @param refresh_token the refresh token provided by the oauth2 server
     * @param expires the expiration time provided by the oauth2 server
     * @return a Response object
     */
    private byte[] buildJsonResponseEntity(String access_token, String refresh_token, String expires, KiWiUser user) {
        // return token in the following form:
        // {"expires_in":"3600","refresh_token":"dae53cf5e69234ff564466c471dd5e8","access_token":"7da2bbe23a89d2a2649d56f8c5312d16"}
        HashMap<String,String> tokenResponse = new HashMap<String,String>(3);
        if(expires != null) {
            tokenResponse.put("expires_in", expires);
        }
        if(refresh_token != null) {
            tokenResponse.put("refresh_token", refresh_token);
        }
        if(user != null) {
            // Tx apparently needed to query user data
            Transaction tx = transactionService.getTransaction();
            transactionService.begin(tx);
            tokenResponse.put("username", user.getNick());
            transactionService.commit(tx);
        }
        if(access_token != null) {
            tokenResponse.put("access_token", access_token);
            return KiWiJSONresponse.buildJSONresponseEntity(tokenResponse);
        } else
            return buildJsonErrorResponseEntity("No Access Token", "No access token found", "No access token found");
    }

    /**
     * Collect necessary parameters and construct the request URI string for the token request
     *
     * @param data the OAuth2Data object that contains the relevant information for an oauth2.0 request
     * @return a string representation of the token request URI
     */
    private String getOAuth2tokenRequest(OAuth2Data data) {

        //data.grant_type = "authorization_code";

        String propertyKey = getLookupKey(data);

        String client_id = getClientId(data.oauth2server, propertyKey);
        if(client_id == null) {
            log.error("No client id configured for callback {} (hash: {} ) on oauth 2.0 server " + data.oauth2server,
                    propertyKey);
            return null;
        }
        data.client_id = client_id;

        String client_secret = getClientSecret(data.oauth2server, propertyKey);
        if(client_secret == null) {
            log.error("No client secret configured for callback {} (hash: {} ) on oauth 2.0 server " + data.oauth2server,
                    propertyKey);
            return null;
        }
        data.client_secret = client_secret;
        // callback to LMF should already be defined
        if(data.callback_to_LMF == null) {
            data.callback_to_LMF = constructCallbackService(data);
        }

        return buildOAuth2tokenRequest(data);
    }

    private String getClientSecret(String oauth2server, String hash) {
        return getClientProperty(oauth2server, hash, "client_secret");
    }
    private String getClientId(String oauth2server, String hash) {
        return getClientProperty(oauth2server, hash, "client_id");
    }

    private String getClientProperty(String oauth2server, String hash, String property) {
        Properties client_properties = configurationService.getPropertiesConfiguration(oauth2server+"."+property);

        String client_property = client_properties.getProperty(hash);
        return client_property;
    }

    /**
     * Hashes a part of the callback string to provide a lookup for client ids and secrets
     * in the property file.
     * 
     * @param data the OAuth2Data object
     * @return a String that is the hashcode of parts of the callback_to_LMF url
     */
    private String getLookupKey(OAuth2Data data) {
        String lookupKey = null;
        if(data.redirect_from_LMF != null && data.callback_from_LMF != null) {
            try {
                lookupKey = data.mime_type+"/" +
                        URLEncoder.encode("redirect_uri="+data.redirect_from_LMF +
                                "&callback=" + data.callback_from_LMF, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else if(data.redirect_from_LMF != null) {
            try {
                lookupKey = data.mime_type+"/" +
                        URLEncoder.encode("redirect_uri="+data.redirect_from_LMF, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            lookupKey = data.mime_type;
        }
        int hash = lookupKey.hashCode();
        long unsignedHash = KiWiNumberUtils.toUnsignedInt(hash);
        return Long.valueOf(unsignedHash).toString();
    }

    /**
     * constructs a callback string for a given oauth2server, a redirect_uri,
     * a remote callback uri and a mime type.
     * 
     * @param data the oauth2 data
     * @return a callback string that is handed to the oauth2 server
     */
    private String constructCallbackService(OAuth2Data data) {
        String callbackService = null;
        if(data.redirect_from_LMF != null && data.callback_from_LMF != null) {
            try {
                callbackService = "auth/oauth2/tokenRequest/"+
                        data.oauth2server+"/"+
                        data.mime_type+"/"+
                        URLEncoder.encode("redirect_uri="+data.redirect_from_LMF + "&callback=" + data.callback_from_LMF, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else if(data.redirect_from_LMF != null) {
            try {
                callbackService = "auth/oauth2/tokenRequest/"+
                        data.oauth2server+"/"+
                        data.mime_type+"/"+
                        URLEncoder.encode("redirect_uri="+data.redirect_from_LMF, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            callbackService = "auth/oauth2/tokenRequest/"+
                    data.oauth2server+"/"+
                    data.mime_type;
        }
        return callbackService;
    }

    /**
     * getOAuth2codeRequestURL() collects the parameters for requesting an authorization
     * code and constructs a code request url as a string.
     * @param data
     * @return a code request url as string
     */
    private String getOAuth2codeRequestURL(OAuth2Data data) {
        List<String> scopes = configurationService.getListConfiguration(data.oauth2server+".scope");
        StringBuilder scopeSB = new StringBuilder();
        if(scopes.size() >= 1) {
            for(int i=0; i<scopes.size()-1; i++) {
                scopeSB.append(scopes.get(i));
                scopeSB.append(" ");
            }
            scopeSB.append(scopes.get(scopes.size()-1));
        }
        data.scope = scopeSB.toString();

        data.callback_to_LMF = constructCallbackService(data);
        log.info("callbackService: " + data.callback_to_LMF);

        String propertyKey = getLookupKey(data);
        log.info("propertyKey: " + propertyKey);
        data.client_id = getClientId(data.oauth2server, propertyKey);
        log.info(" returns client id: " + data.client_id);

        if(data.client_id == null) {
            log.error("No client id configured for callback {} (hash: {}) on oauth 2.0 server " + data.oauth2server,
                    data.callback_to_LMF, propertyKey);
            return null;
        }

        String response_type = null;
        if((response_type = configurationService.getStringConfiguration(data.oauth2server+".response_type")) != null) {
            data.response_type = response_type;
        }

        String codeReqServer = configurationService.getStringConfiguration(data.oauth2server+".oauth_code_req");

        assert codeReqServer != null;
        assert data.callback_to_LMF != null;
        assert data.client_id != null;
        assert data.scope != null;
        assert data.response_type != null;

        return buildOAuth2codeRequestURL(codeReqServer, data);
    }

    /**
     * buildOAuth2codeRequestURL() constructs a code request url as a string.
     * @param codeReqServer the code request server (e.g. facebook or youtube)
     * @param data the oauth2 request data object containing the relevant
     *        data for the auth_code and access token request
     * @return a code request url as string
     */
    private String buildOAuth2codeRequestURL(String codeReqServer, OAuth2Data data) {
        try {

            if(codeReqServer == null)
                return null;
            if(data.callback_to_LMF == null)
                return null;
            if(data.client_id == null)
                return null;

            String hostname = KiWiUrlUtils.getHost(configurationService.getServerUri());

            StringBuilder sb = new StringBuilder(codeReqServer);
            sb.append("?client_id=");
            sb.append(URLEncoder.encode(data.client_id, "UTF-8"));
            sb.append("&redirect_uri=");
            sb.append(URLEncoder.encode(hostname + data.callback_to_LMF, "UTF-8"));
            sb.append("&scope=");
            sb.append(URLEncoder.encode(data.scope, "UTF-8"));
            sb.append("&response_type=");
            sb.append(URLEncoder.encode(data.response_type, "UTF-8"));

            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * buildOAuth2tokenRequest() constructs a token request url as a string.
     * @return a token request url as string
     */
    private String buildOAuth2tokenRequest(OAuth2Data data) {

        try {

            if(data.auth_code == null || data.client_id == null ||
                    data.client_secret == null || data.callback_to_LMF == null)
                return null;

            String hostname = KiWiUrlUtils.getHost(configurationService.getServerUri());

            if(data.grant_type == null) {
                data.grant_type = "";
            }

            StringBuilder sb = new StringBuilder("code=");
            sb.append(URLEncoder.encode(data.auth_code, "UTF-8"));
            sb.append("&client_id=");
            sb.append(URLEncoder.encode(data.client_id, "UTF-8"));
            sb.append("&client_secret=");
            sb.append(URLEncoder.encode(data.client_secret, "UTF-8"));
            sb.append("&redirect_uri=");
            sb.append(URLEncoder.encode(hostname + data.callback_to_LMF, "UTF-8"));
            sb.append("&grant_type=");
            sb.append(URLEncoder.encode(data.grant_type, "UTF-8"));

            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static class OAuth2Data {
        // data needed to request the auth code and the access token
        String oauth2server;
        String redirect_from_LMF;
        String callback_from_LMF;
        String mime_type;
        // data that is send to the oauth2 server to request the token
        String auth_code;
        String client_id;
        String client_secret;
        String scope;
        String response_type = "code";
        String grant_type = "authorization_code";
        // The difference between callback_from_LMF and callback_to_LMF is that
        // the callback_from_LMF will be executed by LMF.
        // In other words LMF will call the specified callback if one is defined
        // (and redirect to the redirect_from_LMF url if one is defined),
        // whereas callback_to_LMF is executed by the remote oauth2 service
        // provider and will result in a call of an LMF webservice.
        String callback_to_LMF;
        // access token data that is returned from the
        // request to the oauth2server with the
        // defined configurations
        String access_token;
        String refresh_token;
        String expires_in = "3600";
        // access token data that is produced by LMF and
        // returned to the requester
        String lmf_access_token;
        String lmf_refresh_token;
        String lmf_expires_in;

        private OAuth2Data(String oauth2server, String mime_type, String redirect_from_LMF, String callback_from_LMF) {
            this.oauth2server = oauth2server;
            this.mime_type = mime_type;
            this.redirect_from_LMF = redirect_from_LMF;
            this.callback_from_LMF = callback_from_LMF;
        }
    }
}


