/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.services;

import at.newmedialab.lmf.oauth.model.AccessToken;
import at.newmedialab.lmf.oauth.model.RefreshToken;

import at.newmedialab.lmf.oauth.exception.AccessTokenExpiredException;
import at.newmedialab.lmf.oauth.exception.AccessTokenNotValidException;

import at.newmedialab.lmf.oauth.api.TokenManagerService;

import kiwi.core.api.config.ConfigurationService;
import kiwi.core.model.rdf.KiWiResource;
import kiwi.core.model.user.KiWiUser;
import org.apache.amber.oauth2.as.issuer.MD5Generator;
import org.apache.amber.oauth2.as.issuer.OAuthIssuer;
import org.apache.amber.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.amber.oauth2.common.exception.OAuthSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.HashMap;

/**
 * User: Stephanie Stroka
 * Date: 18.02.2011
 * Time: 10:18:00
 */
@Named("kiwi.core.tokenManagerService")
@ApplicationScoped
public class TokenManagerServiceImpl implements TokenManagerService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    /* <accessToken_string, accessToken_entity> */
    //private HashMap<String, AccessToken> accessTokens;
    private HashMap<String, TokenBundle> tokens;

    /* <refreshToken_string, refreshToken_entity> */
    private HashMap<String, RefreshToken> refreshTokens;

    @Inject
    private ConfigurationService configurationService;

    @PostConstruct
    private void init() {
        tokens = new HashMap<String, TokenBundle>();
        refreshTokens = new HashMap<String, RefreshToken>();
    }

    private Date expiration_int2date(int expires_in) {
        long currentMs = System.currentTimeMillis();
        long expirationMs = currentMs + (expires_in * 1000);
        return new Date(expirationMs);
    }

    /**
     * relates the accessToken with its expiration time, its refreshToken and the user
     * @param user
     * @param accessToken
     * @param refreshToken
     * @param expiration
     * @return
     */
    @Override
    public TokenBundle setToken(
            KiWiUser user, String accessToken, String refreshToken, int expiration)
            throws AccessTokenNotValidException {

        if(expiration < 0) {
            // evil!!!
            return null;
        }

        Date expirationDate = expiration_int2date(expiration);

        if(tokens.containsKey(accessToken)) {
            if(tokens.get(accessToken).getAccessToken() == null) {
                throw new AccessTokenNotValidException("Token bundle for access token found, but corrupt.");
            }
            if(tokens.get(accessToken).getAccessToken().getKiwiUser() != user) {
                log.error("AccessToken is already in use by another user.");
                throw new AccessTokenNotValidException("AccessToken is already in use by another user.");
            } else {
                log.warn("AccessToken already exists for current user. Ignoring token request.");
                return tokens.get(accessToken);
            }
        } else {
            log.info("Added access token: {}", accessToken);
        }

        AccessToken accessTokenEntity = new AccessToken(accessToken, user, expirationDate);
        RefreshToken refreshTokenEntity = new RefreshToken(refreshToken, accessTokenEntity);

        TokenBundle tb = new TokenBundle(accessTokenEntity, refreshTokenEntity);

        tokens.put(accessToken, tb);
        refreshTokens.put(refreshToken, refreshTokenEntity);

        return tb;
    }

    /**
     * relates the accessToken with its expiration time, its refreshToken and the user
     * @param user
     * @param accessToken
     * @param tokenProvider
     * @param refreshToken
     * @param expiration
     * @return
     */
    @Override
    public TokenBundle setToken(
            KiWiUser user, String tokenProvider, String accessToken, String refreshToken, int expiration)
            throws AccessTokenNotValidException {

        TokenBundle tb = null;
        AccessToken lmfAccessTokenEntity;

        if(tokenProvider != null && !tokenProvider.equals("lmf")) {
            // TODO: add refreshToken/expiration date to the third-party access token
            lmfAccessTokenEntity  = getAccessTokenEntity(user.getDelegate());

            if(lmfAccessTokenEntity == null) {
                OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
                try {
                    String lmfAccessToken = oauthIssuerImpl.accessToken();
                    String lmfRefreshToken = oauthIssuerImpl.refreshToken();
                    int lmfExpiration = getDefaultExpiration();

                    tb = setToken(user, lmfAccessToken, lmfRefreshToken, lmfExpiration);
                    log.info("Created new lmf access token.");

                } catch (OAuthSystemException e) {
                    e.printStackTrace();
                    throw new AccessTokenNotValidException("OAuth 2.0 Issuer problem.");
                }
            } else {
                // same user was already identified, so let's just return the existing token bundle
                tb = tokens.get(lmfAccessTokenEntity.getAccessToken());
            }
            tb.getAccessToken().addThirdPartyToken(tokenProvider, accessToken, refreshToken, expiration_int2date(expiration));
            return tb;
        } else {
            return setToken(user, accessToken, refreshToken, expiration);
        }
    }

    private int getDefaultExpiration() {
        int exp;
        if(configurationService.getConfiguration("session_expiration") == null) {
            exp = 3600;
            configurationService.setConfiguration("session_expiration", String.valueOf(3600));
        } else {
            String expStr = (String) configurationService.getConfiguration("session_expiration");
            log.debug("expStr: {}", expStr);

            exp = Integer.parseInt(expStr);
        }
        return exp;
    }

    @Override
    public String getAccessToken(KiWiResource userResource, String provider) {
        AccessToken lmfAccessTokenEntity = getAccessTokenEntity(userResource);
        if(provider != null && !provider.equals("lmf")) {
            log.info("getAccessToken for provider {}", provider);
            AccessToken thirdPartyToken =
                    lmfAccessTokenEntity != null ? lmfAccessTokenEntity.getThirdPartyAccessTokens().get(provider) : null;
            if(thirdPartyToken != null) {
                return thirdPartyToken.getAccessToken();
            } else {
                return null;
            }
        } else {
            return getAccessToken(userResource);
        }
    }

    @Override
    public String getAccessToken(KiWiResource userResource) {
        AccessToken accessToken;
        return (accessToken = getAccessTokenEntity(userResource)) != null ? accessToken.getAccessToken() : null;
    }

    private AccessToken getAccessTokenEntity(KiWiResource userResource) {

        for(String s : tokens.keySet()) {

            if(tokens.get(s).getAccessToken() == null) {
                throw new RuntimeException("tokenBundles are messed up.");
            }
            AccessToken accessToken = tokens.get(s).getAccessToken();
            if(accessToken.getKiwiUser().getDelegate().equals(userResource)) {
                log.info("Found access token {}", s);
                return accessToken;
            } else {

            }
        }
        log.warn("No token found for resource {}.", userResource);

        return null;
    }

    @Override
    public RefreshToken getRefreshToken(String refreshToken) {
        return refreshTokens.get(refreshToken);
    }

    @Override
    public boolean removeAccessToken(String accessToken) {
        return tokens.remove(accessToken) == null;
    }

    @Override
    public boolean removeAccessToken(String accessToken, String tokenProvider) {
        if(tokenProvider == null || tokenProvider.equals("lmf"))
            return removeAccessToken(accessToken);
        else {
            AccessToken accessTokenEntity = tokens.get(accessToken).getAccessToken();
            accessTokenEntity.removeThirdPartyToken(tokenProvider);
            // TODO: return false if it could not be removed
            return true;
        }
    }

    // TODO: implement for third-party auth servers
    @Override
    public boolean removeRefreshToken(String refreshToken) {
        return refreshTokens.remove(refreshToken) == null ? true : false;
    }

    @Override
    public KiWiUser validateAccessToken(String accessToken) throws AccessTokenNotValidException {
        if(!tokens.containsKey(accessToken)) {
            throw new AccessTokenNotValidException("The access token does not exist in the token manager.");
        }
        AccessToken accessTokenEntity = tokens.get(accessToken).getAccessToken();
        Date expirationDate = accessTokenEntity.getExpirationDate();
        long currentMs = System.currentTimeMillis();

        if(expirationDate.getTime() < currentMs) {
            throw new AccessTokenExpiredException("The access token expired.");
        } else {
            return accessTokenEntity.getKiwiUser();
        }
    }

    /**
     * Bundle access and refresh token so
     * that it can be returned as single parameter
     * when setToken() is called.
     */
    public static class TokenBundle {
        
        private AccessToken accessToken;
        private RefreshToken refreshToken;

        private TokenBundle(AccessToken accessToken, RefreshToken refreshToken) {
            this.accessToken = accessToken;
            this.refreshToken = refreshToken;
        }

        public AccessToken getAccessToken() {
            return accessToken;
        }

        private void setAccessToken(AccessToken accessToken) {
            this.accessToken = accessToken;
        }

        public RefreshToken getRefreshToken() {
            return refreshToken;
        }

        private void setRefreshToken(RefreshToken refreshToken) {
            this.refreshToken = refreshToken;
        }
    }
}

