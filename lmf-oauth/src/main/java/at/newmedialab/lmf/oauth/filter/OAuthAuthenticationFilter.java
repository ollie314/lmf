/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.filter;

import at.newmedialab.lmf.oauth.exception.AccessTokenExpiredException;
import at.newmedialab.lmf.oauth.exception.AccessTokenNotValidException;

import at.newmedialab.lmf.oauth.api.CookieService;
import at.newmedialab.lmf.oauth.api.TokenManagerService;

import kiwi.core.api.modules.LMFHttpFilter;
import kiwi.core.api.user.UserService;
import kiwi.core.model.rdf.KiWiUriResource;
import kiwi.core.model.user.KiWiUser;
import org.apache.amber.oauth2.common.exception.OAuthProblemException;
import org.apache.amber.oauth2.common.exception.OAuthSystemException;
import org.apache.amber.oauth2.common.message.types.ParameterStyle;
import org.apache.amber.oauth2.rs.request.OAuthAccessResourceRequest;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * @author Stephanie Stroka
 *         User: sstroka
 *         Date: 17.10.2011
 *         Time: 10:10:33
 */
@ApplicationScoped
public class OAuthAuthenticationFilter implements LMFHttpFilter {

    @Inject
    private Logger log;

    @Inject
    private UserService userService;

    @Inject
    private TokenManagerService tokenManagerService;

    @Inject
    private CookieService cookieService;

    @Override
    public String getPattern() {
        return "^/.*";
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }


    /**
     * Return the priority of the filter. Filters that need to be executed before anything else should return
     * PRIO_FIRST, filters that need to be executed last in the chain should return PRIO_LAST, all other filters
     * something inbetween (e.g. PRIO_MIDDLE).
     *
     * @return
     */
    @Override
    public int getPriority() {
        return PRIO_FIRST;
    }

    @Override
    public void doFilter(ServletRequest _request, ServletResponse _response, FilterChain filterChain) 
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) _request;
        HttpServletResponse response = (HttpServletResponse) _response;

        KiWiUser user = null;

        ////////////////////////////////////////////////////////////////////////////////////
        //////////////////////// Authentication via SessionToken  //////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        if(request.getParameter("oauth_token") != null) {
            // what to do if we receive a token?!
            try {
                OAuthAccessResourceRequest oauthRequest = new OAuthAccessResourceRequest(request, ParameterStyle.QUERY);
                String token = oauthRequest.getAccessToken();

                log.debug("AccessToken received: " + token);
                try {
                    user = tokenManagerService.validateAccessToken(token);
                } catch(AccessTokenExpiredException e) {
                    log.error("The provided access token expired.");
                    e.printStackTrace();
                } catch (AccessTokenNotValidException e) {
                    log.error("The provided access token expired.");
                    e.printStackTrace();
                }

                if(user == null) {
                    log.error("AccessToken " + token + " found, but invalid");
                }
            } catch (OAuthSystemException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (OAuthProblemException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else if(request.getParameter("refresh_token") != null) {
            
        } else if(request.getCookies() != null) {
            String token = cookieService.getCookieValue(request, "oauth_token");

            if(token != null) {
                try {
                    log.debug("AccessToken received: " + token);
                    user = tokenManagerService.validateAccessToken(token);
                } catch(AccessTokenExpiredException e) {
                    Cookie c = cookieService.removeCookie(cookieService.getCookie(request, "oauth_token"));
                    if(c != null)
                        response.addCookie(c);
                    log.error("The provided access token expired.");
                } catch (AccessTokenNotValidException e) {
                    Cookie c = cookieService.removeCookie(cookieService.getCookie(request, "oauth_token"));
                    if(c != null)
                        response.addCookie(c);
                    log.error("The provided access token expired.");
                }
                if(user == null) {
                    log.error("AccessToken " + token + " found, but invalid");
                }
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////
        //////////////////////// Authentication via Client Cert ////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
		else if(request.getHeader("Authorization") != null) {
            String credentials = request.getHeader("Authorization");
            String[] authSet = credentials.split(" ");

            if(authSet[0] != null && authSet[0].equals("ClientCertificate")) {
                // TODO: someday :)
            }
        }

        if(user != null) {
            userService.setCurrentUser((KiWiUriResource)user.getDelegate());
        }

        response.setStatus(HttpServletResponse.SC_OK);
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
