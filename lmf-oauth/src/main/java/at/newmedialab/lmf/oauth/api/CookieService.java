/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.api;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.NewCookie;

/**
 * @author Stephanie Stroka
 *         User: sstroka
 *         Date: 27.09.2011
 *         Time: 10:28:06
 */
public interface CookieService {

    public Cookie getCookie(HttpServletRequest request, String name);
    public String getCookieValue(HttpServletRequest request, String name);

    public NewCookie buildCookie(String name, String value, String str_expires);
    public NewCookie buildCookie(String name, String value, String str_expires, boolean secure);
    public NewCookie buildCookie(String name, String value, String path, String str_expires, boolean secure);
    public NewCookie buildCookie(String name, String value, String path, String domain,
                                 String version, String str_expires, boolean secure);

    public NewCookie buildCookie(String name, String value, int expires);
    public NewCookie buildCookie(String name, String value, int expires, boolean secure);
    public NewCookie buildCookie(String name, String value, String path, int expires, boolean secure);
    public NewCookie buildCookie(String name, String value, String path, String domain,
                                 String version, int expires, boolean secure);

    public Cookie removeCookie(Cookie cookie);
    public NewCookie removeCookie(NewCookie cookie);
}
