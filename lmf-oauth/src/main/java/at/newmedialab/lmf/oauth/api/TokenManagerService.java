/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.api;

import at.newmedialab.lmf.oauth.services.TokenManagerServiceImpl;

import at.newmedialab.lmf.oauth.model.RefreshToken;

import at.newmedialab.lmf.oauth.exception.AccessTokenNotValidException;

import kiwi.core.model.rdf.KiWiResource;
import kiwi.core.model.user.KiWiUser;

/**
 * This service checks oauth access tokens against expiration date and existing user
 * User: Stephanie Stroka
 * Date: 18.02.2011
 * Time: 10:09:18
 */
public interface TokenManagerService {

    /**
     *  maps access token and refresh token to user
     * @param user
     * @param token
     * @param refreshToken
     * @param expiration
     * @return true if the token is new
     */
    public TokenManagerServiceImpl.TokenBundle setToken(
            KiWiUser user, String token, String refreshToken, int expiration)
            throws AccessTokenNotValidException;
    public TokenManagerServiceImpl.TokenBundle setToken(
            KiWiUser user, String tokenProvider, String token, String refreshToken, int expiration)
            throws AccessTokenNotValidException;

    /**
     * returns the current token of the user.
     * The token may be used to send requests to
     * remote oauth server, e.g. facebook, google, etc.
     * @param userResource
     * @return the access token of the user
     */
    public String getAccessToken(KiWiResource userResource);
    public String getAccessToken(KiWiResource userResource, String provider);

    /**
     * getRefreshToken() returns a refreshTokenEntity if the refreshToken could be found
     * @param refreshToken
     * @return refreshTokenEntity
     */
    public RefreshToken getRefreshToken(String refreshToken);

    /**
     * removes an accessToken, e.g. if the token gets invalid
     * @param accessToken
     * @return true if token could be found and removed, false if token could not be found
     */
    public boolean removeAccessToken(String accessToken);
    public boolean removeAccessToken(String accessToken, String tokenProvider);

    /**
     * removes a refreshToken, e.g. if the user logs out
     * @param refreshToken
     * @return true if token could be found and removed, false if token could not be found
     */
    public boolean removeRefreshToken(String refreshToken);

    /**
     * validates if the accessToken is valid and not expired
     * @param accessToken
     * @return the KiWiUser, if the accessToken is valid and not expired
     */
    public KiWiUser validateAccessToken(String accessToken) throws AccessTokenNotValidException;
}
