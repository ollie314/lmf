/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * @author Stephanie Stroka
 *         User: sstroka
 *         Date: 29.11.2011
 *         Time: 13:50:51
 */
public class PasswordGenerator {

    private PasswordGenerator() {}

    static private String toASCII( byte[] bytes){

        byte[] newByteArray = new byte[bytes.length];
        for(int i=0; i<bytes.length; i++) {
            int ubyte = bytes[i] + Byte.MAX_VALUE;
            newByteArray[i] = (byte) (ubyte/3 + 41);
        }
        return new String(newByteArray);
    }

    public static String generatePassword() {
        try {
            String rand = null;
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");

            byte[] seed = sr.generateSeed(10);
            sr.setSeed(seed);

            byte[] bytes = new byte[128/8];
            sr.nextBytes(bytes);

            return toASCII(bytes);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
