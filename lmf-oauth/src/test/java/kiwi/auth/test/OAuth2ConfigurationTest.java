/**
 *  Copyright (c) 2012 Salzburg Research.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package kiwi.auth.test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import kiwi.core.model.Constants;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import static com.jayway.restassured.RestAssured.expect;
import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/**
 * This test covers the checking for available and valid configuration,
 * e.g. the client id and client secret for a given oauth2 server.
 *
 * @author Stephanie Stroka
 *         User: sstroka
 *         Date: 23.11.2011
 *         Time: 13:55:02
 */
public class OAuth2ConfigurationTest {

    String configKeysGenertic[] = {"oauth2.defaultExpiration"};

    String fb_client_id = "208720099202094";
    String yt_client_id1 = "873065855370.apps.googleusercontent.com";
    String yt_client_id2 = "873065855370-b6jr6gbv6ld4kmagv6dv3c38r1qp80uo.apps.googleusercontent.com";
    String fb_client_secret = "434bd769aec210f18575f16a034fa7b7";
    String yt_client_secret1 = "cMpG90DUcBDKh0I2cW7jlPWs";
    String yt_client_secret2 = "iOYF3le6oBWVcNyoSJISDonS";

    String configKeysFB[] = {"facebook.oauth_code_req", "facebook.access_token_uri", "facebook.client_id",
                "facebook.client_secret", "facebook.account", "facebook.scope", "facebook.serviceHomepage",
                "facebook.APIurl", "facebook.account"};
    
    String configKeysYT[] = {"youtube.oauth_code_req", "youtube.access_token_uri", "youtube.client_id",
                "youtube.client_secret", "youtube.account", "youtube.scope", "youtube.serviceHomepage",
                "youtube.APIurl", "youtube.account"};


    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port=8080;
        RestAssured.basePath="/LMF";
    }



    /**
     * The test checks whether the configurations have been set
     */
    @Test
    public void testConfigurationAvailable() {

        for(int i=0; i<configKeysGenertic.length; i++) {
            expect().
                statusCode(200).
            when().
                get("/config/data/" + configKeysGenertic[i]);
        }

        for(int i=0; i<configKeysFB.length; i++) {
            expect().
                statusCode(200).
            when().
                get("/config/data/" + configKeysFB[i]);
        }

        for(int i=0; i<configKeysYT.length; i++) {
            expect().
                statusCode(200).
            when().
                get("/config/data/" + configKeysYT[i]);
        }
    }
    
    /**
     * The test checks some values of the facebook oauth2.0 configurations.
     */
    @Test
    public void testFBConfigurationValues() {

        expect().
            statusCode(200).body("'facebook.oauth_code_req'", equalTo("https://www.facebook.com/dialog/oauth")).
        when().
            get("/config/data/facebook.oauth_code_req");

        expect().
            statusCode(200).body("'facebook.access_token_uri'", equalTo("https://graph.facebook.com/oauth/access_token")).
        when().
            get("/config/data/facebook.access_token_uri");

        expect().
            statusCode(200).body("'facebook.account'", equalTo("https://www.facebook.com/")).
        when().
            get("/config/data/facebook.account");

        String scope_str = get("/config/data/facebook.scope").asString();

        JsonPath scope = new JsonPath(scope_str).setRoot("'facebook.scope'");
        List<String> scope_values = scope.get();
        assertThat( scope_values.contains("user_about_me"), is(true));
        assertThat( scope_values.contains("user_likes"), is(true));
        assertThat( scope_values.contains("user_videos"), is(true));
        assertThat( scope_values.contains("user_birthday"), is(true));


        expect().
            statusCode(200).body("'facebook.serviceHomepage'", equalTo("https://www.facebook.com/")).
        when().
            get("/config/data/facebook.serviceHomepage");

        expect().
            statusCode(200).body("'facebook.APIurl'", equalTo("https://graph.facebook.com/me?access_token=")).
        when().
            get("/config/data/facebook.APIurl");

        String client_id_str = get("/config/data/facebook.client_id").asString();

        JsonPath client_ids = new JsonPath(client_id_str).setRoot("'facebook.client_id'");
        List<String> client_id_pairs = client_ids.get();
        
        assertThat( client_id_pairs.contains("603849904=" + fb_client_id), is(true));
        assertThat( client_id_pairs.contains("3298863372=" + fb_client_id), is(true));

        String client_secret_str = get("/config/data/facebook.client_secret").asString();

        JsonPath client_secrets = new JsonPath(client_secret_str).setRoot("'facebook.client_secret'");
        List<String> client_secrets_pairs = client_secrets.get();
        assertThat( client_secrets_pairs.contains("603849904=" + fb_client_secret), is(true));
        assertThat( client_secrets_pairs.contains("3298863372=" + fb_client_secret), is(true));
    }

    /**
     * The test checks some values of the youtube oauth2.0 configurations.
     */
    @Test
    public void testYTConfigurationValues() {


        expect().
            statusCode(200).body("'youtube.oauth_code_req'", equalTo("https://accounts.google.com/o/oauth2/auth")).
        when().
            get("/config/data/youtube.oauth_code_req");

        expect().
            statusCode(200).body("'youtube.access_token_uri'", equalTo("https://accounts.google.com/o/oauth2/token")).
        when().
            get("/config/data/youtube.access_token_uri");

        expect().
            statusCode(200).body("'youtube.account'", equalTo("https://www.youtube.com/profile?user=")).
        when().
            get("/config/data/youtube.account");

        expect().
            statusCode(200).body("'youtube.response_type'", equalTo("code")).
        when().
            get("/config/data/youtube.response_type");

        expect().
            statusCode(200).body("'youtube.scope'", equalTo("http://gdata.youtube.com")).
        when().
            get("/config/data/youtube.scope");

        expect().
            statusCode(200).body("'youtube.serviceHomepage'", equalTo("https://www.youtube.com/")).
        when().
            get("/config/data/youtube.serviceHomepage");

        expect().
            statusCode(200).body("'youtube.APIurl'", equalTo("https://gdata.youtube.com/feeds/api/users/default?oauth_token=")).
        when().
            get("/config/data/youtube.APIurl");

        String client_id_str = get("/config/data/youtube.client_id").asString();

        JsonPath client_ids = new JsonPath(client_id_str).setRoot("'youtube.client_id'");
        List<String> client_id_pairs = client_ids.get();
        assertThat( client_id_pairs.contains("603849904="+yt_client_id1), is(true));
        assertThat( client_id_pairs.contains("3298863372="+yt_client_id2), is(true));

        String client_secret_str = get("/config/data/youtube.client_secret").asString();

        JsonPath client_secrets = new JsonPath(client_secret_str).setRoot("'youtube.client_secret'");
        List<String> client_secrets_pairs = client_secrets.get();
        assertThat( client_secrets_pairs.contains("603849904="+yt_client_secret1), is(true));
        assertThat( client_secrets_pairs.contains("3298863372="+yt_client_secret2), is(true));
    }

    /**
     * The test checks whether the youtube oauth2.0
     * login returns the correct Responses
     */
    @Test
    public void testYTlogin() {

        expect().
            statusCode(200).
        when().
            get("/auth/oauth2/youtube?mime_type="+ Constants.MIME_TYPE_XHTML);
    }

    /**
     * The test checks whether the youtube oauth2.0
     * login returns the correct Responses
     */
    @Test
    public void testYTloginWithRedirect() {

        try {
            expect().
                statusCode(200).
            when().
                get("/auth/oauth2/youtube" +
                        "?mime_type="+ Constants.MIME_TYPE_XHTML +
                        "&redirect_uri=" + URLEncoder.encode("http://localhost:8080/LMF/social/", "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            // fail...
            assertThat("1", equals("2"));
        }
    }

    /**
     * The test checks whether the youtube oauth2.0
     * login returns the correct Responses
     */
    @Test
    public void testYTtokenRequest() {

        String code_param = "code=lmf-test123";
        String client_id_param = "&client_id="+yt_client_id1;
        String client_secret_param = "&client_secret="+yt_client_secret1;
        String redirect_uri_param = "&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2FLMF%2Fauth%2Foauth2%2FtokenRequest%2Fyoutube%2Fapplication%2Fxhtml%2Bxml";
        String grand_type_param = "&grant_type=authorization_code";

        StringBuilder tokenReqBuilder = new StringBuilder();
        tokenReqBuilder.append(code_param);
        tokenReqBuilder.append(client_id_param);
        tokenReqBuilder.append(client_secret_param);
        tokenReqBuilder.append(redirect_uri_param);
        tokenReqBuilder.append(grand_type_param);

        String body = "<?xml version=\"1.0\"?> \n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"> \n" +
                "<title>\n" +
                "LMF - Error \n" +
                "<style type=\"text/css\" media=\"screen\">@import \"http://localhost:8080/LMF/css/basic.css\";</style> \n" +
                "\t<style type=\"text/css\" media=\"screen\">@import \"http://localhost:8080/LMF/css/tabs.css\";</style> \n" +
                "</title> \n" +
                "<body> \n" +
                "<h1> \n" +
                "LMF - Linked Media Framework \n" +
                "</h1> \n" +
                "<h2> \n" +
                "An error happened\n" +
                "</h2> \n" +
                "<div id=\"main\"> \n" +
                "<div id=\"contents\">  \n" +
                "Could not authenticate with server: youtube<br/>Reason: LMF failed due to an I/O processing error: This is just a test: "+tokenReqBuilder.toString()+"</div> \n" +
                "</div> \n" +
                "</body> \n" +
                "</html> \n";

        String error = get("/auth/oauth2/tokenRequest/youtube/"+Constants.MIME_TYPE_XHTML+"?code=lmf-test123").asString();

        assertThat( body.equals(error), is(true));
    }

    /**
     * The test checks whether the youtube oauth2.0
     * login returns the correct Responses
     */
    @Test
    public void testYTtokenRequestWithRedirect() {

        String code_param = "code=lmf-test123";
        String client_id_param = "&client_id="+yt_client_id2;
        String client_secret_param = "&client_secret="+yt_client_secret2;
        String redirect_uri_param = "&redirect_uri=" +
                "http%3A%2F%2Flocalhost%3A8080%2FLMF%2Fauth%2Foauth2%2FtokenRequest" +
                "%2Fyoutube" +
                "%2Fapplication%2Fxhtml%2Bxml" +
                "%2Fredirect_uri%253Dhttp%253A%252F%252Flocalhost%253A8080%252FLMF%252Fsocial%252F";
        String grand_type_param = "&grant_type=authorization_code";

        StringBuilder tokenReqBuilder = new StringBuilder();
        tokenReqBuilder.append(code_param);
        tokenReqBuilder.append(client_id_param);
        tokenReqBuilder.append(client_secret_param);
        tokenReqBuilder.append(redirect_uri_param);
        tokenReqBuilder.append(grand_type_param);

        String body = "<?xml version=\"1.0\"?> \n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"> \n" +
                "<title>\n" +
                "LMF - Error \n" +
                "<style type=\"text/css\" media=\"screen\">@import \"http://localhost:8080/LMF/css/basic.css\";</style> \n" +
                "\t<style type=\"text/css\" media=\"screen\">@import \"http://localhost:8080/LMF/css/tabs.css\";</style> \n" +
                "</title> \n" +
                "<body> \n" +
                "<h1> \n" +
                "LMF - Linked Media Framework \n" +
                "</h1> \n" +
                "<h2> \n" +
                "An error happened\n" +
                "</h2> \n" +
                "<div id=\"main\"> \n" +
                "<div id=\"contents\">  \n" +
                "Could not authenticate with server: youtube<br/>Reason: LMF failed due to an I/O processing error: This is just a test: "+tokenReqBuilder.toString()+"</div> \n" +
                "</div> \n" +
                "</body> \n" +
                "</html> \n";

        String error = get("/auth/oauth2/tokenRequest/youtube/"+Constants.MIME_TYPE_XHTML
                +"/redirect_uri%3Dhttp%3A%2F%2Flocalhost%3A8080%2FLMF%2Fsocial%2F"
                +"?code=lmf-test123").asString();

        assertThat( body.equals(error), is(true));
    }

    /**
     * The test checks whether the facebook oauth2.0
     * login returns the correct Responses
     */
    @Test
    public void testFBlogin() {

        expect().
            statusCode(200).
        when().
            get("/auth/oauth2/facebook?mime_type="+ Constants.MIME_TYPE_XHTML);
    }

    /**
     * The test checks whether the facebook oauth2.0
     * login returns the correct Responses
     */
    @Test
    public void testFBloginWithRedirect() {

        try {
            expect().
                statusCode(200).
            when().
                get("/auth/oauth2/facebook" +
                        "?mime_type="+ Constants.MIME_TYPE_XHTML +
                        "&redirect_uri=" + URLEncoder.encode("http://localhost:8080/LMF/social/", "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            // fail...
            assertThat("1", equals("2"));
        }
    }

    /**
     * The test checks whether the dacebook oauth2.0
     * login returns the correct Responses
     */
    @Test
    public void testFBtokenRequest() {

        String code_param = "code=lmf-test123";
        String client_id_param = "&client_id=" + fb_client_id;
        String client_secret_param = "&client_secret=" + fb_client_secret;
        String redirect_uri_param = "&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2FLMF%2Fauth%2Foauth2%2FtokenRequest%2Ffacebook%2Fapplication%2Fxhtml%2Bxml";
        String grand_type_param = "&grant_type=authorization_code";

        StringBuilder tokenReqBuilder = new StringBuilder();
        tokenReqBuilder.append(code_param);
        tokenReqBuilder.append(client_id_param);
        tokenReqBuilder.append(client_secret_param);
        tokenReqBuilder.append(redirect_uri_param);
        tokenReqBuilder.append(grand_type_param);

        String body = "<?xml version=\"1.0\"?> \n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"> \n" +
                "<title>\n" +
                "LMF - Error \n" +
                "<style type=\"text/css\" media=\"screen\">@import \"http://localhost:8080/LMF/css/basic.css\";</style> \n" +
                "\t<style type=\"text/css\" media=\"screen\">@import \"http://localhost:8080/LMF/css/tabs.css\";</style> \n" +
                "</title> \n" +
                "<body> \n" +
                "<h1> \n" +
                "LMF - Linked Media Framework \n" +
                "</h1> \n" +
                "<h2> \n" +
                "An error happened\n" +
                "</h2> \n" +
                "<div id=\"main\"> \n" +
                "<div id=\"contents\">  \n" +
                "Could not authenticate with server: facebook<br/>Reason: LMF failed due to an I/O processing error: This is just a test: "+tokenReqBuilder.toString()+"</div> \n" +
                "</div> \n" +
                "</body> \n" +
                "</html> \n";

        String error = get("/auth/oauth2/tokenRequest/facebook/"+Constants.MIME_TYPE_XHTML+"?code=lmf-test123").asString();

        assertThat( body.equals(error), is(true));
    }

    /**
     * The test checks whether the youtube oauth2.0
     * login returns the correct Responses
     */
    @Test
    public void testFBtokenRequestWithRedirect() {

        String code_param = "code=lmf-test123";
        String client_id_param = "&client_id="+fb_client_id;
        String client_secret_param = "&client_secret="+fb_client_secret;
        String redirect_uri_param = "&redirect_uri=" +
                "http%3A%2F%2Flocalhost%3A8080%2FLMF%2Fauth%2Foauth2%2FtokenRequest" +
                "%2Ffacebook" +
                "%2Fapplication%2Fxhtml%2Bxml" +
                "%2Fredirect_uri%253Dhttp%253A%252F%252Flocalhost%253A8080%252FLMF%252Fsocial%252F";
        String grand_type_param = "&grant_type=authorization_code";

        StringBuilder tokenReqBuilder = new StringBuilder();
        tokenReqBuilder.append(code_param);
        tokenReqBuilder.append(client_id_param);
        tokenReqBuilder.append(client_secret_param);
        tokenReqBuilder.append(redirect_uri_param);
        tokenReqBuilder.append(grand_type_param);

        String body = "<?xml version=\"1.0\"?> \n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"> \n" +
                "<title>\n" +
                "LMF - Error \n" +
                "<style type=\"text/css\" media=\"screen\">@import \"http://localhost:8080/LMF/css/basic.css\";</style> \n" +
                "\t<style type=\"text/css\" media=\"screen\">@import \"http://localhost:8080/LMF/css/tabs.css\";</style> \n" +
                "</title> \n" +
                "<body> \n" +
                "<h1> \n" +
                "LMF - Linked Media Framework \n" +
                "</h1> \n" +
                "<h2> \n" +
                "An error happened\n" +
                "</h2> \n" +
                "<div id=\"main\"> \n" +
                "<div id=\"contents\">  \n" +
                "Could not authenticate with server: facebook<br/>Reason: LMF failed due to an I/O processing error: This is just a test: "+tokenReqBuilder.toString()+"</div> \n" +
                "</div> \n" +
                "</body> \n" +
                "</html> \n";

        String error = get("/auth/oauth2/tokenRequest/facebook/"+Constants.MIME_TYPE_XHTML
                +"/redirect_uri%3Dhttp%3A%2F%2Flocalhost%3A8080%2FLMF%2Fsocial%2F"
                +"?code=lmf-test123").asString();

        assertThat( body.equals(error), is(true));
    }

}
