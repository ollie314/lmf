<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (c) 2013 Salzburg Research.
  ~
  ~  Licensed under the Apache License, Version 2.0 (the "License");
  ~  you may not use this file except in compliance with the License.
  ~  You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing, software
  ~  distributed under the License is distributed on an "AS IS" BASIS,
  ~  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~  See the License for the specific language governing permissions and
  ~  limitations under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>at.newmedialab.lmf</groupId>
        <artifactId>lmf-parent</artifactId>
        <version>3.1.0</version>
    </parent>

    <artifactId>lmf-social</artifactId>
    <packaging>jar</packaging>

    <name>LMF Module: Social Media Support</name>
    <description>
        EXPERIMENTAL. Provides access to various social media services like the generic RSS standard, or the specific APIs of Google, 
        YouTube and Vimeo to allow integration and interlinking with social media content.
    </description>

    <repositories>
        <repository>
            <id>google-api-services</id>
            <url>http://google-api-client-libraries.appspot.com/mavenrepo</url>
        </repository>
    </repositories>
  
    <dependencies>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-sparql</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-ldpath</artifactId>
        </dependency>
        <dependency>
            <groupId>at.newmedialab.lmf</groupId>
            <artifactId>lmf-scheduler</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-ldcache</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>ldclient-provider-mediawiki</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>ldclient-provider-phpbb</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>ldclient-provider-vimeo</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>ldclient-provider-youtube</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>ldclient-provider-facebook</artifactId>
        </dependency>
        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
        </dependency>        
        <dependency>
            <groupId>org.rometools</groupId>
            <artifactId>rome</artifactId>
        </dependency>
        <dependency>
            <groupId>org.rometools</groupId>
            <artifactId>rome-modules</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.api-client</groupId>
            <artifactId>google-api-client</artifactId>
            <version>1.13.2-beta</version>
			<exclusions>
				<exclusion>
					<groupId>com.google.guava</groupId>
					<artifactId>guava-jdk5</artifactId>
				</exclusion>
			</exclusions>
        </dependency>
        <dependency>
            <groupId>com.google.apis</groupId>
            <artifactId>google-api-services-oauth2</artifactId>
            <version>v2-rev28-1.13.2-beta</version>
        </dependency>        
        <dependency>
            <groupId>com.google.http-client</groupId>
            <artifactId>google-http-client-jackson</artifactId>
            <version>1.13.1-beta</version>
			<exclusions>
				<exclusion>
					<groupId>com.google.guava</groupId>
					<artifactId>guava-jdk5</artifactId>
				</exclusion>
			</exclusions>
        </dependency>
		<dependency>
		    <groupId>com.google.gdata</groupId>
		    <artifactId>core</artifactId>
		    <version>1.47.1</version>
            <exclusions>
                <exclusion>
                    <groupId>com.google.oauth-client</groupId>
                    <artifactId>google-oauth-client-jetty</artifactId>
                </exclusion>
            </exclusions>
		</dependency>    
        <dependency>
	        <groupId>com.google.code.gson</groupId>
	        <artifactId>gson</artifactId>
	        <version>2.2.2</version>
        </dependency>                    
        <dependency>
            <groupId>com.restfb</groupId>
            <artifactId>restfb</artifactId>
            <version>1.6.11</version>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpmime</artifactId>
        </dependency>

        <!-- Integration and Unit Testing -->
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-core</artifactId>
            <version>${marmotta.version}</version>
            <type>test-jar</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-server</artifactId>
            <version>${jetty.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-servlet</artifactId>
            <version>${jetty.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.jboss.weld.se</groupId>
            <artifactId>weld-se-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.jayway.restassured</groupId>
            <artifactId>rest-assured</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.google.code.tempus-fugit</groupId>
            <artifactId>tempus-fugit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.googlecode.jatl</groupId>
            <artifactId>jatl</artifactId>
            <scope>test</scope>
        </dependency>

    </dependencies>
    
</project>
