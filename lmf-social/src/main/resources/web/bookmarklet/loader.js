/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function() {
	window.lmfLoader = window.lmfLoader || {};
	var lmf_rndKey = window.lmfLoader.rndKey = window.lmfLoader.rndKey || Math.random().toString().substring(2);
	var scriptsLoaded = window.lmfLoader.scriptsLoaded = window.lmfLoader.scriptsLoaded || {};
	var cssLoaded = window.lmfLoader.csssLoaded = window.lmfLoader.csssLoaded || {};
	
	function countDown(callback, timeout, display, interval) {
		interval = interval || 100;
		if (!display) {
			setTimeout(callback, timeout);
		} else {
			if (timeout <= 0) {
				display(timeout);
				callback();
			} else {
				setTimeout( function() {countDown(callback, (timeout - interval), display, interval)}, interval);
				display(timeout);
			}
		}
	}
	
	// Load the script from url and when it's ready loading run the callback.
	function loadScript(url, callback) {
		// Avoid reloading already loaded scripts
		if (scriptsLoaded[url]) {
			callback();
			return;
		}
		var head = document.getElementsByTagName("head")[0];
		var script = document.createElement("script");
		script.src = url;

		// Attach handlers for all browsers
		var done = false;
		script.onload = script.onreadystatechange = function() {
			if (!done
					&& (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {
				done = true;

				scriptsLoaded[url] = true;
				// Continue your code
				callback();

				// Handle memory leak in IE
				script.onload = script.onreadystatechange = null;
				head.removeChild(script);
			}
		};

		head.appendChild(script);
	}

	// Load a list of scripts *one after the other* and run cb
	var loadScripts = function(scripts, cb) {
		var script, _i, _len, _results;
		if (scripts.length) {
			script = scripts.shift();
			loadScript(script, function() {
				loadScripts(scripts.slice(0), cb);
			});
		} else {
			// console.info("all scripts loaded.");
			if (cb)
				cb();
		}
	};

	var loadStyles = function(csss) {
		var css, _i, _len, _results;
		for (_i = 0, _len = csss.length; _i < _len; _i++) {
			css = csss[_i];
			// Avoid double-loading css-files.
			if (cssLoaded[css]) continue;

			var e = document.createElement('link');
			e.setAttribute('rel', 'stylesheet');
			e.setAttribute('href', css);
			document.head.appendChild(e);
			cssLoaded[css] = true;
		}
	};
	function noCache() {
		return Math.random().toString().substring(2);
	}
	var appRoot = window.bookmarkletConfig.appRoot;
	var target = window.bookmarkletConfig.targetURL;
	// Loading style definitions
	loadStyles([ appRoot + "jquery-ui-1.8.16.custom.css",
	             appRoot + "dialog.css" ]);

	// Loading the scripts
	loadScripts([ appRoot + "jquery-1.6.2.min.js",
	              appRoot + "jquery-ui-1.8.16.custom.min.js"
	              ], 
	              function() {
		var jQuery = window.lmfLoader.jQuery = window.lmfLoader.jQuery || window.jQuery.noConflict(true);
		var content = jQuery("#lmf_rssdlg_" + lmf_rndKey);
		if ( content.length < 1) {
			// Create the dlg-content
			content = jQuery("<div/>");
			content.attr("id", "lmf_rssdlg_" + lmf_rndKey);

			// Create the dialog
			var closeTimeout;
			var closeDlg = function() {
				content.dialog('close');
				jQuery('#countdown').text('');
			};
			var postRSS = function(inputs, callback, hasError) {
				hasError = hasError || false;
				var input, _i, _len, _results;
				if (inputs.length) {
					input = jQuery(inputs[0]);
					var label = input.siblings("label[for='" + input.attr('id') + "']");
					var icon = jQuery("span.ui-icon", label).removeClass("ui-icon-gear ui-icon-circle-check ui-icon-cancel");
					if (icon.length < 1) {
						icon = jQuery("<span>").appendTo(label);
					}
					icon.addClass("ui-icon ui-icon-gear");
					
					var feedUrl = input.val();
					if (feedUrl.search(/^https?:\/\//) == 0) {
						// absolut url
					} else if (feedUrl.charAt(0) === '/') {
						feedUrl = window.location.protocol + window.location.host + feedUrl;
					} else {
						feedUrl = window.location.protocol + window.location.host + window.location.pathname.substr(0, window.location.pathname.lastIndexOf('/') + 1) + feedUrl;
					}
					
					var error = true;
					jQuery.ajax( target.replace(/\{url\}/g,  encodeURIComponent(feedUrl))
									   .replace(/\{type\}/g, encodeURIComponent(input.attr('feed-type'))), {
						type: 'POST',
						success: function() {
							error = false;
							input.removeAttr("checked");
							input.attr('disabled', 'disabled');
							icon.removeClass("ui-icon-gear").addClass("ui-icon-circle-check");
						},
						error: function() {
							error = true;
							icon.removeClass("ui-icon-gear").addClass("ui-icon-cancel");
						},
						complete: function() {
							console.info("Posted " + input.val());
							postRSS(inputs.slice(1), callback, (hasError || error));
						}
					});
				} else {
					if (callback) {
						callback(!hasError);
					}
				}
				
			};

			var i=0;
			var cdSpan = jQuery("<span id='countdown' style='float: right; ' class='ui-state-error-text' />");
			jQuery("<div>").append(jQuery("<strong>").text("Available News-Feeds")).append(cdSpan).appendTo(content);
			jQuery("link[type='application/rss+xml'], link[type='application/atom+xml']").each(function() {
				var link = jQuery(this);
				var div = jQuery("<div>").appendTo(content);
				var r = jQuery("<input type='checkbox' name='feed' />").appendTo(div);
				r.attr('id', "link_" + (++i));
				r.attr('value', link.attr('href'));
				r.attr('title', link.attr('href'));
				r.attr('feed-type', link.attr('type'));
				
				var l = jQuery("<label>").attr('for', "link_" + i).text(link.attr('title') + " (" + link.attr('type').replace(/^[^\/]*\/(.*)\+xml$/, "$1") + ")").appendTo(div);
				l.attr('title', link.attr('href'));
			});
			function showCountdown(msLeft) {
				if (msLeft > 0) 
					cdSpan.text(' Closing in ' + Math.round(msLeft/1000) + 's'); 
				else cdSpan.text('');
			}
			
			var okDlg = function() {
				postRSS(jQuery("input:checked", jQuery(this)), function(allSuccess) {
					if (allSuccess) {
						countDown(closeDlg, 5000, showCountdown, 1000);
						//closeTimeout = setTimeout(closeDlg, 5000);
					}
				});
			};

			var buttons = {};
			if (i == 0) {
				jQuery("<div>").append(jQuery("<p>").text("No News-Feeds available")).appendTo(content);
			} else {
				buttons['OK'] = okDlg;
			}
			buttons['Close'] =  closeDlg;

			// Build the dialog
			content.dialog({
				title: "News Feeds",
				modal: true, 
				show: 'blind', 
				hide: 'blind', 
				position: ['center', 'top'], 
				buttons: buttons,
				open: function(event, ui) {
					// If there is no (more) content, auto-close the dialog after 10 sec.
					if (jQuery("input:enabled", content).length < 1) {
						// closeTimeout = setTimeout(closeDlg, 10000);
						countDown(closeDlg, 10000, showCountdown, 1000);
					}
				},
				close: function() {
					clearTimeout(closeTimeout);
				},
				autoOpen: false
			});
		} else {
		}
		content.dialog('open');
	});
})();