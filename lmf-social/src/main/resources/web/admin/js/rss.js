/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
$(function() {
	/* Welcome */
	var root = _SERVER_URL;
	var service = "social/rss";
	
	var loadRSS_TO;
	function loadRSS() {
		clearTimeout(loadRSS_TO);

		var table = $("table#rss-table");
		var tBody = $("tbody", table);
		$.getJSON(root + service, function(data) {
			$("tr", tBody).addClass("toRemove");
			$.each(data, function() {
				var row = $("tr[uri='" + this.uri + "']", tBody);
				if (row.size() == 0) {
					row = $("<tr />");
					row.attr("uri", this.uri);
					row.append($("<td/>").text(this.title));
					row.append($("<td/>").append("<a/>").attr('href', this.uri).text(this.uri));
					row.append($("<td/>").text(new Date(this.updated).toString()));
					row.append($("<td/>").text("TODO"));
					var action = $("<td/>").appendTo(row);
					action.append($("<a/>").attr("href", root + "resource/inspect?uri=" + encodeURIComponent(this.uri)).text("inspect"));
					
					row.appendTo(tBody);
				}
				row.removeClass("toRemove");
			});
			$("tr.toRemove", tBody).slideUp("slow", function() { $(this).remove(); });
		}).complete(function() {
			loadRSS_TO = setTimeout(loadRSS, 60000);
		});
	};
	loadRSS();
	
	function createBM() {
		var bookmarkletConfig = {appRoot: _SERVER_URL + 'social/bookmarklet/', targetURL: _SERVER_URL + "social/rss?url={url}&type={type}" };
		var link =
			"<a class='bookmarklet' href='" +
				"javascript:window.bookmarkletConfig=" + JSON.stringify(bookmarkletConfig) + ";" +
			'var e=document.createElement("script");' +
			'e.setAttribute("language","javascript");' +
			'e.setAttribute("type","text/javascript");' +
			'e.setAttribute("src","' + bookmarkletConfig.appRoot + 'loader.js?"+Math.random().toString().substring(2));' +
			'document.head.appendChild(e);' +
			'void(0);' +
			"'>" + "Index News Feed (LMF)" + "</a>"; 
		document.getElementById("rss-bookmarklet").innerHTML = link;
	};
	createBM();
	
});