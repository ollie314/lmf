/*
 * Copyright (c) 2012 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function( $ ){
    $.fn.social_import = function() {
        var settings = {
            host: 'http://localhost:8080/LMF/',
            redirectTo: 'http://localhost:8080/LMF/social/',
            mime_type: 'application/json',
            fbimport : 'social/facebook/import/user',
            ytimport : 'social/youtube/import/user'
        }

        /**
         * main method
         */
        return this.each(function() {

            var title = $("<div id='widgetTitle'> Import Data from Social Networks</div><hr/>");
            
            var div = $("<div id='importDataForm'></div>");

            var fbURL = settings.host + settings.fbimport;
            if(settings.redirectTo != '') {
                fbURL += '?redirect_uri=' + encodeURIComponent(settings.redirectTo);
            }

            var ytURL = settings.host + settings.ytimport;
            if(settings.redirectTo != '') {
                ytURL += '?redirect_uri=' + encodeURIComponent(settings.redirectTo);
            }

            var importLinkLabel = $("<p id=\"importLabel\">Import from</p>");
            var fbImportLink = $("<a href=\""+ fbURL +"\" > <img src=\"img/icons/facebook-icon.png\" alt=\"Facebook\" width=\"40px\" height=\"40px\"/></a>");

            var ytImportLink = $("<a href=\""+ ytURL +"\"> <img src=\"img/icons/youtube-icon.png\" alt=\"Youtube\" width=\"40px\" height=\"40px\"/></a>");

            //append elements
            $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'widgets/socialnetwork/hacksAndDemos/socialImportWidget.css') );

            $(this).html('');
            div.append(title);
            div.append(importLinkLabel);
            div.append(fbImportLink);
            div.append(ytImportLink);

            $(this).append(div);
        });
    };
})( jQuery );

