/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.services.rss.quartz;

import at.newmedialab.lmf.scheduler.api.DependencyInjectedJob;
import at.newmedialab.lmf.social.model.rss.RSSFeedFacade;
import at.newmedialab.lmf.social.model.rss.RSSUpdateMechanism;
import at.newmedialab.lmf.social.webservices.rss.RSSWebService;
import org.apache.marmotta.commons.sesame.facading.FacadingFactory;
import org.apache.marmotta.commons.sesame.repository.ResourceUtils;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;
import org.openrdf.repository.RepositoryConnection;
import org.quartz.DateBuilder;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;

public class CloudRegistrationJob implements DependencyInjectedJob {

    private static final int RETRY_COUNT = 5, RETRY_INTERVAL_MINUTES = 1, REFRESH_INTERVAL_MINUTES = 5;

    @Inject
    private Logger           log;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private SesameService sesameService;

    private String           feedResourceUri;
    private int              errorCount;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            RepositoryConnection conn = sesameService.getConnection();

            try {
                final Scheduler scheduler = context.getScheduler();
                final Trigger oldTrigger = context.getTrigger();
                final TriggerKey oldTriggerKey = oldTrigger.getKey();

                org.openrdf.model.URI rsc = ResourceUtils.getUriResource(conn, feedResourceUri);
                if (rsc == null) {
                    log.warn("could not find feedResource {}", feedResourceUri);
                    rescheduleAfterError(scheduler, oldTrigger);
                    return;
                }
                RSSFeedFacade feed = FacadingFactory.createFacading(conn).createFacade(rsc, RSSFeedFacade.class);
                if (feed == null) {
                    log.warn("could not load feedResource {}", feedResourceUri);
                    rescheduleAfterError(scheduler, oldTrigger);
                    return;
                }
                RSSUpdateMechanism up = feed.getUpdateMechanism();
                if (up == null) {
                    log.warn("no update mechanism for {} found", feedResourceUri);
                    rescheduleAfterError(scheduler, oldTrigger);
                    return;
                }
                String feedSource = feed.getFeedSource();
                if (feedSource == null) {
                    feedSource = feedResourceUri;
                }

                if (register4Notifications(up, feedSource)) {
                    if (errorCount != 0 || context.getNextFireTime() == null
                            || context.getFireTime().before(context.getNextFireTime())) {
                        // There was an error, so we now switch back to normal schedule
                        @SuppressWarnings("unchecked")
                        TriggerBuilder<Trigger> tb = (TriggerBuilder<Trigger>) oldTrigger.getTriggerBuilder();
                        tb.withIdentity(oldTriggerKey);
                        tb.usingJobData("errorCount", 0);
                        tb.withSchedule(SimpleScheduleBuilder.repeatMinutelyForever(REFRESH_INTERVAL_MINUTES));
                        tb.startAt(DateBuilder.futureDate(REFRESH_INTERVAL_MINUTES, IntervalUnit.MINUTE));

                        scheduler.rescheduleJob(oldTriggerKey, tb.build());
                        if (errorCount != 0) {
                            log.info("refreshed registration for feed {} after {} errors. resuming with normal schedule.",
                                    feedResourceUri, errorCount);
                        } else {
                            log.info("registered feed {} for updates.", feedResourceUri);
                        }

                    } else {
                        log.info("refreshed registration for feed {}", feedResourceUri);
                    }
                } else {
                    // There was an error refreshing the registration...
                    errorCount++;
                    if (errorCount > RETRY_COUNT) {
                        // ... and we tried really often, so we give up.
                        scheduler.unscheduleJob(oldTriggerKey);
                        log.warn("could not register for update notifications for {} (tried {} times, giving up...)",
                                feedResourceUri,
                                errorCount);
                    } else {
                        rescheduleAfterError(scheduler, oldTrigger);
                    }
                }
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (SchedulerException e) {
            log.error("could not modify schedule {}: {}", context.getTrigger().getKey().toString(), e.getMessage());
        } catch (RuntimeException e) {
            log.error("failure while execution JOB", e);
        } catch (Exception e) {
            log.error("failure while execution JOB", e);
        } catch (Error e) {
            log.error("failure while execution JOB", e);
        }
    }

    private void rescheduleAfterError(final Scheduler scheduler, final Trigger oldTrigger)
            throws SchedulerException {
        // ... we'll try again soon, maybe next time will work.
        final TriggerBuilder<? extends Trigger> tb = oldTrigger.getTriggerBuilder();
        tb.withIdentity(oldTrigger.getKey());
        tb.startAt(DateBuilder.futureDate(RETRY_INTERVAL_MINUTES, IntervalUnit.MINUTE));
        tb.usingJobData("errorCount", errorCount);

        Date nextTry = scheduler.rescheduleJob(oldTrigger.getKey(), tb.build());
        log.warn("could not register for update notifications for {} (will retry at {})", feedResourceUri, nextTry);
    }

    private boolean register4Notifications(RSSUpdateMechanism mechanism, String updateUrl) {
        if (!"http-post".equals(mechanism.getProtocol())) {
            log.warn("unsupported update protocol: '{}'", mechanism.getProtocol());
            return false;
        } else {
            if (log.isDebugEnabled()) {
                log.debug("About to register at '{}'://{}:{}/{}", new Object[] { mechanism.getProtocol(), mechanism.getDomain(),
                        mechanism.getPort(), mechanism.getPath() });
            }
        }
        try {
            String data = createContentData(updateUrl);

            String p[] = mechanism.getProtocol().split("-");
            URL register = new URL(p[0], mechanism.getDomain(), mechanism.getPort(), mechanism.getPath());
            final HttpURLConnection con = (HttpURLConnection) register.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod(p[1].toUpperCase());
            con.setRequestProperty("Host", register.getHost());
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Content-Length", Integer.toString(data.length()));

            con.getOutputStream().write(data.getBytes());

            if (con.getResponseCode() / 100 == 2) {
                SAXBuilder b = new SAXBuilder(XMLReaders.NONVALIDATING);
                Document doc = b.build(con.getInputStream());

                String success = doc.getRootElement().getAttributeValue("success");
                String message = doc.getRootElement().getAttributeValue("msg");

                con.disconnect();
                if (Boolean.parseBoolean(success))
                    // Hooray!
                    return true;
                else {
                    log.warn("registration for updates failed: {}", message);
                }
            } else {
                log.warn("registration for updates failed: {} ({})", con.getResponseMessage(), con.getResponseCode());
                con.disconnect();
            }
        } catch (MalformedURLException e) {
            log.error("could not build valid url: {}", e.getMessage());
        } catch (ProtocolException e) {
            log.error("invalid/unknown http-method: {}", e.getMessage());
        } catch (IOException e) {
            log.error("io-failure: {}", e.getMessage());
        } catch (JDOMException e) {
            log.warn("received invalid registration response");
        } catch (NullPointerException e) {
            log.warn("stumbled over a missing value: {}", e.getMessage());
        }
        return false;
    }

    private String createContentData(String updateUrl) throws MalformedURLException {
        final URL notificationUrl = new URL(configurationService.getServerUri() +
                RSSWebService.SERVICE_PATH + RSSWebService.UPDATE_PATH);

        StringBuilder data = new StringBuilder();
        data.append("notifyProcedure="); // for http-post, procedure is empty.
        data.append("&protocol=").append("http-post"); // that's all we support
        data.append("&domain=").append(urlEncode(notificationUrl.getHost()));
        data.append("&port=")
                .append(notificationUrl.getPort() > 0 ? notificationUrl.getPort() : notificationUrl.getDefaultPort());
        data.append("&path=").append(urlEncode(notificationUrl.getPath().replaceAll("//", "/")));

        data.append("&url1=").append(urlEncode(updateUrl));

        final String dataString = data.toString();
        log.trace("Data sent to cloud: '{}'", dataString);
        return dataString;
    }

    private String urlEncode(String in) {
        try {
            return URLEncoder.encode(in, "utf-8");
        } catch (UnsupportedEncodingException e) {
            log.warn("Could not URL-encode '{}': {}", in, e.getMessage());
            return in;
        }
    }

    public void setFeedResourceUri(String uri) {
        this.feedResourceUri = uri;
    }

    public void setErrorCount(int errorCount) {
        this.errorCount = errorCount;
    }

}
