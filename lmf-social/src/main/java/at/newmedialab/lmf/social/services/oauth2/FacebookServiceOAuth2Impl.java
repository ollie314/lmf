/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.services.oauth2;

import java.io.IOException;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.marmotta.platform.core.api.http.HttpClientService;
import org.slf4j.Logger;

import at.newmedialab.lmf.social.api.facebook.FacebookService;
import at.newmedialab.lmf.social.model.facebook.UserAccountFacebook;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.restfb.BinaryAttachment;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.DefaultJsonMapper;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.WebRequestor;
import com.restfb.types.NamedFacebookType;
import com.restfb.types.User;

/**
 * Facebook Graph API access using RestFB. Queries and returns the /me user object based on the access token
 * passed as argument.
 * 
 * @see <a href="http://restfb.com">http://restfb.com</a>
 *
 */
@ApplicationScoped
public class FacebookServiceOAuth2Impl implements FacebookService {

    @Inject
    private Logger log;

    @Inject
    private HttpClientService httpClientService;

    @PostConstruct
    public void initialise() {
    }

    @Override
    public UserAccountFacebook getUserProfile(@NotNull String accessToken) {
        FacebookClient facebookClient = new DefaultFacebookClient(accessToken,new LMFWebRequester(), new DefaultJsonMapper());

        User user = facebookClient.fetchObject("me", User.class, Parameter.with("metadata", 1));

        UserAccountFacebook result = new UserAccountFacebook(user);

        // get the movies a user likes
        Connection<NamedFacebookType> movies = facebookClient.fetchConnection("me/movies", NamedFacebookType.class);
        result.setMovies(Lists.transform(movies.getData(), new FBUriFunction()));

        // get the books a user likes
        Connection<NamedFacebookType> books = facebookClient.fetchConnection("me/books", NamedFacebookType.class);
        result.setBooks(Lists.transform(books.getData(), new FBUriFunction()));

        // get the books a user likes
        Connection<NamedFacebookType> music = facebookClient.fetchConnection("me/music", NamedFacebookType.class);
        result.setMusic(Lists.transform(music.getData(), new FBUriFunction()));

        // get the interests a user likes
        Connection<NamedFacebookType> interests = facebookClient.fetchConnection("me/interests", NamedFacebookType.class);
        result.setInterests(Lists.transform(interests.getData(), new FBUriFunction()));


        // get the sports a user likes
        result.setSports(Lists.transform(user.getSports(), new FBUriFunction()));


        // get all other items a user likes
        Connection<NamedFacebookType> likes = facebookClient.fetchConnection("me/likes", NamedFacebookType.class);
        result.setLikes(Lists.transform(likes.getData(), new FBUriFunction()));


        // get the friends of a user
        Connection<NamedFacebookType> friends = facebookClient.fetchConnection("me/friends", NamedFacebookType.class);
        result.setFriends(Lists.transform(friends.getData(), new FBUriFunction()));


        return result;
    }

    protected class FBNameFunction implements Function<NamedFacebookType,String> {
        @Nullable
        @Override
        public String apply(@Nullable NamedFacebookType input) {
            return input.getName();
        }

    }

    /**
     * A converter function extracting the facebook Graph API URI from an object
     */
    protected class FBUriFunction implements Function<NamedFacebookType,String> {
        @Nullable
        @Override
        public String apply(@Nullable NamedFacebookType input) {
            return "http://graph.facebook.com/"+input.getId();
        }

    }


    protected class LMFWebRequester implements WebRequestor {
        /**
         * Given a Facebook API endpoint URL, execute a {@code GET} against it.
         *
         * @param url The URL to make a {@code GET} request for, including URL
         *            parameters.
         * @return HTTP response data.
         * @throws java.io.IOException If an error occurs while performing the {@code GET} operation.
         * @since 1.5
         */
        @Override
        public Response executeGet(String url) throws IOException {
            return httpClientService.doGet(url, new RestFBResponseHandler());
        }

        /**
         * Given a Facebook API endpoint URL and parameter string, execute a
         * {@code POST} to the endpoint URL.
         *
         * @param url        The URL to {@code POST} to.
         * @param parameters The parameters to be {@code POST}ed.
         * @return HTTP response data.
         * @throws java.io.IOException If an error occurs while performing the {@code POST}.
         */
        @Override
        public Response executePost(String url, String parameters) throws IOException {
            return httpClientService.doPost(url + (parameters.length() > 0 ? "?" + parameters : ""), new StringEntity(""), new RestFBResponseHandler());
        }

        /**
         * Given a Facebook API endpoint URL and parameter string, execute a
         * {@code POST} to the endpoint URL.
         *
         * @param url               The URL to {@code POST} to.
         * @param parameters        The parameters to be {@code POST}ed.
         * @param binaryAttachments Optional binary attachments to be included in the {@code POST}
         *                          body (e.g. photos and videos).
         * @return HTTP response data.
         * @throws java.io.IOException If an error occurs while performing the {@code POST}.
         */
        @Override
        public Response executePost(String url, String parameters, BinaryAttachment... binaryAttachments) throws IOException {
            MultipartEntity multipart = new MultipartEntity();
            for(BinaryAttachment a : binaryAttachments) {
                InputStreamBody body = new InputStreamBody(a.getData(), a.getFilename());
                multipart.addPart(a.getFilename(),body);
            }
            return httpClientService.doPost(url + (parameters.length() > 0 ? "?" + parameters : ""), multipart, new RestFBResponseHandler());
        }

    }

    protected class RestFBResponseHandler implements ResponseHandler<WebRequestor.Response> {
        /**
         * Processes an {@link org.apache.http.HttpResponse} and returns some value
         * corresponding to that response.
         *
         * @param response The response to process
         * @return A value determined by the response
         * @throws org.apache.http.client.ClientProtocolException
         *                             in case of an http protocol error
         * @throws java.io.IOException in case of a problem or the connection was aborted
         */
        @Override
        public WebRequestor.Response handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
            return new WebRequestor.Response(response.getStatusLine().getStatusCode(), IOUtils.toString(response.getEntity().getContent()));
        }
    }

}
