/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.model.tagging;

import static org.apache.marmotta.commons.sesame.model.Namespaces.NS_HGTAGS;

import java.util.Date;

import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;
import org.openrdf.model.Resource;

/**
 * A tagging represents the activity of associating a tag with a resource. It is an interface to the HGTags ontology.
 * <p/>
 * User: sschaffe
 */
@RDFType(NS_HGTAGS + "Tagging")
public interface Tagging extends Facade {

    /**
     * Return the user who created the tagging. Mapped to the hgtags:taggedBy RDF property.
     * @return
     */
    @RDF(NS_HGTAGS+"taggedBy")
    public Resource getTaggedBy();


    /**
     * Set the user who created the tagging. Mapped to the hgtags:taggedBy RDF property.
     * @param user
     */
    public void setTaggedBy(Resource user);


    /**
     * Get the datetime on which the tagging has been performed.  Mapped to the hgtags:taggedOn RDF property.
     * @return
     */
    @RDF(NS_HGTAGS+"taggedOn")
    public Date getCreatedOn();

    /**
     * Set the datetime on which the tagging has been performed. Mapped to the hgtags:taggedOn RDF property.
     * @param date
     */
    public void setCreatedOn(Date date);


    /**
     * Return the resource that is used as tag in this tagging. Mapped to the hgtags:associatedTag RDF property.
     * @return
     */
    @RDF(NS_HGTAGS+"associatedTag")
    public Tag getTaggingResource();



    public void setTaggingResource(Tag taggingResource);


    /**
     * Return the resource that has been tagged by the tagging. Mapped to the hgtags:taggedResource RDF property.
     * @return
     */
    @RDF(NS_HGTAGS+"taggedResource")
    public Resource getTaggedResource();


    /**
     * Set the resource that has been tagged by the tagging. Mapped to the hgtags:taggedResource RDF property.
     * @return
     */
    public void setTaggedResource(Resource kiWiResource);
}
