/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.model.rss;


import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFInverse;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;
import org.apache.marmotta.platform.core.model.user.MarmottaUser;

import java.util.Date;
import java.util.LinkedList;

import static at.newmedialab.lmf.social.model.Constants.NS_FCP_RSS;
import static org.apache.marmotta.commons.sesame.model.Namespaces.NS_DC;
import static org.apache.marmotta.commons.sesame.model.Namespaces.NS_RSS;

@RDFType(NS_RSS + "item")
public interface RSSEntryFacade extends Facade {

    @RDF(NS_RSS + "title")
    String getTitle();
    void setTitle(String title);

    @RDF(NS_RSS + "description")
    String getDescription();
    void setDescription(String description);

    @RDF(NS_RSS + "link")
    String getLink();
    void setLink(String url);

    @RDF(NS_DC + "creator")
    LinkedList<MarmottaUser> getAuthors();
    void setAuthors(LinkedList<MarmottaUser> authors);

    @RDF(NS_DC + "date")
    Date getDate();
    void setDate(Date date);

    @RDF(NS_RSS + "modules/content/")
    LinkedList<String> getContent();
    void setContent(LinkedList<String> content);

    @RDFInverse(NS_RSS + "items")
    RSSFeedFacade getFeed();
    void setFeed(RSSFeedFacade feed);

    @RDF(NS_FCP_RSS + "categories")
    LinkedList<RSSCategoryFacade> getCategories();
    void setCategories(LinkedList<RSSCategoryFacade> categories);

}
