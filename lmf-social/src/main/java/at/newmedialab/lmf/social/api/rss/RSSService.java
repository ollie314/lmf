/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.api.rss;

import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;

import at.newmedialab.lmf.social.model.rss.RSSFeedFacade;

import java.io.Reader;
import java.net.URL;
import java.util.Collection;
import java.util.Date;

public interface RSSService {

    public int addFeed(String feedUrl);
    public int addFeed(URL feedUrl);

    public Collection<URI> getFeeds();
    public Collection<RSSFeedFacade> getFeedFacades(RepositoryConnection connection);

    public URI getFeed(String feedUrl);
    public URI getFeed(URL feedUrl);
    public RSSFeedFacade getFeedFacade(String feedUrl, RepositoryConnection connection);
    public RSSFeedFacade getFeedFacade(URL feedUrl, RepositoryConnection connection);

    public int updateFeed(String feedUrl);
    public int updateFeed(URL feedUrl);
    public int updateFeed(URI feed);

    public int importFeed(URL url, URI user);
    public int importFeed(Reader r, URI user);

    public boolean isFeed(Resource feed);

    public boolean isKnownFeedSource(String url);

    public Date getScheduledUpdate(String feedUrl);

    public Date getScheduledUpdate(URL feedUrl);
}
