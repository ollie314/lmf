/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.webservices;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.slf4j.Logger;

import at.newmedialab.lmf.social.api.UserAccountService;
import at.newmedialab.lmf.social.model.UserAccount;

@ApplicationScoped
@Path(UserAccountWebService.PATH)
public class UserAccountWebService {

	public static final String PATH = "/social";
    
    @Inject
    private Logger log;
	
    @Inject
    private UserAccountService userAccountService;
    
    @GET
    @Path("/accounts")
    @Produces("application/json")
    public Response getUserInfo(@QueryParam("person") @NotNull String person) {
		if (StringUtils.isBlank(person)) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("'person'parameter is missing").build();
		} else {
	        if (userAccountService.exists(person)) {
	        	List<UserAccount> userAccounts = userAccountService.getUserAccounts(person);
	            try {
	                ObjectMapper mapper = new ObjectMapper();
	                mapper.setSerializationInclusion(Inclusion.NON_NULL);
	                ObjectWriter typedWriter = mapper.writerWithType(mapper.getTypeFactory().constructCollectionType(List.class, UserAccount.class));
	                String str = typedWriter.writeValueAsString(userAccounts); //TODO: json-ld
	                return Response.status(Status.OK).entity(str).build();
	            } catch (Exception e) {
	                log.error("Error serializing to JSON the user account: " + e.getMessage());
	                return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
	            }
	        } else {
	            return Response.status(Status.NOT_FOUND).entity("person not found").build();
	        }
		}
    } 
    
    @GET
    @Path("/preferences")
    @Produces("application/json")
    public Response getUserPreferences(@QueryParam("person") @NotNull String person) throws JsonGenerationException, JsonMappingException, IOException {
		if (StringUtils.isBlank(person)) {
			return Response.status(Status.NOT_ACCEPTABLE).entity("'person'parameter is missing").build();
		} else {
	        if (userAccountService.exists(person)) {
	        	Map<String, Double> preferences = userAccountService.getPreferences(person);
	        	ObjectMapper mapper = new ObjectMapper();
	            return Response.ok().entity(mapper.writeValueAsString(preferences)).build();
	        } else {
	            return Response.status(Status.NOT_FOUND).entity("person not found").build();
	        }
		}
    }  
	
	
}
