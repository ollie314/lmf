/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.model.tagging;

import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFInverse;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;
import org.apache.marmotta.commons.sesame.model.Namespaces;
import org.openrdf.model.Resource;

import java.util.Set;

import static org.apache.marmotta.commons.sesame.model.Namespaces.NS_HGTAGS;

/**
 * A tag is a KiWi resource that is used to tagged other KiWi resources. A tag participates in Taggings.
 * <p/>
 * User: sschaffe
 */
@RDFType({NS_HGTAGS+"Tag", Namespaces.NS_MOAT+"Tag" })
public interface Tag extends Facade {

    /**
     * Return the tag labels that are used to refer to this tag in addition to the title.
     * @return
     */
    @RDF(NS_HGTAGS+"name")
    public Set<String> getTagLabels();



    /**
     * Return the tag labels that are used to refer to this tag in addition to the title.
     * @return
     */
    public void setTagLabels(Set<String> labels);



    /**
     * Return a collection of the items tagged with the tag.
     * @return
     */
    @RDF(NS_HGTAGS+"isTagOf")
    public Set<Resource> getTaggedItems();


    /**
     * Return a collection of tags that are equivalent to the tag.
     * @return
     */
    @RDF(NS_HGTAGS+"equivalentTag")
    public Set<Tag> getEquivalentTags();


    /**
     * Set the tags that are equivalent to the tag.
     * @param equivalentTags
     */
    public void setEquivalentTags(Set<Tag> equivalentTags);


    /**
     * Return the taggings that use this tag.
     * @return
     */
    @RDFInverse(NS_HGTAGS+"associatedTag")
    public Set<Tagging> getTaggings();
}
