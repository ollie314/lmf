/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.webservices.oauth2;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.slf4j.Logger;

import at.newmedialab.lmf.social.api.UserAccountService;
import at.newmedialab.lmf.social.api.facebook.FacebookService;
import at.newmedialab.lmf.social.model.UserAccount;

@ApplicationScoped
@Path(FacebookOauth2WebService.PATH)
public class FacebookOauth2WebService implements Oauth2WebService {
    
    public static final String PATH = "/social/facebook/oauth2";
    
    @Inject
    private Logger log;
    
    @Inject
    private FacebookService facebookService;
    
    @Inject
    private UserAccountService userAccountService;


    @GET
    @Path("/user")
    public Response getUserInfo(@QueryParam("access_token") @NotNull String accessToken) {
        try {
            UserAccount account = facebookService.getUserProfile(accessToken);

            try {
                ObjectMapper mapper = new ObjectMapper();
                mapper.setSerializationInclusion(Inclusion.NON_NULL);
                String str = mapper.writeValueAsString(account); //TODO: json-ld
                return Response.status(Status.CREATED).entity(str).build();
            } catch (Exception e) {
                log.error("Error serializing to JSON the user account: " + e.getMessage());
                return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
            }
        } catch(RuntimeException e) {
            log.error(e.getMessage(), e);
            return Response.status(400).entity(e.getMessage()).build();
        }
    }


    @POST
    @Path("/user")
    public Response getUserInfo(@QueryParam("access_token") @NotNull String accessToken, @QueryParam("person") String person, @QueryParam("context") String context) {
        try {
            UserAccount account = facebookService.getUserProfile(accessToken);
            account = userAccountService.save(person, account, context);

            try {
                ObjectMapper mapper = new ObjectMapper();
                mapper.setSerializationInclusion(Inclusion.NON_NULL);
                String str = mapper.writeValueAsString(account); //TODO: json-ld
                return Response.status(Status.CREATED).entity(str).build();
            } catch (Exception e) {
                log.error("Error serializing to JSON the user account: " + e.getMessage());
                return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
            }
        } catch(RuntimeException e) {
            log.error(e.getMessage(), e);
            return Response.status(400).entity(e.getMessage()).build();
        }
        
        //TODO: do the other stuff
        

    }

}
