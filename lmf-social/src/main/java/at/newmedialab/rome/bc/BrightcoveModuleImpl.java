/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.rome.bc;

import com.sun.syndication.feed.CopyFrom;
import com.sun.syndication.feed.module.Module;
import com.sun.syndication.feed.module.ModuleImpl;

import java.io.Serializable;

public class BrightcoveModuleImpl extends ModuleImpl implements BrightcoveModule, Serializable {

    private static final long serialVersionUID = 1L;

    private String            playerId, lineupId, titleId, accountId;
    private Long              duration;

    public BrightcoveModuleImpl() {
        this(BrightcoveModule.class, BrightcoveModule.URI);
    }

    protected BrightcoveModuleImpl(Class<? extends BrightcoveModule> beanClass, String uri) {
        super(beanClass, uri);
    }

    @Override
    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    @Override
    public String getLineupId() {
        return lineupId;
    }

    public void setLineupId(String lineupId) {
        this.lineupId = lineupId;
    }

    @Override
    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    @Override
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Override
    public Class<? extends Module> getInterface() {
        return BrightcoveModule.class;
    }

    /**
     * Copies all the properties of the given bean into this one.
     * <p/>
     * Any existing properties in this bean are lost.
     * <p/>
     * This method is useful for moving from one implementation of a bean interface to another.
     * For example from the default SyndFeed bean implementation to a Hibernate ready implementation.
     * <p/>
     *
     * @param obj the instance to copy properties from.
     */
    @Override
    public void copyFrom(CopyFrom obj) {
        if (obj instanceof BrightcoveModule) {
            BrightcoveModule bc = (BrightcoveModule) obj;
            setPlayerId(bc.getPlayerId());
            setAccountId(bc.getAccountId());
            setLineupId(bc.getLineupId());
            setTitleId(bc.getTitleId());
        }
    }
}
